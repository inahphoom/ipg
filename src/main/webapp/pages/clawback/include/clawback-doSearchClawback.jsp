<%-- 
    Document   : clawback-doSearchClawback
    Created on : Oct 28, 2016, 12:47:55 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">

<script type="text/javascript" src="scripts/jPage/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jPage/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jPage/paging.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		
		$('#tableData').paging({
			limit : 10
		});
		
	});

</script>

<style type="text/css">
.paging-nav {
  text-align: right;
  padding-top: 10px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #31B2D5;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

.paging-nav, #tableData {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
</style>
</head>
<body>

  <table style="width: 100%">
    <tr>
      <td><div>
          <b>List of Effected Agent :</b>
        </div></td>
    </tr>
    <tr>
      <td><table id="tableData" class="table-ipg table-bordered table-hover"
          style="width: 100%">
          <thead>
            <tr>
            <th>No.</th>
              <th>Policy No.</th>
              <th>Rider No.</th>
              <th>Agent Code</th>
            </tr>
          </thead>
          <c:forEach items="${agentList}" var="agent" varStatus="loop">
            <tr align="center">
            <td align="right">${agent.no}</td>
              <td>${agent.policyNo}</td>
              <%-- <td><a href="${pageContext.request.contextPath}/agentStructure/agentStructure-ownPerformance.html">${agent.agentCode} (${agent.agentPosition})</a></td> --%>
              <td>${agent.riderNo}</td>
              <td><a href="#" onclick="callerRedirect('ownPerformance','${agent.agentCode}')">${agent.agentCode}
                  (${agent.agentPosition})</a></td>
            </tr>
          </c:forEach>
        </table></td>
    </tr>
  </table>


  <div id="clawbackDetail"></div>
</body>
</html>
