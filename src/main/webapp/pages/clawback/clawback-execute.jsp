<%-- 
    Document   : clawback-execute
    Created on : Oct 25, 2016, 5:10:17 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript">
    function doSearchClawback(){
        if($('#policyNo').val().length > 0){
            showLoading();
            $.ajax({
                url : '${pageContext.request.contextPath}/clawback/clawback-doSearchClawback.html',
                data : {
                    policyNo : $('#policyNo').val(),
                   // riderNo : $('#riderNo').val(),
                    scheduleId : ${currentSchedule.scheduleId},
                    version : ${currentSchedule.version}
                },
                cache : false,
                type: "POST",
                success : function(data) {
                    var target = $('#result');
                    target.html(data);
                    hideLoading();
                }
            });
        }
    }
    
    function resetSearch(){
        $('#result').html('');
        $('#policyNo').val('');
       // $('#riderNo').val('');
    }
</script>
</head>
<body>
  <div>
    <h2>Clawback</h2>
    <div>&nbsp;</div>
    <table>
      <tr>
        <td width="10"></td>
        <td width="15%"><b>Schedule ID : </b>${currentSchedule.scheduleId}</td>
        <td width="15%"></td>
        <td><b>Schedule Description : </b>${currentSchedule.desc}</td>
      </tr>
      <tr>
        <td></td>
        <td><b>Period : </b>
        <fmt:formatDate value="${currentSchedule.from}" dateStyle="short" /></td>
        <td></td>
        <td><b>Period To : </b>
        <fmt:formatDate value="${currentSchedule.to}" dateStyle="short" /></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Main Version : </b>${currentSchedule.version}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td width="10"><div>&nbsp;&nbsp;</div></td>
        <td></td>
        <td width="50"><div>&nbsp;</div></td>
        <td><div>&nbsp;</div></td>
      </tr>
    </table>
    <div>&nbsp;</div>
    <div>
      <b></b>
    </div>
    <table>
      <tr>
        <td width="10"></td>
        <td style="font-weight: bold">Clawback</td>
        <td></td>
      </tr>
      <tr>
        <td width="10"></td>
        <td>Policy No :</td>
        <td><input class="form-control" placeholder="กรอกข้อมูล..." type="text" id="policyNo"></td>
      <td><img src="${pageContext.request.contextPath}/images/search.png" width="25"
          height="25" style="cursor: pointer;" onclick="doSearchClawback()"><img
          src="${pageContext.request.contextPath}/images/clean.png" width="25" height="25"
          style="cursor: pointer;" onclick="resetSearch()"></td>
      </tr>
      <!-- <tr>
				<td width="10"></td>
				<td>Rider No :</td>
				<td><input class="form-control" placeholder="กรอกข้อมูล..."
					type="text" id="riderNo"/></td>
			</tr> -->
      <tr>
        <td colspan="2"></td>
        
      </tr>
    </table>
    <div>&nbsp;</div>
    <div id="result"></div>
  </div>
</body>
</html>
