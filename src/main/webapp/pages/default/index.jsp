<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
<script type="text/javascript">

    function goto(page) {
    	
        var url = '';
        var data = {};
        
        if ('selectData' === page) {
            url = '${pageContext.request.contextPath}/selectData/selectData-selectData.html';
        } else if ('pullData' === page) {
            url = '${pageContext.request.contextPath}/selectData/selectData-pullData.html';
        } else if ('synchronizeUsers' === page) {
            url = '${pageContext.request.contextPath}/selectData/selectData-synchronizeUsers.html';
        } else if ('displayStructure' === page) {
            url = '${pageContext.request.contextPath}/agentStructure/agentStructure-displayStructure.html';
        } else if ('downline' === page) {
            url = '${pageContext.request.contextPath}/agentStructure/agentStructure-downline.html';
        } else if ('ownPerformance' === page) {
            url = '${pageContext.request.contextPath}/agentStructure/agentStructure-ownPerformance.html';
        } else if ('resultDisplay' === page) {
            url = '${pageContext.request.contextPath}/resultDisplay/resultDisplay-byBenefit.html';
        } else if ('resultDisplayByAgent' === page) {
            url = '${pageContext.request.contextPath}/resultDisplay/resultDisplay-doCompare.html';
        } else if ('clawback' === page) {
            url = '${pageContext.request.contextPath}/clawback/clawback.html';
        }
        
        if(url !== '') {
            showLoading();
            $.ajax({
                url : url,
                data : data,
                cache : false,
                type: "POST",
                success : function(data) {
                    var target = $('#content');
                    target.html(data);
                    hideLoading();
                }
            });
        } else {
            alert('No Navigation Yet')
        }
        
    }
    
    function popupLogout() {
    	
	    var data = {};
	    
	   	$.ajax({
		    url : '${pageContext.request.contextPath}/logout.html',
		    data : data,
		    cache : false,
		    type: "POST",
		    success : function(data) {
		    	var target = $('#logOutDialog');
				target.html(data);
				showDialog($('#logOutDialog'));
		    }
		});
	   	
    }
    
    
    function toggleDisplay(targetId) {
    	
        if (document.getElementById(targetId).style.display === "none") {
            $('#' + targetId).show();
        } else {
            $('#' + targetId).hide();
        }
        
    }
    
    function showDialog(id) {
        $("#overlay").show();
        id.fadeIn(400);
    }
    
    function hideDialog(id) {
        $(id).fadeOut(400);
		$("#overlay").hide();
    }
    
    function showLoading() {
        $("#overlay-loading").show();
        $('#loading').show();
    }
    
    function hideLoading() {
        $('#loading').hide();
		$("#overlay-loading").hide();
    }
    
    function callerRedirect(toPage,agentCode) {
    	
        var url = '';
        var data = {};
        
        if ('ownPerformance' === toPage) {
            url = '${pageContext.request.contextPath}/agentStructure/agentStructure-ownPerformance.html';
        } else if ('downline' === toPage) {
        	url = '${pageContext.request.contextPath}/agentStructure/agentStructure-downline.html';
        }
        
        if (url !== '') {
            showLoading();
            $.ajax({
                url : url,
                data : {
                	page : toPage,
                	agentCode : agentCode
                },
                cache : false,
                type: "POST",
                success : function(data) {
                    hideLoading();
                    var target = $('#content');
                    target.html(data);
                }
            });
        } else {
            alert('No Navigation Yet')
        }
        
    }
    
</script>
<style type="text/css">

html, body, .top-header-wrapper {display:table;width:100%;} 
.content-wrapper {display:table;width:calc(100%);}

</style>
</head>
<body id="body" class="index-body">
	
	<div class="top-header-wrapper">
		<div class="top-header" id="header">
			<h2>iCompensation Playground</h2>
		</div>
		<div class="user-display" style="float: right;">User : <s:property value="#session.loginId" /></div>
	</div>
	<div style="clear: both;"></div>

	<div class="content-wrapper">
	
		<table style="width: 100%;">
			<tr>
				<td style="width: 260px; padding: 0px; vertical-align: top;">
					<div class="menu" id="menu">
						<ul id="menuList" style="border: 0px;" >
					
							<s:if test="%{#session.loginRole != ('IPG_USER')}">
							
								<li><a href="javascript:void(0)" onclick="toggleDisplay('menuListAdministration')">Administration</a>
									<ul id="menuListAdministration" style="display: none;">
										<li><a href="javascript:void(0)" onclick="goto('selectData')">Select Data</a></li>
										<li><a href="javascript:void(0)" onclick="goto('pullData')">Pull Data</a></li>
										<li><a href="javascript:void(0)" onclick="goto('synchronizeUsers')">Synchronize Users</a></li>
									</ul>
								</li>
							
						   </s:if>
							
							<li><a href="javascript:void(0)" onclick="toggleDisplay('menuListAgent')">Agent Structure</a>
								<ul id="menuListAgent" style="display: none;">
									<li><a href="javascript:void(0)" onclick="goto('displayStructure')">Display Structure</a></li>
									<li><a href="javascript:void(0)" onclick="goto('downline')">Downline</a></li>
									<li><a href="javascript:void(0)" onclick="goto('ownPerformance')">Own Performance</a></li>
								</ul>
							</li>
							<li><a href="javascript:void(0)" onclick="goto('resultDisplay')">List result by benefit</a></li>
							<li><a href="javascript:void(0)" onclick="goto('clawback')">Clawback</a></li>
							<li><a href="javascript:void(0)" onclick="popupLogout()">Logout</a></li>
						</ul>
					</div>
				</td>
				<td style="width: 100%; padding: 0px; vertical-align: top;">
					<div class="content" id="content"></div>
				</td>
			</tr>
		</table>

<!-- 		<div class="menu" id="menu"> -->
<!-- 			<ul id="menuList" style="border: 0px;" > -->
				
<%-- 				<s:if test="%{#session.loginRole != ('IPG_USER')}"> --%>
				
<!-- 					<li><a href="javascript:void(0)" onclick="toggleDisplay('menuListAdministration')">Administration</a> -->
<!-- 						<ul id="menuListAdministration" style="display: none;"> -->
<!-- 							<li><a href="javascript:void(0)" onclick="goto('selectData')">Select Data</a></li> -->
<!-- 							<li><a href="javascript:void(0)" onclick="goto('pullData')">Pull Data</a></li> -->
<!-- 							<li><a href="javascript:void(0)" onclick="goto('synchronizeUsers')">Synchronize Users</a></li> -->
<!-- 						</ul> -->
<!-- 					</li> -->
				
<%-- 			   </s:if> --%>
				
<!-- 				<li><a href="javascript:void(0)" onclick="toggleDisplay('menuListAgent')">Agent Structure</a> -->
<!-- 					<ul id="menuListAgent" style="display: none;"> -->
<!-- 						<li><a href="javascript:void(0)" onclick="goto('displayStructure')">Display Structure</a></li> -->
<!-- 						<li><a href="javascript:void(0)" onclick="goto('downline')">Downline</a></li> -->
<!-- 						<li><a href="javascript:void(0)" onclick="goto('ownPerformance')">Own Performance</a></li> -->
<!-- 					</ul> -->
<!-- 				</li> -->
<!-- 				<li><a href="javascript:void(0)" onclick="toggleDisplay('menuListResult')">Result display</a> -->
<!-- 					<ul id="menuListResult" style="display: none;"> -->
<!-- 						<li><a href="javascript:void(0)" onclick="goto('resultDisplay')">List result by benefit</a></li> -->
<!-- 						<li><a href="javascript:void(0)" onclick="goto('resultDisplay')">Detail result by benefit by agent</a></li> -->
<!-- 					</ul> -->
<!-- 				</li> -->
<!-- 				<li><a href="javascript:void(0)" onclick="goto('clawback')">Clawback</a></li> -->
<!-- 				<li><a href="javascript:void(0)" onclick="popupLogout()">Logout</a></li> -->
<!-- 			</ul> -->
<!-- 		</div> -->
<!-- 		<div class="content" id="content"></div> -->
	</div>
	<div id="popupDialog" class="web-dialog"></div>
	<div id="logOutDialog" class="logout-dialog"></div>
	<div id="overlay" class="web-dialog-overlay"></div>
	<div id="loading" class="loading-dialog">
		<img src="${pageContext.request.contextPath}/images/loading.gif" width="200" height="100" alt="Loading..." />
	</div>
	<div id="overlay-loading" class="loading-dialog-overlay"></div>
</body>
</html>