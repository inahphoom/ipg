<%-- 
    Document   : agentStructure-doResultDisplay
    Created on : Nov 2, 2016, 4:01:34 PM
    Author     : Adichart
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">
<script type="text/javascript">
	function displayByAgent(agentCode, inScheduleId, isRule, isCompareVersion,
			inMainVersion, policyNo, riderNo) {

		showLoading();
		ajaxCheckKeyInDisplay(agentCode, inScheduleId, isRule,
				isCompareVersion, inMainVersion, policyNo, riderNo);
	}
</script>
<script type="text/javascript" src="scripts/jPage/paging.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		
		$('#benefittable').paging({
			limit : 10
		});
		
	});
	

</script>
<style type="text/css">
.paging-nav {
  text-align: right;
  padding-top: 10px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #31B2D5;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

.paging-nav, #tableData {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
</style>
</head>
<body>
  <div id="benefit">
    <table style="width: 100%">
      <tr>
        <td width="10"></td>
        <td><table style="width: 100%">
            <tr align="right">
              <td><b>Version: </b>${compareVersion}</td>
            </tr>
          </table>
          <table id="benefittable" class="table-ipg table-bordered table-hover">
            <thead>
              <tr>
              <th>No.</th>
                <th>Agent ID</th>
                <th>Agent</th>
                <th>Policy No</th>
                <th>Rider No</th>
                <th>Benefit Amount<br />Main Version
                </th>
                <th style="background-color: #04B431">Benefit Amount<br />Compare Version
                </th>
              </tr>
            </thead>
            <c:forEach items="${listBenefit}" var="benefit" varStatus="loop">
              <tr>
                <%-- <td><a href="${pageContext.request.contextPath}/resultDisplay/resultDisplay-doCompare.html?page=resultDisplayByAgent&agentId=${benefit.agentCode}&scheduleId=${scheduleId}&rule=${rule}&compareVersion=${compareVersion}&mainVersion=${mainVersion}" target="_self">${benefit.agentCode}</a></td> --%>
                <td align="right">${benefit.no}</td>
                <td><a href="#"
                  onclick="displayByAgent('${benefit.agentCode}','${scheduleId}','${rule}','${compareVersion}','${mainVersion}','${benefit.policyNo}','${benefit.riderNo}')">${benefit.agentCode}</a></td>
                <td>${benefit.agentTitleTha}${benefit.agentFirstnameTha}
                  ${benefit.agentLastnameTha}<br />${benefit.agentTitleEng}
                  ${benefit.agentFirstnameEng} ${benefit.agentLastnameEng}
                </td>
                <td>${benefit.policyNo}</td>
                <td>${benefit.riderNo}</td>
                <td align="right">${benefit.benefitAmountMain}</td>
                <td align="right">${benefit.benefitAmountCompare}</td>
              </tr>
            </c:forEach>
          </table></td>
      </tr>
    </table>

  </div>
  <div>&nbsp;</div>
  <div id="display"></div>
</body>
</html>
