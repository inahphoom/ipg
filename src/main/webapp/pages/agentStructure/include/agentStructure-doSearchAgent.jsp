<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>

<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">

<script type="text/javascript" src="scripts/jPage/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jPage/jquery-ui.min.js"></script>

<script type="text/javascript" src="scripts/jPage/paging.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		
		$('#tableData').paging({
			limit : 10
		});
		
	});

	function setAgent() {
		
		var agentCode = $("input[name='selectAgent']:checked").val();

		if (undefined !== agentCode && agentCode.length > 0) {
			
			$('#searchById').click();
			$('#searchInput').val(agentCode);

			ajaxCheckKeyIn($('#searchInput'), true);
			
		} else {
			
			alert('Please select agent');
			return false;
			
		}

		hideDialog($('#popupDialog'));
		
	}
	
	function clearPopData() {
	
		$("#popupSearchInput").val("");
		$("#searchResult").html("");
	
	}
	
</script>

<style type="text/css">
.paging-nav {
  text-align: right;
  padding-top: 10px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #31B2D5;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

.paging-nav, #tableData {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
</style>

</head>
<body>

  <div>
    <b>Select Agent (ระบุตัวแทนเพื่อแสดงโครงสร้าง)</b>
  </div>
  <table id="tableData" class="table-ipg table-bordered table-hover" style="width: 100%">
    <thead>
      <tr>
        <th>#</th>
        <th>Agent Code</th>
        <th>Name</th>
        <th>Surname</th>
        <th>ชื่อ</th>
        <th>นามสกุล</th>
        <th>Channel</th>
        <th>Position</th>
        <th>Status</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach items="${agentList}" var="agent" varStatus="loop">
        <tr>
          <td align="center"><input type="radio" name="selectAgent" value="${agent.agentCode}" /></td>
          <td>${agent.agentCode}</td>
          <td>${agent.agentTitleEng}${agent.agentFirstnameEng}</td>
          <td>${agent.agentLastnameEng}</td>
          <td>${agent.agentTitleTha}${agent.agentFirstnameTha}</td>
          <td>${agent.agentLastnameTha}</td>
          <td align="center">${agent.saleChannelCode}</td>
          <td align="center">${agent.position}</td>
          <td align="center">${agent.agentStatus}</td>
        </tr>
      </c:forEach>
    </tbody>
  </table>

  <div align="center">
    <input class="btn btn-info btn-sm" type="button" value="Display" onclick="setAgent()" /> <input
      class="btn btn-info btn-sm" type="button" value="Clear" onclick="clearPopData()" />
  </div>
  <br />
  <span id="alert"></span>

</body>
</html>