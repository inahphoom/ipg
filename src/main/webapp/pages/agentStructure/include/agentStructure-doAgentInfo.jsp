<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">

<script type="text/javascript" src="scripts/jPage/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jPage/jquery-ui.min.js"></script>

<script type="text/javascript" src="scripts/jPage/paging.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		
		$('#tableData').paging({
			limit : 10
		});
		
	});
	
	function searchDownline(agentCode) {
		
		if (undefined !== agentCode && agentCode.length > 0) {
			
			$('#searchById').click();
			$('#searchInput').val(agentCode);
			
			ajaxCheckKeyIn($('#searchInput'), true);
			
		}

	}

</script>

<style type="text/css">
.paging-nav {
  text-align: right;
  padding-top: 10px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #31B2D5;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

.paging-nav, #tableData {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
</style>

</head>
<body>

  <table style="width: 100%">
    <tr>
      <td width="10"></td>
      <td><b>Agent Information (ข้อมูลตัวแทน)</b></td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>ตัวแทน :</b> ${agent.agentTitleTha}${agent.agentFirstnameTha}
        ${agent.agentLastnameTha} ${agent.agentTitleEng} ${agent.agentFirstnameEng}
        ${agent.agentLastnameEng}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Code :</b> <a href="javascript:void(0)"
        onclick="callerRedirect('ownPerformance','${agent.agentCode}')">${agent.agentCode}</a></td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Channel :</b> ${agent.saleChannelCode}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Status :</b> ${agent.agentStatus}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Position :</b> ${agent.position}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Upline Code :</b> <a href="javascript:void(0)"
        onclick="searchDownline('${agent.uplineAgent.agentCode}');">${agent.uplineAgent.agentCode}</a></td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Team Size :</b> ${fn:length(agent.downlineList)}</td>
      <td></td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <table style="width: 100%">
    <tr>
      <td width="10"></td>
      <td><b>Downline (แสดงลูกทีม)</b></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td>
        <table id="tableData" class="table-ipg table-bordered table-hover">
          <thead>
            <tr>
            <th>No.</th>
              <th>Agent Code<br />รหัสตัวแทน
              </th>
              <th>Name-Surname<br />ชื่อ-นามสกุล
              </th>
              <th>Agent Position</th>
              <th>Team Size<br />จำนวนลูกทีม
              </th>
            </tr>
          </thead>
          <c:forEach items="${agent.downlineList}" var="downline" varStatus="loop">
            <tr>
            <td align="right">${downline.no}</td>
              <td><a href="javascript:void(0)"
                onclick="callerRedirect('ownPerformance','${downline.agentCode}')">${downline.agentCode}</a></td>
              <td>${downline.agentTitleEng}${downline.agentFirstnameEng}
                ${downline.agentLastnameEng} <br />
                ${downline.agentTitleTha}${downline.agentFirstnameTha} ${downline.agentLastnameTha}
              </td>
              <td>${downline.position}</td>
              <td><a href="javascript:void(0)"
                onclick="searchDownline('${downline.agentCode}');">${downline.downlineCount}</a></td>
            </tr>
          </c:forEach>
        </table>
      </td>
    </tr>
  </table>

</body>
</html>