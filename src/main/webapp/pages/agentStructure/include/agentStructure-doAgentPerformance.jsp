<%-- 
    Document   : agentStructure-doAgentPerformance
    Created on : Oct 25, 2016, 11:10:49 AM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">
<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<link rel="stylesheet" href="css/footable.standalone.css">
<link rel="stylesheet" href="css/footable.standalone.min.css">

<script type="text/javascript" src="scripts/jPage/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jPage/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jPage/paging.js"></script>
<script type="text/javascript">

	$(document).ready(function() {
		
		$('#tableData').paging({
			limit : 10
		});
		
		$('#tableDataClawback').paging({
			limit : 10
		});
		
	});
	
	

</script>

<style type="text/css">
.paging-nav {
  text-align: right;
  padding-top: 10px;
}

.paging-nav a {
  margin: auto 1px;
  text-decoration: none;
  display: inline-block;
  padding: 1px 7px;
  background: #31B2D5;
  color: white;
  border-radius: 3px;
}

.paging-nav .selected-page {
  background: #187ed5;
  font-weight: bold;
}

.paging-nav {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, sans-serif;
}
</style>
<script type="text/javascript"></script>
</head>
<body>
  <div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <b>Agent Information (ข้อมูลตัวแทน)</b>
  </div>
  <table>
    <tr>
      <td>ตัวแทน :</td>
      <td>${agent.agentTitleTha}${agent.agentFirstnameTha} ${agent.agentLastnameTha}
        ${agent.agentTitleEng} ${agent.agentFirstnameEng} ${agent.agentLastnameEng}</td>
    </tr>
    <tr>
      <td>Agent Code :</td>
      <td>${agent.agentCode}</td>
    </tr>
    <tr>
      <td>Channel :</td>
      <td>${agent.saleChannelCode}</td>
    </tr>
    <tr>
      <td>Agent Status :</td>
      <td>${agent.agentStatus}</td>
    </tr>
    <tr>
      <td>Agent Position :</td>
      <td>${agent.position}</td>
    </tr>
    <tr>
      <td>Upline Code :</td>
      <td>${agent.uplineAgent.agentCode}</td>
    </tr>
    <tr>
      <td>Team Size :</td>
      <td>${fn:length(agent.downlineList)}</td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <div>
    <b>Own Performance (แสดงกรมธรรม์ที่ขายเอง)</b>
  </div>
  <table id="tableData" class="table-ipg table-bordered">
    <thead>
      <tr>
      	<th>No.</th>
        <th>Policy No.</th>
        <th>Rider No.</th>
        <th>Year</th>
        <th>AC Flag</th>
        <th>FYP</th>
        <th>AFYP</th>
        <th>FYC</th>
        <th>AFYC</th>
        <th>RYP</th>
        <th>ARYP</th>
        <th>RYC</th>
      </tr>
    </thead>

    <c:set var="performanceFYP" value="${0}" />
    <c:set var="performanceAFYP" value="${0}" />
    <c:set var="performanceFYC" value="${0}" />
    <c:set var="performanceAFYC" value="${0}" />
    <c:set var="performanceRYP" value="${0}" />
    <c:set var="performanceARYP" value="${0}" />
    <c:set var="performanceRYC" value="${0}" />

    <c:forEach items="${performanceList}" var="performance" varStatus="loop">

      <tr>
      	<td align="right">${performance.no}</td>
        <td align="center">${performance.policyNumber}</td>
        <td align="center">${performance.riderNumber}</td>
        <td align="center">${performance.year}</td>
        <td align="center">${performance.acFlag}</td>
        <td align="right"><fmt:formatNumber value="${performance.fyp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.afyp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.fyc}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.afyc}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.ryp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.aryp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${performance.ryc}" pattern="###,##0.0000" /></td>
      </tr>

      <c:set var="performanceFYP"
        value="${performanceFYP + (performance.fyp == null ? 0 : performance.fyp)}" />
      <c:set var="performanceAFYP"
        value="${performanceAFYP + (performance.afyp == null ? 0 : performance.afyp)}" />
      <c:set var="performanceFYC"
        value="${performanceFYC + (performance.fyc == null ? 0 : performance.fyc)}" />
      <c:set var="performanceAFYC"
        value="${performanceAFYC + (performance.afyc == null ? 0 : performance.afyc)}" />
      <c:set var="performanceRYP"
        value="${performanceRYP + (performance.ryp == null ? 0 : performance.ryp)}" />
      <c:set var="performanceARYP"
        value="${performanceARYP + (performance.aryp == null ? 0 : performance.aryp)}" />
      <c:set var="performanceRYC"
        value="${performanceRYC + (performance.ryc == null ? 0 : performance.ryc)}" />

    </c:forEach>

  </table>
  <div>Total: ${fn:length(performanceList)} Record(s)</div>
  <div>
    <b>Total Performance Amount</b>
  </div>
  <table class="table-ipg table-bordered">
    <thead>
      <tr>
        <th>FYP</th>
        <th>AFYP</th>
        <th>FYC</th>
        <th>AFYC</th>
        <th>RYP</th>
        <th>ARYP</th>
        <th>RYC</th>
      </tr>
    </thead>
    <tr>
      <td align="right"><fmt:formatNumber value="${performanceFYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceAFYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceFYC}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceAFYC}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceRYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceARYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${performanceRYC}" pattern="###,##0.0000" /></td>
    </tr>

  </table>
  <div>&nbsp;</div>
  <div>
    <b>Clawback (แสดงกรมธรรม์ขายเองที่ถูกเรียกคืน)</b>
  </div>
  <table id="tableDataClawback" class="table-ipg table-bordered">
    <thead>
      <tr>
      <th>No.</th>
        <th>Policy No.</th>
        <th>Rider No.</th>
        <th>Year</th>
        <th>AC Flag</th>
        <th>FYP</th>
        <th>AFYP</th>
        <th>FYC</th>
        <th>AFYC</th>
        <th>RYP</th>
        <th>ARYP</th>
        <th>RYC</th>
      </tr>
    </thead>

    <c:set var="clawbackFYP" value="${0}" />
    <c:set var="clawbackAFYP" value="${0}" />
    <c:set var="clawbackFYC" value="${0}" />
    <c:set var="clawbackAFYC" value="${0}" />
    <c:set var="clawbackRYP" value="${0}" />
    <c:set var="clawbackARYP" value="${0}" />
    <c:set var="clawbackRYC" value="${0}" />

    <c:forEach items="${clawbackList}" var="clawback" varStatus="loop">

      <tr>
      	<td align="right">${clawback.no}</td>
        <td align="center">${clawback.policyNumber}</td>
        <td align="center">${clawback.riderNumber}</td>
        <td align="center">${clawback.year}</td>
        <td align="center">${clawback.acFlag}</td>
        <td align="right"><fmt:formatNumber value="${clawback.fyp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.afyp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.fyc}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.afyc}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.ryp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.aryp}" pattern="###,##0.0000" /></td>
        <td align="right"><fmt:formatNumber value="${clawback.ryc}" pattern="###,##0.0000" /></td>
      </tr>

      <c:set var="clawbackFYP" value="${clawbackFYP + (clawback.fyp == null ? 0 : clawback.fyp)}" />
      <c:set var="clawbackAFYP"
        value="${clawbackAFYP + (clawback.afyp == null ? 0 : clawback.afyp)}" />
      <c:set var="clawbackFYC" value="${clawbackFYC + (clawback.fyc == null ? 0 : clawback.fyc)}" />
      <c:set var="clawbackAFYC"
        value="${clawbackAFYC + (clawback.afyc == null ? 0 : clawback.afyc)}" />
      <c:set var="clawbackRYP" value="${clawbackRYP + (clawback.ryp == null ? 0 : clawback.ryp)}" />
      <c:set var="clawbackARYP"
        value="${clawbackARYP + (clawback.aryp == null ? 0 : clawback.aryp)}" />
      <c:set var="clawbackRYC" value="${clawbackRYC + (clawback.ryc == null ? 0 : clawback.ryc)}" />

    </c:forEach>

  </table>
  <div>Total: ${fn:length(clawbackList)} Record(s)</div>
  <div>
    <b>Total Clawback Amount</b>
  </div>
  <table class="table-ipg table-bordered">
    <thead>
      <tr>
        <th>FYP</th>
        <th>AFYP</th>
        <th>FYC</th>
        <th>AFYC</th>
        <th>RYP</th>
        <th>ARYP</th>
        <th>RYC</th>
      </tr>
    </thead>
    <tr>
      <td align="right"><fmt:formatNumber value="${clawbackFYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackAFYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackFYC}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackAFYC}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackRYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackARYP}" pattern="###,##0.0000" /></td>
      <td align="right"><fmt:formatNumber value="${clawbackRYC}" pattern="###,##0.0000" /></td>
    </tr>

  </table>

</body>
</html>