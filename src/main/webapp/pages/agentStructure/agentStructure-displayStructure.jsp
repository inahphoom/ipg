<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />

<style type="text/css">
th, td {
  padding: 5px;
}

.node circle {
  fill: #fff;
  stroke: steelblue;
  stroke-width: 3px;
}

.node text {
  font: 12px sans-serif;
}

.link {
  fill: none;
  stroke: #ccc;
  stroke-width: 2px;
}

a.agentNode {
  fill: #0000FF;
  text-decoration: none;
  font-weight: bold;
}

a.notAgentNode {
  fill: #000000;
  text-decoration: none;
  font-weight: bold;
}
</style>

<script type="text/javascript">

    function clearDefaultText(object) {
    	
        if ('' === object.val()) {
        	
            object.val('');
            
        }
        
    }
    
    function checkBlank(object) {
    	
        if (object.val().length === 0) {
        	
            object.val('');
            
            return false;
            
        }
        
        return true;
        
    }
    
    function ajaxCheckKeyIn(object, flagSearch) {
    	
        if (!checkBlank(object)) {
        	
            return false;
            
        }
        
        if (flagSearch) {
        	
            showLoading();
            $.ajax({
                url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
                data : {
                    searchBy : $("input[name='searchBy']:checked").val(),
                    searchInput : $('#searchInput').val(),
                    page : 'displayStructure',
                    scheduleId : ${currentSchedule.scheduleId}
                },
                cache : false,
                type: "POST",
                success : function(data) {
                	$("#wordTree").html("");
                	if (data.listMapTier.length > 0) {
    	            	generateTree(data.listMapTier, $('#searchInput').val());
                	}
                    hideLoading();
                }
    		});

        } else {
        	
	        popupSearchAgent(true);
        	
        }
        
    }
    
    function clearData() {
    	
        $('#searchInput').val("");
        $("#wordTree").html("");
        
    }
    
    function popupSearchAgent(searchFlag) {
    	
//         showLoading();
        $.ajax({
            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-popupSearch.html',
            data : {
                searchBy : $("input[name='searchBy']:checked").val(),
                searchInput : $('#searchInput').val(),
                searchFlag : searchFlag
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#popupDialog');
                target.html(data);
                showDialog($('#popupDialog'));
//                 hideLoading();
            }
        });
        
    }
    
    var nodeCount = [1];
    
    function getNodeHeight(node, deep) {

    	if (typeof node.children != "undefined" && node.children.length > 0) {
    		
    		for (var i = 0 ; i < node.children.length ; i++) {
    			
    			getNodeHeight(node.children[i], deep + 1);
    			
    		}
    		
    		nodeCount[deep] = (typeof nodeCount[deep] == 'undefined' ? 1 : nodeCount[deep] += 1);
    		
    	} else if (deep != 0) {
    		
    		nodeCount[deep] = (typeof nodeCount[deep] == 'undefined' ? 1 : nodeCount[deep] += 1);
    		
    	}
    	
    }
    
    function generateTree(listMapTier, agentCode) {
//     	var ttt = {};
//     	ttt.agent_code = "221501239";
//     	ttt.display = "นางสาวอนัญธิดา เช็กชื่นกุล (ACC-SP)";
//     	ttt.upline_code = "221501233";
//     	listMapTier.push(ttt);
//     	var ttt2 = {};
//     	ttt2.agent_code = "221501240";
//     	ttt2.display = "นางสาวอนัญธิดา เช็กชื่นกุล (ACC-SP)";
//     	ttt2.upline_code = "221501239";
//     	listMapTier.push(ttt2);
    	
    	var data = listMapTier;

		var dataMap = data.reduce(function(map, node) {
			map[node.agent_code] = node;
			return map;
		}, {});

		var treeData = [];
		data.forEach(function(node) {
         	var parent = dataMap[node.upline_code];
         	if (parent) {
         		(parent.children || (parent.children = [])).push(node);
			} else {
         		treeData.push(node);
         	}
		});

		root = treeData[0];
		
		nodeCount = [1];
		getNodeHeight(root, 0);
    	
// 		var margin = {top: 20, right: 120, bottom: 20, left: 120},
//         	width = 960 - margin.right - margin.left,
//         	height = 500 - margin.top - margin.bottom; - margin.right - margin.left
		var margin = {top: 20, right: 120, bottom: 20, left: 260},
        	width = 250 * nodeCount.length,
        	height = Math.max.apply(Math, nodeCount) * 50;
        	
        var i = 0;

        var tree = d3.layout.tree().size([height, width]);

        var diagonal = d3.svg.diagonal().projection(function(d) { return [d.y, d.x]; });

        var svg = d3.select("#wordTree").append("svg").attr("width", width + margin.right + margin.left).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		update(root);
        
        function update(source) {

			var nodes = tree.nodes(root).reverse(),
				links = tree.links(nodes);

// 	        nodes.forEach(function(d) { d.y = d.depth * 180; });
	        nodes.forEach(function(d) { d.y = d.depth * 280; });
	
	        var node = svg.selectAll("g.node").data(nodes, function(d) { return d.id || (d.id = ++i); });
	
	        var nodeEnter = node.enter().append("g").attr("class", "node").attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });
	
	        nodeEnter.append("circle").attr("r", 10).style("fill", "#fff");
	
// 	        nodeEnter.append("text").attr("x", function(d) { return d.children || d._children ? -13 : 13; }).attr("dy", ".35em").attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; }).html(function(d) { return d.display; }).style("fill-opacity", 1);
	        var nodeEnterText = nodeEnter.append("text");
// 	        nodeEnterText.append('tspan').attr("x", function(d) { return d.children || d._children ? -13 : 13; }).attr("dy", "-.10em").attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; }).html(function(d) { return '<a href="javascript:void(0);" class="' + (d.agent_code == agentCode ? 'agentNode' : 'notAgentNode') + '" onclick="callerRedirect(\'' + (d.agent_code == agentCode ? 'ownPerformance' : 'downline') + '\', \'' + d.agent_code + '\');">' + d.agent_code + '</a>'; }).style("fill-opacity", 1);
// 	        nodeEnterText.append('tspan').attr("x", function(d) { return d.children || d._children ? -13 : 13; }).attr("dy", "1.00em").attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; }).html(function(d) { return '<a href="javascript:void(0);" class="' + (d.agent_code == agentCode ? 'agentNode' : 'notAgentNode') + '" onclick="callerRedirect(\'' + (d.agent_code == agentCode ? 'ownPerformance' : 'downline') + '\', \'' + d.agent_code + '\');">' + d.display + '</a>'; }).style("fill-opacity", 1);
	        nodeEnterText.append('tspan').attr("x", function(d) { return d.children || d._children ? -13 : 13; }).attr("dy", "-.10em").attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; }).html(function(d) { return '<a href="javascript:void(0);" class="' + (d.agent_code == agentCode ? 'agentNode' : 'notAgentNode') + '" onclick="callerRedirect(\'downline\', \'' + d.agent_code + '\');">' + d.agent_code + '</a>'; }).style("fill-opacity", 1);
	        nodeEnterText.append('tspan').attr("x", function(d) { return d.children || d._children ? -13 : 13; }).attr("dy", "1.00em").attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; }).html(function(d) { return '<a href="javascript:void(0);" class="' + (d.agent_code == agentCode ? 'agentNode' : 'notAgentNode') + '" onclick="callerRedirect(\'downline\', \'' + d.agent_code + '\');">' + d.display + '</a>'; }).style("fill-opacity", 1);
	        
	        var link = svg.selectAll("path.link").data(links, function(d) { return d.target.id; });
	
	        link.enter().insert("path", "g").attr("class", "link").attr("d", diagonal);
	        
        }

	}
    
</script>
</head>
<body>

  <div>
    <h2 style="margin-left: 15px;">Display Structure</h2>
    <br />
    <table style="width: 100%;">
      <tr>
        <td width="10"></td>
        <td width="15%"><b>Schedule ID : </b>${currentSchedule.scheduleId}</td>
        <td width="15%"></td>
        <td><b>Schedule Description : </b>${currentSchedule.desc}</td>
      </tr>
      <tr>
        <td></td>
        <td><b>Period : </b>
        <fmt:formatDate value="${currentSchedule.from}" dateStyle="short" /></td>
        <td></td>
        <td><b>Period To : </b>
        <fmt:formatDate value="${currentSchedule.to}" dateStyle="short" /></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Main Version : </b>${currentSchedule.version}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="padding-left: 0px;">
          <table>
            <tr>
              <td><b>Search</b></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><div class="radio">
                  <input type="radio" name="searchBy" id="searchByName" value="name"
                    checked="checked" onclick="clearData();" />Name
                </div></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><div class="radio">
                  <input type="radio" name="searchBy" id="searchById" value="id"
                    onclick="clearData()" />ID
                </div></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><input class="form-control" placeholder="กรอกข้อมูล..." type="text"
                id="searchInput" onfocus="clearDefaultText($(this));" onblur="checkBlank($(this));"
                onchange="ajaxCheckKeyIn($(this), false);" /></td>
              <td><img src="${pageContext.request.contextPath}/images/search.png" width="25"
                height="25" style="cursor: pointer;" onclick="popupSearchAgent(false);" /></td>
              <td><img src="${pageContext.request.contextPath}/images/clean.png" width="25"
                height="25" style="cursor: pointer;" onclick="clearData();" /></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <div id="wordTree"></div>
  </div>

</body>
</html>