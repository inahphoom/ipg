<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>

<style type="text/css">
th, td {
  padding: 5px;
}
</style>

<script type="text/javascript">

	$( document ).ready(function() {
		
		$('#searchInput').val('${agentCode}');
		if ($('#searchInput').val() !== '') {
		  
			showLoading();
	        $.ajax({
	            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
	            data : {
	                searchBy : 'Id',
	                searchInput : $('#searchInput').val(),
	                page : 'performance',
	                scheduleId : ${currentSchedule.scheduleId},
// 	                scheduleId : 762,
	                ver : ${currentSchedule.version}
	            },
	            cache : false,
	            type: "POST",
	            success : function(data) {
	                var target = $('#result');
	                target.html(data);
	                hideLoading();
	            }
	        });
	        
		} else {
		  
			return false;
	  	
		}
	  
	});

    function clearDefaultText(object) {
    	
        if ('' === object.val()) {
        	
            object.val('');
            
        }
        
    }
    
    function checkBlank(object) {
    	
        if (object.val().length === 0) {
        	
            object.val('');
            
            return false;
            
        }
        
        return true;
        
    }
    
    
    function ajaxCheckKeyIn(object, flagSearch) {
    	
        if (!checkBlank(object)) {
        	
            return false;
            
        }
        
        if (flagSearch) {
        	
	        showLoading();
	        $.ajax({
	            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
	            data : {
	                searchBy : $("input[name='searchBy']:checked").val(),
	                searchInput : $('#searchInput').val(),
	                page : 'performance',
	                scheduleId : ${currentSchedule.scheduleId},
// 	                scheduleId : 762,
	                ver : ${currentSchedule.version}
	            },
	            cache : false,
	            type: "POST",
	            success : function(data) {
	                //show agentInfo
	                var target = $('#result');
	                target.html(data);
	                hideLoading();
	            }
	        });

        } else {
        	
	        popupSearchAgent(true);
        	
        }
        
    }
    
    function showAgentClawback(agentCode,shcheduleId,version) {
    	
    	if (!checkBlank(object)) {
    		
            return false;
            
        }
        
        showLoading();
        $.ajax({
            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
            data : {
                searchInput : agentCode,
                page : 'performance',
                scheduleId : shcheduleId,
                ver : version
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#result');
                target.html(data);
                hideLoading();
            }
        });
        
    }
    
    function clearData() {
    	
        $('#searchInput').val('');
        
    }
    
    function clearDataAll() {
    	
        $('#searchByName').click();
        
    }
    
    function popupSearchAgent(searchFlag) {
    	
//         showLoading();
        $.ajax({
            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-popupSearch.html',
            data : {
                searchBy : $("input[name='searchBy']:checked").val(),
                searchInput : $('#searchInput').val(),
                searchFlag : searchFlag
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#popupDialog');
                target.html(data);
                showDialog($('#popupDialog'));
//                 hideLoading();
            }
        });
        
    }
    
</script>

</head>
<body>

  <div>
    <h2 style="margin-left: 15px;">Own Performance</h2>
    <br />
    <table style="width: 100%;">
      <tr>
        <td width="10"></td>
        <td width="15%"><b>Schedule ID : </b>${currentSchedule.scheduleId}</td>
        <td width="15%"></td>
        <td><b>Schedule Description : </b>${currentSchedule.desc}</td>
      </tr>
      <tr>
        <td></td>
        <td><b>Period : </b>
        <fmt:formatDate value="${currentSchedule.from}" dateStyle="short" /></td>
        <td></td>
        <td><b>Period To : </b>
        <fmt:formatDate value="${currentSchedule.to}" dateStyle="short" /></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Main Version : </b>${currentSchedule.version}</td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3" style="padding-left: 0px;">
          <table>
            <tr>
              <td><b>Search</b></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><div class="radio">
                  <input type="radio" name="searchBy" id="searchByName" value="name"
                    checked="checked" onclick="clearData();" />Name
                </div></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><div class="radio">
                  <input type="radio" name="searchBy" id="searchById" value="id"
                    onclick="clearData();" />ID
                </div></td>
              <td><div>&nbsp;&nbsp;</div></td>
              <td><input class="form-control" placeholder="กรอกข้อมูล..." type="text"
                id="searchInput" onfocus="clearDefaultText($(this));" onblur="checkBlank($(this));"
                onchange="ajaxCheckKeyIn($(this), false);" /></td>
              <td><img src="${pageContext.request.contextPath}/images/search.png" width="25"
                height="25" style="cursor: pointer;" onclick="popupSearchAgent(false);" /></td>
              <td><img src="${pageContext.request.contextPath}/images/clean.png" width="25"
                height="25" style="cursor: pointer;" onclick="clearDataAll();" /></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td></td>
        <td colspan="3"><div id="result"></div></td>
      </tr>
    </table>
  </div>

</body>
</html>
