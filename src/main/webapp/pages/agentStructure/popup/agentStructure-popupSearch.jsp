<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>

<style type="text/css">
th, td {
  padding: 5px;
}
</style>

<script type="text/javascript">

	$(document).ready(function() {
		
		if (${searchFlag} == true) {
			
			doSearchAgent();
			
		}
		
	});

	function clearDefaultText(object) {
		
        if ('' === object.val()) {
        	
            object.val('');
            
        }
        
    }
	
    function popupClearData() {
    	
        $('#popupSearchInput').val('');
        $('#searchResult').html('');
        
    }
    
    function doSearchAgent() {
    	
        showLoading();
        $.ajax({
            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-doSearchAgent.html',
            data : {
                searchBy : $("input[name='popupSearchBy']:checked").val(),
                searchInput : $('#popupSearchInput').val()
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#searchResult');
                target.html(data);
                hideLoading();
            }
        });
        
    }
    
</script>
</head>
<body>

  <img src="${pageContext.request.contextPath}/images/close.png"
    style="cursor: pointer; float: right;" width="25" height="25"
    onclick="hideDialog($('#popupDialog'))" />
  <div style="padding: 10px;">
    <div class="header">Search Agent</div>
    <div>&nbsp;</div>
    <table>
      <tr>
        <td><b>Search</b></td>
        <td><div>&nbsp;&nbsp;</div></td>
        <c:if test="${searchBy == 'name' }">
          <td><div class="radio">
              <input type="radio" name="popupSearchBy" id="popupSearchByName" value="name"
                checked="checked" onclick="popupClearData();" />Name
            </div></td>
          <td><div class="radio">
              <input type="radio" name="popupSearchBy" id="popupSearchById" value="id"
                onclick="popupClearData();" />ID
            </div></td>
        </c:if>
        <td><div>&nbsp;&nbsp;</div></td>
        <c:if test="${searchBy == 'id' }">
          <td><div class="radio">
              <input type="radio" name="popupSearchBy" id="popupSearchByName" value="name"
                onclick="popupClearData();" />Name
            </div></td>
          <td><div class="radio">
              <input type="radio" name="popupSearchBy" id="popupSearchById" value="id"
                checked="checked" onclick="popupClearData();" />ID
            </div></td>
        </c:if>

        <td><input class="form-control" placeholder="กรอกข้อมูล..." type="text"
          id="popupSearchInput" value="${searchInput}" onfocus="clearDefaultText($(this));"
          onblur="checkBlank($(this));" /></td>
        <td><img src="${pageContext.request.contextPath}/images/search.png" width="25"
          height="25" style="cursor: pointer;" onclick="doSearchAgent();" /></td>
        <td><img src="${pageContext.request.contextPath}/images/clean.png" width="25"
          height="25" style="cursor: pointer;" onclick="popupClearData();" /></td>
      </tr>
    </table>
    <div>&nbsp;</div>
    <div id="searchResult"></div>
  </div>

</body>
</html>