<!DOCTYPE HTML><%@page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>iCompensation Playground</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<script src="scripts/login.js"></script>
<script type="text/javascript">
	function confirmLogout() {
        window.location="${pageContext.request.contextPath}/";
	}

	function cancelLogout() {
		hideDialog($('#logOutDialog'));
	}
</script>
</head>
<body>
  <div class="panel panel-default">
    <div class="panel-heading">Log Out</div>
    <div align="center">
      <label>Confirm to Logout</label>
    </div>
    <div>&nbsp;</div>
    <div align="center">
      <button class="btn btn-default btn-sm" onclick="confirmLogout()">Confirm</button>
      <button class="btn btn-default btn-sm" onclick="cancelLogout()">Cancel</button>
    </div>
    <div>&nbsp;</div>
  </div>

</body>
</html>