<!DOCTYPE HTML><%@page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title>iCompensation Playground</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="css/loginstyle.css">
</head>
<style type="text/css">
</style>
<body>
  <div class="pen-title">
    <h1>iPlayground</h1>
    <span>Powered by <a href='#'>MOTIF</a></span>
    <div class="module form-module">
      <div class="toggle"></div>
      <div class="form">
        <h2>Login to your account</h2>
        <form action="login.do" method="post">
          <input type="text" placeholder="Username" name="username" autofocus /> <input
            type="password" placeholder="Password" name="password" />
          <button>Login</button>
          <br /> <br />
          <s:iterator value="actionErrors">
            <font style="color: red;"><s:property escape="false" /></font>
          </s:iterator>
        </form>
      </div>
      <div class="cta">
        <a href="#">Forgot your password?</a>
      </div>
    </div>
  </div>
  <script src="scripts/login.js"></script>

</body>
</html>