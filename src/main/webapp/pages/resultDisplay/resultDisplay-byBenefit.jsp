<%-- 
    Document   : resultDisplay-byBenefit
    Created on : Oct 25, 2016, 5:05:05 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet"
	href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css"
	rel="stylesheet" />
	<script type="text/javascript" src="scripts/jPage/jquery.min.js"></script>
<script type="text/javascript" src="scripts/jPage/jquery-ui.min.js"></script>

<script type="text/javascript" src="scripts/jPage/paging.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		
// 		doCompare();
	    
	});
	
	function ajaxCheckKeyInDisplay(agentCode,inScheduleId,isRule,isCompareVersion,inMainVersion,inPolicyNo,inRiderNo){
	
        showLoading();
        $.ajax({
           url : '${pageContext.request.contextPath}/resultDisplay/resultDisplay-doCompare.html',
                data : {
                	agentId : agentCode,
                	page : 'resultDisplayByAgent',
					scheduleId : inScheduleId,
					rule : isRule,
					compareVersion : isCompareVersion,
					mainVersion : inMainVersion,
					policyNo : inPolicyNo,
					riderNo : inRiderNo
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#result');
                 $('#benefit').html('');
                    target.html(data);
                hideLoading();
            }
        });
   	}
	
    function doCompare(){
//         if($('#rule').val() != '' && $('#version').val() != ''){
            showLoading();
            $.ajax({
                url : '${pageContext.request.contextPath}/resultDisplay/resultDisplay-doCompare.html',
                data : {
                    rule : $('#rule').val(),
                    version : $('#version').val(),
                    scheduleId : ${currentSchedule.scheduleId},
                    mainVersion : ${currentSchedule.version},
                    page : 'doCompare'
                },
                cache : false,
                type: "POST",
                success : function(data) {
                    var target = $('#result');
                   
                    target.html(data);
                    hideLoading();
                }
            });
//         }
//         else{
//             alert('Please select Rule and Version first');
//         }
    }
    
    function clearDefaultText(object){
        if('' === object.val()){
            object.val('');
        }
    }
    
    function checkBlank(object){
        if(object.val().length === 0){
            object.val('');
            
            return false;
        }
        
        return true;
    }
    
    function clearData(){
        $('#searchInput').val('');
    }
    
    function clearDataAll(){
        $('#searchByName').click();
    }
    
    function popupSearchAgent(searchFlag){
    	
		if($('#rule').val() != '' && $('#version').val() != ''){
	        $.ajax({
	            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-popupSearch.html',
	            data : {
	                searchBy : $("input[name='searchBy']:checked").val(),
	                searchInput : $('#searchInput').val(),
	                searchFlag : searchFlag
	            },
	            cache : false,
	            type: "POST",
	            success : function(data) {
	                var target = $('#popupDialog');
	                target.html(data);
	                showDialog($('#popupDialog'));
	            }
	        });
		} else {
	         alert('Please select Rule and Version first');
		}
     
	}
    
    function ajaxCheckKeyIn(object, flagSearch){
        
		if($('#rule').val() != '' && $('#version').val() != ''){
    	
	    	if(!checkBlank(object)){
	         
	    		return false;
	    		
	        }
	        
	        if (flagSearch) {
	        
		        showLoading();
		        $.ajax({
		            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
		            data : {
		                searchBy : $("input[name='searchBy']:checked").val(),
		                searchInput : $('#searchInput').val(),
		                rule : $('#rule').val(),
		                scheduleId : ${currentSchedule.scheduleId},
		                compareVersion : $('#version').val(),
		                mainVersion : ${currentSchedule.version},
		                page : 'resultDisplay'
		            },
		            cache : false,
		            type: "POST",
		            success : function(data) {
		                var target = $('#resultCompare');
		                target.html(data);
		                hideLoading();
		            }
		        });
	        
	        } else {
	        	
		        popupSearchAgent(true);
	        	
	        }
        
		} else {
	         alert('Please select Rule and Version first');
		}
        
    }
    
    function manualCompare() {
    	
		if($('#rule').val() != '' && $('#version').val() != ''){
    	
	        showLoading();
	        $.ajax({
	            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
	            data : {
	                searchBy : $("input[name='searchBy']:checked").val(),
	                searchInput : ($('#searchInput').val() == "" ? "%" : $('#searchInput').val()),
	                rule : $('#rule').val(),
	                scheduleId : ${currentSchedule.scheduleId},
	                compareVersion : $('#version').val(),
	                mainVersion : ${currentSchedule.version},
	                page : 'resultDisplay'
	            },
	            cache : false,
	            type: "POST",
	            success : function(data) {
	                var target = $('#resultCompare');
	                target.html(data);
	                hideLoading();
	            }
	        });
        
		} else {
	         alert('Please select Rule and Version first');
		}
    	
    }
    
</script>
</head>
<style type="text/css">
th, td {
	padding: 5px;
}
</style>
<body>

	<div id="benefit">
	
		<h2 style="margin-left: 15px;">List Result by Benefit</h2>
		
		<br/>

		<table style="width: 100%">
			<tr>
				<td width="10"></td>
				<td width="15%"><b>Schedule ID : </b>${currentSchedule.scheduleId}</td>
				<td width="15%"></td>
				<td><b>Schedule Description : </b>${currentSchedule.desc}</td>
			</tr>
			<tr>
				<td></td>
				<td><b>Period : </b> <fmt:formatDate value="${currentSchedule.from}" dateStyle="short" /></td>
				<td></td>
				<td><b>Period To : </b> <fmt:formatDate value="${currentSchedule.to}" dateStyle="short" /></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Main Version : </b>${currentSchedule.version}</td>
				<td></td>
				<td></td>
			</tr>
		</table>
		
		<br/>
		
		<table style="width: 100%">
			<tr>
				<td width="10"></td>
				<td width="5%"><b>Benefit:</b></td>
				<td>
					<select class="form-control" id="rule" style="width: 30%">
						<option value="">- กรุณาเลือก -</option>
						<c:forEach items="${ruleList}" var="rule" varStatus="loop">
							<option value="${rule.value}">${rule.message}</option>
						</c:forEach>
					</select>
				</td>
			</tr>
		</table>
		
		<br/>
		
		<table>
			<tr>
				<td width="10"></td>
				<td width="25%"><b>Compare Version:</b></td>
				<td width="25%">
					<select class="form-control" id="version" style="width: 100%">
						<option value="">- กรุณาเลือก -</option>
						<c:forEach items="${versionList}" var="version" varStatus="loop">
							<option value="${version.value}">${version.message}</option>
						</c:forEach>
					</select>
				</td>
				<td></td>
			</tr>
		</table>
						
		<br/>
	
		<table>
			<tr>
				<td width="10"></td>
				<td><b>Search</b></td>
				<td>
					<div class="radio">
						<input type="radio" name="searchBy" id="searchByName" value="name" checked="true" onclick="clearData()" />Name
					</div>
				</td>
				<td>
					<div class="radio">
						<input type="radio" name="searchBy" id="searchById" value="id" onclick="clearData()" />ID
					</div>
				</td>
				<td>
					<input class="form-control" placeholder="กรอกข้อมูล..." type="text" id="searchInput" onfocus="clearDefaultText($(this))" onblur="checkBlank($(this))" onchange="ajaxCheckKeyIn($(this), false);" />
				</td>
				<td>
					<img src="${pageContext.request.contextPath}/images/search.png" width="25" height="25" style="cursor: pointer;" onclick="popupSearchAgent(false)" />
				</td>
				<td>
					<img src="${pageContext.request.contextPath}/images/clean.png" width="25" height="25" style="cursor: pointer;" onclick="clearDataAll()" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="6">
					<input class="btn btn-primary" type="button" value="Compare" onclick="manualCompare();" />
				</td>
			</tr>
		</table>
			
	    <br/>
	    
		<div id="resultCompare"></div>

	</div>
	
    <br/>
    
	<div id="result"></div>
	
</body>
</html>