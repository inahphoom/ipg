<%-- 
    Document   : agentStructure-ownPerformance
    Created on : Oct 11, 2016, 2:10:27 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript">
	
</script>


</head>
<body>
  <div>
    <h2>Result By Agent</h2>
    <table style="width: 100%">
			<tr>
				<td width="10"></td>
				<td width="15%"><b>Schedule ID : </b>${currentSchedule.scheduleId}</td>
				<td width="15%"></td>
				<td><b>Schedule Description : </b>${currentSchedule.desc}</td>
			</tr>
			<tr>
				<td></td>
				<td><b>Period : </b> <fmt:formatDate value="${currentSchedule.from}" dateStyle="short" /></td>
				<td></td>
				<td><b>Period To : </b> <fmt:formatDate value="${currentSchedule.to}" dateStyle="short" /></td>
			</tr>
			<tr>
				<td></td>
				<td><b>Main Version : </b>${currentSchedule.version}</td>
				<td></td>
				<td></td>
			</tr>
		</table>
    <div id="result"></div>
  </div>
  <table style="width: 100%">
    <tr>
      <td width="10"></td>
      <td><br><b>Agent Information (ข้อมูลตัวแทน)</b></td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>ตัวแทน :</b> ${agent.agentTitleTha}${agent.agentFirstnameTha}
        ${agent.agentLastnameTha} ${agent.agentTitleEng} ${agent.agentFirstnameEng}
        ${agent.agentLastnameEng}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Code :</b> ${agent.agentCode}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Channel :</b> ${agent.saleChannelCode}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Status :</b> ${agent.agentStatus}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Agent Position :</b> ${agent.position}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Upline Code :</b> ${agent.uplineAgent.agentCode}</td>
      <td></td>
    </tr>
    <tr>
      <td width="10"></td>
      <td><b>Team Size :</b> ${fn:length(agent.downlineList)}</td>
      <td></td>
    </tr>
  </table>
  <div>&nbsp;</div>

  <table>
    <tr>
      <td width="10"><div>&nbsp;</div></td>
      <td><b>Detail Result by Benefit by Agent</b></td>
    </tr>
    <tr>
      <td width="10"><div>&nbsp;</div></td>
      <td><b>Policy No:</b> ${policyno}</td>
    </tr>
    <tr>
      <td width="10"><div>&nbsp;</div></td>
      <td><b>Rider No:</b> ${riderno}</td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <table style="width: 60%">
    <tr>
      <td width="10">&nbsp;</td>
      <td>
        <table style="width: 100%">
          <tr align="right">
            <td><b>Version: </b>${compareVersion}</td>
          </tr>
        </table>
        <table class="table-ipg table-bordered">
          <thead>
            <tr align="center">
              <th>Benefit</th>
              <th>Amount Main Version</th>
              <th>Amount Compare Version</th>
            </tr>
          </thead>
          <tr>
            <td>${benefit.ruleDescription}</td>
            <td align="right">${benefit.benefitAmountMain}</td>
            <td align="right">${benefit.benefitAmountCompare}</td>
          </tr>
        </table>
        <table style="width: 100%">
          <tr>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td align="left"><b>Calculation Note</b></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
          </tr>
        </table>
        <table class="table-ipg table-bordered">
          <thead>
            <tr align="center">
              <th>Description</th>
              <th>Main Version Value</th>
              <th>Compare Version Value</th>
            </tr>
          </thead>
          <tr>
            <td>${benefit.ruleDescription}</td>
            <td align="right">${benefit.benefitAmountMain}</td>
            <td align="right">${benefit.benefitAmountCompare}</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

</body>
</html>
