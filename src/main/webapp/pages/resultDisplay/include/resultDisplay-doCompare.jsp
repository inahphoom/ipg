<%-- 
    Document   : resultDisplay-doCompare
    Created on : Nov 2, 2016, 2:40:12 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript">
    function clearDefaultText(object){
        if('' === object.val()){
            object.val('');
        }
    }
    
    function checkBlank(object){
        if(object.val().length === 0){
            object.val('');
            
            return false;
        }
        
        return true;
    }
    
    function clearData(){
        $('#searchInput').val('');
    }
    
    function clearDataAll(){
        $('#searchByName').click();
    }
    
    function popupSearchAgent(searchFlag){
//         showLoading();
        $.ajax({
            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-popupSearch.html',
            data : {
                searchBy : $("input[name='searchBy']:checked").val(),
                searchInput : $('#searchInput').val(),
                searchFlag : searchFlag
            },
            cache : false,
            type: "POST",
            success : function(data) {
                var target = $('#popupDialog');
                target.html(data);
                showDialog($('#popupDialog'));
//                 hideLoading();
            }
        });
    }
    
    function ajaxCheckKeyIn(object, flagSearch){
        
    	if(!checkBlank(object)){
         
    		return false;
    		
        }
        
        if (flagSearch) {
        
	        showLoading();
	        $.ajax({
	            url : '${pageContext.request.contextPath}/agentStructure/agentStructure-ajaxSearchAgentByKeyIn.html',
	            data : {
	                searchBy : $("input[name='searchBy']:checked").val(),
	                searchInput : $('#searchInput').val(),
	                rule : ${rule},
	                scheduleId : ${scheduleId},
	                compareVersion : ${compareVersion},
	                mainVersion : ${mainVersion},
	                page : 'resultDisplay'
	            },
	            cache : false,
	            type: "POST",
	            success : function(data) {
	                var target = $('#resultCompare');
	                target.html(data);
	                hideLoading();
	            }
	        });
        
        } else {
        	
	        popupSearchAgent(true);
        	
        }
        
    }

</script>

</head>
<body>
  <table>
    <tr>
      <td width="10"><div>&nbsp;&nbsp;</div></td>
      <td colspan="5"><b>List Result by Benefit</b></td>
    </tr>
    <tr>
      <td width="10"><div>&nbsp;&nbsp;</div></td>

      <td colspan="5">
        <table>
          <tr>
            <td><b>Search</b></td>
            <td><div>&nbsp;&nbsp;</div></td>
            <td><div class="radio">
                <input type="radio" name="searchBy" id="searchByName" value="name" checked="true"
                  onclick="clearData()" />Name
              </div></td>
            <td><div>&nbsp;&nbsp;</div></td>
            <td><div class="radio">
                <input type="radio" name="searchBy" id="searchById" value="id" onclick="clearData()" />ID
              </div></td>
            <td><div>&nbsp;&nbsp;</div></td>
            <td><input class="form-control" placeholder="กรอกข้อมูล..." type="text"
              id="searchInput" onfocus="clearDefaultText($(this))" onblur="checkBlank($(this))"
              onchange="ajaxCheckKeyIn($(this), false);" /></td>
            <td><img src="${pageContext.request.contextPath}/images/search.png" width="25"
              height="25" style="cursor: pointer;" onclick="popupSearchAgent(false)" /></td>
            <td><img src="${pageContext.request.contextPath}/images/clean.png" width="25"
              height="25" style="cursor: pointer;" onclick="clearDataAll()" /></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <div id="resultCompare"></div>
</body>
</html>
