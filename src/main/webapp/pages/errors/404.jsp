<!DOCTYPE html>
<html>

<head>
<title>iCompensation Playground</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
</head>

<body>

  <div class="container-fluid">
    <div class="row">
      <div class="circle_container">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
          <h1>404</h1>
        </div>
        <div class="col-md-5 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12">
          <p>The page you seek does not exist or has been moved.</p>
          <p>Start from <a href="${pageContext.request.contextPath}/">the beginning</a> might help you find what you seek.</p>
        </div>
      </div>
    </div>
  </div>

</body>
</html>