<%-- 
    Document   : selectData
    Created on : Oct 6, 2016, 5:29:48 PM
    Author     : Adichart
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="TIS-620"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript">
    function ajaxFetchSchedule(value){
        var versionDD = $('#scheduleVersion');
        
        if("" !== value){
            showLoading();
            $.ajax({
                url : '${pageContext.request.contextPath}/selectData/selectData-ajaxFetchScheduleData.html',
                data : {
                    scheduleId : value
                },
                cache : false,
                type: "POST",
                success : function(data) {
                    $('#scheduleDesc').html(data.resultMap.desc);
                    $('#scheduleFrom').html(data.resultMap.from);
                    $('#scheduleTo').html(data.resultMap.to);
                    
                    versionDD.empty().append('<option value="">- ��س����͡ -</option>');
                    
                    for (i = 0; i < data.resultMap.versionList.length; i++) {
                        var version = data.resultMap.versionList[i];
                        
                        versionDD.append('<option value="' + version.value + '">' + version.message + '</option>');
                    }
                    hideLoading();
                }
            });
        }
        else{
            $('#scheduleDesc').html('');
            $('#scheduleFrom').html('');
            $('#scheduleTo').html('');
            
            versionDD.empty().append('<option value="">- ��س����͡ -</option>');
        }
    }
    
    function setSchedule(){
        var scheduleId = $('#scheduleId').val();
        var version = $('#scheduleVersion').val();
        
        if(scheduleId !== '' && version !== ''){
            showLoading();
            $.ajax({
                url : '${pageContext.request.contextPath}/selectData/selectData-ajaxSetData.html',
                data : {
                    scheduleId : scheduleId,
                    version : version,
                    periodForm : $('#scheduleFrom').text(),
                    periodTo : $('#scheduleTo').text(),
                    desc : $('#scheduleDesc').text()
                },
                cache : false,
                type: "POST",
                success : function(data) {
                    if('OK' === data.resultMap.code){
                        alert('Success!');
                    }
                    else{
                        alert(data.resultMap.errMsg);
                    }
                    hideLoading();
                }
            });
        }
        else{
            //validate Failed
            alert('��͡���������١��ͧ');
        }
    }
    
    function clearData(){
        $('#scheduleDesc').html('');
        $('#scheduleFrom').html('');
        $('#scheduleTo').html('');
        $('#scheduleId').val('');
        $('#scheduleVersion').empty().append('<option value="">- ��س����͡ -</option>');
    }
</script>
</head>
<style type="text/css">
th, td {
  padding: 5px;
}
</style>
<body>
  <div>
    <h2 style="margin-left: 15px;">Select data (��ú����êش������)</h2>
    <br />
    <table style="width: 100%;">
      <tr>
        <td width="10"></td>
        <td width="20%"><b>Schedule ID : </b></td>
        <td><select class="form-control" id="scheduleId"
          onchange="ajaxFetchSchedule(this.value)" style="width: 30%">
            <option value="">- ��س����͡ -</option>
            <c:forEach items="${scheduleIdList}" var="scheduleId" varStatus="loop">
              <option value="${scheduleId.value}">${scheduleId.message}</option>
            </c:forEach>
        </select></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Schedule Description : </b></td>
        <td><span id="scheduleDesc"></span></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Period : </b></td>
        <td><div id="scheduleFrom"></div></td>
      </tr>
      <tr>
        <td></td>
        <td><b>To : </b></td>
        <td><div id="scheduleTo"></div></td>
      </tr>
      <tr>
        <td width="10"></td>
        <td width="10%"><b>Version : </b></td>
        <td><select class="form-control" id="scheduleVersion" style="width: 30%">
            <option value="">- ��س����͡ -</option>
        </select></td>
      </tr>
    </table>
    <br />
    <table>
      <tr>
        <td width="10"></td>
        <td><input type="button" value="Set" onclick="setSchedule()" class="btn btn-info" /></td>
        <td><div>&nbsp;</div></td>
        <td><input type="button" value="Clear" onclick="clearData()" class="btn btn-info" /></td>
      </tr>
    </table>

  </div>
</body>
</html>
