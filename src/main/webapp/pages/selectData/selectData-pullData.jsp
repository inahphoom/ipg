<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="TIS-620"%>
<%@include file="../tags.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>iCompensation Playground</title>
<link href="${pageContext.request.contextPath}/css/styles.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/theme.css" />
<link rel="stylesheet" href="assets/css/MoneAdmin.css" />
<link rel="stylesheet" href="assets/plugins/Font-Awesome/css/font-awesome.css" />
<!--END GLOBAL STYLES -->

<!-- PAGE LEVEL STYLES -->
<link href="assets/css/layout2.css" rel="stylesheet" />
<link href="assets/plugins/flot/examples/examples.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/timeline/timeline.css" />
<link href="assets/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
<script type="text/javascript">

	function ajaxListScheduleICOMDB() {

		var scheduleId = $('#scheduleId').val();

		if ("" !== scheduleId) {

			showLoading();
			$.ajax({
				url : '${pageContext.request.contextPath}/selectData/selectData-ajaxListScheduleICOMDB.html',
				data : {
					scheduleId : scheduleId
				},
				cache : false,
				type : "POST",
				success : function(data) {

					clearData();
	
					for (i = 0; i < data.resultMap.scheduleIdList.length; i++) {
						
						var scheduleId = data.resultMap.scheduleIdList[i];
						$('#scheduleFound').append('<option value="' + scheduleId.value + '">' + scheduleId.message + '</option>');
					
					}
					
					hideLoading();
					
				}
			});

		} else {

			clearData();

		}

	}

	function ajaxImportData() {

		var scheduleId = $('#scheduleFound').val();

		if (scheduleId !== '') {

			showLoading();
			$.ajax({
				url : '${pageContext.request.contextPath}/selectData/selectData-ajaxImportData.html',
				data : {
					scheduleId : scheduleId
				},
				cache : false,
				type : "POST",
				success : function(data) {
					if (data.resultMap.result == true) {
						alert('Success!');
						$('#importStatus').html("Done: Schedule '" + data.resultMap.scheduleId + "' Imported to version " + data.resultMap.ver + " in iPlayground");						
					} else {
						alert('Error! Message :: ' + data.resultMap.errMsg);
					}
					hideLoading();
				}
			});

		} else {
			alert('��͡���������١��ͧ');
		}

	}

	function clearData() {

		$('#importStatus').html('');
		$('#scheduleFound').empty().append('<option value="">- ��س����͡ -</option>');

	}
	
</script>
</head>
<style type="text/css">
th, td {
  padding: 5px;
}
</style>
<body>

  <div>
    <h2 style="margin-left: 15px;">Pull Data Set from iCompensation</h2>
    <br/>
    <table style="width: 100%;">
      <tr>
        <td width="10"></td>
        <td width="20%"><b>Search Schedule ID : </b></td>
        <td width="30%"><input type="text" id="scheduleId" name="scheduleId"
          class="form-control" placeholder="��͡������..." style="width: 200px;" /></td>
        <td><input type="button" value="Search" onclick="ajaxListScheduleICOMDB();"
          class="btn btn-info" /></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Schedule found : </b></td>
        <td><select class="form-control" id="scheduleFound" name="scheduleFound"
          style="width: 200px;">
            <option value="">- ��س����͡ -</option>
        </select></td>
        <td><input type="button" value="Import" onclick="ajaxImportData();"
          class="btn btn-info" /></td>
      </tr>
      <tr>
        <td></td>
        <td><b>Import Status : </b></td>
        <td><div id="importStatus"></div></td>
        <td></td>
      </tr>
    </table>
  </div>

</body>
</html>