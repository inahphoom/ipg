/**
 * 
 */

package util;

// import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author 10024111
 *
 */
public class DataEncryption {

  private static byte[] encrypt(String x) throws Exception {
    java.security.MessageDigest d = null;
    d = java.security.MessageDigest.getInstance("SHA-1");
    d.reset();
    d.update(x.getBytes());

    return d.digest();
  }

  public static String convertToHex(String pwd) throws Exception {
    byte[] data = encrypt(pwd);
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < data.length; i++) {
      int halfbyte = (data[i] >>> 4) & 0x0F;
      int two_halfs = 0;
      do {
        if ((0 <= halfbyte) && (halfbyte <= 9)) {
          buf.append((char) ('0' + halfbyte));
        } else {
          buf.append((char) ('a' + (halfbyte - 10)));
        }
        halfbyte = data[i] & 0x0F;
      } while (two_halfs++ < 1);
    }
    return buf.toString();
  }

  public static void main(String[] args) {
    try {
      System.out.println(convertToHex("123"));
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
