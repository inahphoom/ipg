package util;

import java.io.FileInputStream;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;


public class ConfigurationManager {

  private static final Logger logger = Logger.getLogger(ConfigurationManager.class);

  public static final String PATH_SEP = System.getProperty("file.separator");
  // Change To Server's CONFIGURATION PATH
  private static final String CONFIGURATION_PATH = "/app/motif/iPlayground/config/";
  private static final String CONFIGURATION_PATH_WIN = "D:/app/motif/iPlayground/config/";
  // Change To Server's CONFIGURATION FILENAME
  private static final String CONFIGURATION_FILENAME = "config.properties";

  private static ConfigurationManager instance = null;
  private static String ConfigDir = null;
  private Properties props = null;


  private ConfigurationManager() {

    if (ConfigDir == null) {

      ConfigDir = getConfDir();

    }

    loadPros();

  }


  private static String getConfDir() {

    String path = "";

    Map<String, String> mapEnv = System.getenv();
    path = mapEnv.get("IPG_CONF_VARIABLE");// VARIABLE for CONFIGURATION PATH

    if (path == null) {
      logger.info(System.getProperty("os.name") + " is detected.");
      if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
        logger.info("Logging initialized for Windows");
        path = CONFIGURATION_PATH_WIN;
      } else {
        logger.info("Logging initialized for POSIX");
        path = CONFIGURATION_PATH;
      }
    }
    logger.debug("configuration path: " + path);
    return path;
  }


  public static synchronized ConfigurationManager getInstance() {

    if (instance == null) {

      instance = new ConfigurationManager();

    }

    return instance;

  }


  private void loadPros() {

    FileInputStream fis = null;
    try {
      System.out.println(
          "load configuration file: " + ConfigDir + ConfigurationManager.CONFIGURATION_FILENAME);

      props = new Properties();
      // props.load(new FileInputStream(ConfigDir + ConfigurationManager.CONFIGURATION_FILENAME));
      fis = new FileInputStream(ConfigDir + ConfigurationManager.CONFIGURATION_FILENAME);
      props.load(fis);
    } catch (Exception ex) {
      logger.error("An error occurred while reading configuration file " + CONFIGURATION_FILENAME
          + ". Stack trace is:", ex);
      // System.err.println("Error initializing ConfigurationManager");
      ex.printStackTrace();
    } finally {
      try {
        if (fis != null) {
          fis.close();
        }
      } catch (Exception exGen) {
        // Do Nothing.
      }
    }
  }


  public String getProperty(String key) {

    return this.props.getProperty(key);

  }


  public static void setConfigDir(String string) {

    ConfigDir = string;

  }

}
