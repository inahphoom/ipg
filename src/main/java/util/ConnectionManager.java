package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

  private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
  private Connection conn;
  private String connectionStr;

  public ConnectionManager() {

  }

  private String getConnectionString() {

    if (connectionStr == null) {

      connectionStr = "jdbc:oracle:thin:@"
          + ConfigurationManager.getInstance().getProperty("oracle.host") + ":"
          + ConfigurationManager.getInstance().getProperty("oracle.port") + ":"
          + ConfigurationManager.getInstance().getProperty("oracle.sid");

    }

    return connectionStr;

  }

  public void closeConnection() throws SQLException {

    if (conn != null) {

      conn.close();

    }

  }

  public Connection getDbConnection() throws SQLException, ClassNotFoundException {

    // URL of Oracle database server
    Class.forName(DRIVER);
    String url = getConnectionString();

    // properties for creating connection to Oracle database
    Properties props = new Properties();
    props.setProperty("user", ConfigurationManager.getInstance().getProperty("oracle.user"));
    props.setProperty("password",
        ConfigurationManager.getInstance().getProperty("oracle.password"));

    // creating connection to Oracle database using JDBC
    conn = DriverManager.getConnection(url, props);

    return conn;

  }

}
