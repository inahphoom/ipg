package util;

public interface Constants {

  public static String USER_TYPE_INTERNAL = "INTERNAL";
  public static String USER_TYPE_LDAP = "LDAP";

}
