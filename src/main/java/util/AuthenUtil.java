package util;

import com.motif.playground.beans.LdapAuthenConfig;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.text.MessageFormat;
import java.util.Hashtable;
import java.util.Properties;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class AuthenUtil {

  private static Logger logger = Logger.getLogger(AuthenUtil.class);

  private static Properties prop = new Properties();

  static {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    try {
      prop.load(classLoader.getResourceAsStream("/config/ldap/properties/ldap.properties"));
      logger.info("load ldap.properties success");
    } catch (IOException e) {
      // TODO Auto-generated catch block
      logger.error("Error : " + e.getMessage(), e);
      throw new RuntimeException(e);
    }
  }


  public static boolean authenticate(String username, String password) {
    LdapAuthenConfig config = new LdapAuthenConfig();
    config.setServerName(prop.getProperty("ldap.location"));
    config.setProtocol(prop.getProperty("ldap.protocol"));
    config.setServerPort(Integer.valueOf(prop.getProperty("ldap.port")));
    config.setAuthenType(prop.getProperty("ldap.authen.type"));
    config.setAuthenticatingUser(username);
    config.setBindPassword(password);
    config.setBindPrincipal(MessageFormat.format(prop.getProperty("ldap.bind.principal"),
        username));
    config.setSearchFilter(prop.getProperty("ldap.search.filter"));
    config.setSearchAttribute(prop.getProperty("ldap.search.attribute"));
    config.setBaseOu(prop.getProperty("ldap.base.ou"));
    String tmp = prop.getProperty("ldap.validate.certifield");
    boolean isValidateCertificate = (StringUtils.isNotBlank(tmp) ? Boolean.valueOf(tmp) : false);
    config.setValidateCertificate(isValidateCertificate);

    logger.debug("ldap config : " + config);
    return authenLdap(config);
  }


  private static boolean authenLdap(LdapAuthenConfig config) {
    boolean flag = true;
    Hashtable<String, Object> env = new Hashtable<String, Object>();
    if (!config.isValidateCertificate()) {
      disableTrustManager();
    }
    DirContext ctx = null;
    NamingEnumeration<SearchResult> results = null;

    try {
      env.put(Context.INITIAL_CONTEXT_FACTORY, LdapAuthenConfig.getInitialcontextfactory());
      env.put(Context.PROVIDER_URL,
          config.getProtocol() + "://" + config.getServerName() + ":" + config.getServerPort());
      env.put(Context.SECURITY_AUTHENTICATION, config.getAuthenType());
      env.put(Context.SECURITY_PRINCIPAL, config.getBindPrincipal());
      env.put(Context.SECURITY_CREDENTIALS, config.getBindPassword());

      logger.debug("ProivderURL = " + env.get(Context.PROVIDER_URL));
      logger.debug("InitialCont = " + env.get(Context.INITIAL_CONTEXT_FACTORY));
      logger.debug("Authentica  = " + env.get(Context.SECURITY_AUTHENTICATION));
      logger.debug("Principal   = " + env.get(Context.SECURITY_PRINCIPAL));
      logger.debug("Credetials  = " + env.get(Context.SECURITY_CREDENTIALS));
      logger.debug("ValidateConf= " + config.isValidateCertificate());
      logger.debug("Search filte= " + config.getSearchFilter());

      ctx = new InitialDirContext(env);
      SearchControls controls = new SearchControls();
      controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
      results = ctx.search(config.getSearchFilter(),
          config.getSearchAttribute() + "=" + config.getAuthenticatingUser(), controls);

      while (results.hasMore()) {
        SearchResult searchResult = (SearchResult) results.next();
        Attributes attributes = searchResult.getAttributes();

        logger.info("\n\nCommon Name = " + attributes.get("cn").get().toString());
        logger.info("Given Name = " + attributes.get("givenname").get().toString());
      }
    } catch (AuthenticationException exAe) {
      logger.warn("Authentication failed. Stack trace is", exAe);
      flag = false;
    } catch (NamingException exName) {
      logger.warn("LDAP is not properly configure or the configuration is not set. ", exName);
      flag = false;
    } catch (NullPointerException exNul) {
      logger.warn("Session is invalid.", exNul);
      flag = false;
    } finally {
      if (results != null) {
        try {
          results.close();
        } catch (Exception e) {
          // Do nothing.
        }
      }
      if (ctx != null) {
        try {
          ctx.close();
        } catch (Exception e) {
          // Do nothing.
        }
      }
    }
    return flag;
  }


  private static void disableTrustManager() {

    TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
      @Override
      public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
      }


      @Override
      public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}


      @Override
      public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
    }
    };

    try {
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    } catch (GeneralSecurityException e) {
    }

  }

}
