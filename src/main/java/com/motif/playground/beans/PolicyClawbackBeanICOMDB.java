package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PolicyClawbackBeanICOMDB implements Serializable {

  private static final long serialVersionUID = -2934953582454416338L;

  // Primary Keys
  private BigDecimal transactionId;
  private String scheduleId;

  // General Fields
  private String clawbackId;
  private String agentCode;
  private String policyNo;
  private String riderNo;
  private String recalScheduleId;
  private Date createDate;
  private BigDecimal createBy;
  private Date updateDate;
  private BigDecimal updateBy;
  private BigDecimal prodTypeId;
  private BigDecimal prodAmount;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public String getClawbackId() {
    return clawbackId;
  }

  public void setClawbackId(String clawbackId) {
    this.clawbackId = clawbackId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public String getRiderNo() {
    return riderNo;
  }

  public void setRiderNo(String riderNo) {
    this.riderNo = riderNo;
  }

  public String getRecalScheduleId() {
    return recalScheduleId;
  }

  public void setRecalScheduleId(String recalScheduleId) {
    this.recalScheduleId = recalScheduleId;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public BigDecimal getCreateBy() {
    return createBy;
  }

  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public BigDecimal getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(BigDecimal updateBy) {
    this.updateBy = updateBy;
  }

  public BigDecimal getProdTypeId() {
    return prodTypeId;
  }

  public void setProdTypeId(BigDecimal prodTypeId) {
    this.prodTypeId = prodTypeId;
  }

  public BigDecimal getProdAmount() {
    return prodAmount;
  }

  public void setProdAmount(BigDecimal prodAmount) {
    this.prodAmount = prodAmount;
  }

  public BigDecimal getTransactionId() {
    return transactionId;
  }

  /**
   * Set transaction identifier. it should be id of the lowest-level transaction table. 
   * @param transactionId transaction identifier.
   */
  public void setTransactionId(BigDecimal transactionId) {
    this.transactionId = transactionId;
  }

  @Override
  public String toString() {
    return "PolicyClawbackBeanICOMDB [transactionId=" + transactionId + ", scheduleId=" + scheduleId
        + ", clawbackId=" + clawbackId + ", agentCode=" + agentCode + ", policyNo=" + policyNo
        + ", riderNo=" + riderNo + ", recalScheduleId=" + recalScheduleId + ", prodTypeId="
        + prodTypeId + ", prodAmount=" + prodAmount + "]";
  }

}
