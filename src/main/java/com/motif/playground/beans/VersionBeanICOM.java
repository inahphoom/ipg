package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class VersionBeanICOM implements Serializable {

  private static final long serialVersionUID = 4024733325677111693L;

  protected String scheduleId;
  protected BigDecimal ver;
  protected BigDecimal createBy;
  protected Date createDate;
  protected String isDelete;

  public VersionBeanICOM() {}

  public VersionBeanICOM(String scheduleId, BigDecimal ver, BigDecimal createBy, Date createDate,
      String isDelete) {
    this.scheduleId = scheduleId;
    this.ver = ver;
    this.createBy = createBy;
    this.createDate = createDate;
    this.isDelete = isDelete;
  }

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public BigDecimal getVer() {
    return ver;
  }

  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }

  public BigDecimal getCreateBy() {
    return createBy;
  }

  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getIsDelete() {
    return isDelete;
  }

  public void setIsDelete(String isDelete) {
    this.isDelete = isDelete;
  }

}
