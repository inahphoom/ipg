package com.motif.playground.beans;

import java.math.BigDecimal;

public class MasterCodeBean {

  public MasterCodeBean() {
    
  }
  
  public MasterCodeBean(String codeGroup, BigDecimal typeId, String code, String description) {
    super();
    this.codeGroup = codeGroup;
    this.typeId = typeId;
    this.code = code;
    this.description = description;
  }

  private String codeGroup;
  private BigDecimal typeId;
  private String code;
  private String description;

  public String getCodeGroup() {
    return codeGroup;
  }

  public BigDecimal getTypeId() {
    return typeId;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public void setCodeGroup(String codeGroup) {
    this.codeGroup = codeGroup;
  }

  public void setTypeId(BigDecimal typeId) {
    this.typeId = typeId;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "MasterCodeBean [codeGroup=" + codeGroup + ", typeId=" + typeId + ", code=" + code
        + ", description=" + description + "]";
  }
}
