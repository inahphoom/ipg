package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class MasterBean implements Serializable {

  private static final long serialVersionUID = -5833670589769228519L;
  private String codeGroup;
  private BigDecimal typeId;
  private String code;
  private String description;

  public String getCodeGroup() {
    return codeGroup;
  }

  public void setCodeGroup(String codeGroup) {
    this.codeGroup = codeGroup;
  }

  public BigDecimal getTypeId() {
    return typeId;
  }

  public void setTypeId(BigDecimal typeId) {
    this.typeId = typeId;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
