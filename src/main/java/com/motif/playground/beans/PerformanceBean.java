
package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PerformanceBean implements Serializable {

  private static final long serialVersionUID = -4300283153121628158L;

  // Primary Key properties
  private BigDecimal transactionId;
  private String scheduleId;
  private BigDecimal ver;

  // General fields
  private String policyNo;
  private String agentCode;
  private BigDecimal agentPosId;
  private BigDecimal prodTypeId;
  private BigDecimal policyTypeId;
  private BigDecimal prodAmount;
  private String inputType;
  private int policyYear;
  private String riderCode;
  private Date ackDate;
  private String performanceMonth;
  private String performanceYear;


  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }


  public BigDecimal getVer() {
    return ver;
  }

  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }


  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }


  public BigDecimal getAgentPosId() {
    return agentPosId;
  }

  public void setAgentPosId(BigDecimal agentPosId) {
    this.agentPosId = agentPosId;
  }


  public BigDecimal getProdTypeId() {
    return prodTypeId;
  }

  public void setProdTypeId(BigDecimal prodTypeId) {
    this.prodTypeId = prodTypeId;
  }


  public BigDecimal getPolicyTypeId() {
    return policyTypeId;
  }

  public void setPolicyTypeId(BigDecimal policyTypeId) {
    this.policyTypeId = policyTypeId;
  }


  public BigDecimal getProdAmount() {
    return prodAmount;
  }

  public void setProdAmount(BigDecimal prodAmount) {
    this.prodAmount = prodAmount;
  }


  public String getInputType() {
    return inputType;
  }

  public void setInputType(String inputType) {
    this.inputType = inputType;
  }


  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }


  public int getPolicyYear() {
    return policyYear;
  }

  public void setPolicyYear(int policyYear) {
    this.policyYear = policyYear;
  }


  public String getRiderCode() {
    return riderCode;
  }

  public void setRiderCode(String riderCode) {
    this.riderCode = riderCode;
  }


  public Date getAckDate() {
    return ackDate;
  }

  public void setAckDate(Date ackDate) {
    this.ackDate = ackDate;
  }


  public String getPerformanceMonth() {
    return performanceMonth;
  }

  public void setPerformanceMonth(String performanceMonth) {
    this.performanceMonth = performanceMonth;
  }

  
  public String getPerformanceYear() {
    return performanceYear;
  }

  public void setPerformanceYear(String performanceYear) {
    this.performanceYear = performanceYear;
  }

  
  public BigDecimal getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(BigDecimal transactionId) {
    this.transactionId = transactionId;
  }

}
