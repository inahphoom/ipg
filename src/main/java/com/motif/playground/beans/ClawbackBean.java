/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.beans;

import java.math.BigDecimal;

/**
 *
 * @author Adichart
 */
public class ClawbackBean {
  protected BigDecimal total;
  protected String policyNumber;
  protected String riderNumber;
  protected Integer year;
  protected String acFlag;
  protected BigDecimal fyp;
  protected BigDecimal afyp;
  protected BigDecimal fyc;
  protected BigDecimal afyc;
  protected BigDecimal ryp;
  protected BigDecimal aryp;
  protected BigDecimal ryc;
  private int no;

  /**
   * @return the policyNumber
   */
  public String getPolicyNumber() {
    return policyNumber;
  }

  /**
   * @param policyNumber the policyNumber to set
   */
  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  /**
   * @return the riderNumber
   */
  public String getRiderNumber() {
    return riderNumber;
  }

  /**
   * @param riderNumber the riderNumber to set
   */
  public void setRiderNumber(String riderNumber) {
    this.riderNumber = riderNumber;
  }

  /**
   * @return the year
   */
  public Integer getYear() {
    return year;
  }

  /**
   * @param year the year to set
   */
  public void setYear(Integer year) {
    this.year = year;
  }

  /**
   * @return the acFlag
   */
  public String getAcFlag() {
    return acFlag;
  }

  /**
   * @param acFlag the acFlag to set
   */
  public void setAcFlag(String acFlag) {
    this.acFlag = acFlag;
  }

  /**
   * @return the fyp
   */
  public BigDecimal getFyp() {
    return fyp;
  }

  /**
   * @param fyp the fyp to set
   */
  public void setFyp(BigDecimal fyp) {
    this.fyp = fyp;
  }

  /**
   * @return the afyp
   */
  public BigDecimal getAfyp() {
    return afyp;
  }

  /**
   * @param afyp the afyp to set
   */
  public void setAfyp(BigDecimal afyp) {
    this.afyp = afyp;
  }

  /**
   * @return the fyc
   */
  public BigDecimal getFyc() {
    return fyc;
  }

  /**
   * @param fyc the fyc to set
   */
  public void setFyc(BigDecimal fyc) {
    this.fyc = fyc;
  }

  /**
   * @return the afyc
   */
  public BigDecimal getAfyc() {
    return afyc;
  }

  /**
   * @param afyc the afyc to set
   */
  public void setAfyc(BigDecimal afyc) {
    this.afyc = afyc;
  }

  /**
   * @return the ryp
   */
  public BigDecimal getRyp() {
    return ryp;
  }

  /**
   * @param ryp the ryp to set
   */
  public void setRyp(BigDecimal ryp) {
    this.ryp = ryp;
  }

  /**
   * @return the aryp
   */
  public BigDecimal getAryp() {
    return aryp;
  }

  /**
   * @param aryp the aryp to set
   */
  public void setAryp(BigDecimal aryp) {
    this.aryp = aryp;
  }

  /**
   * @return the ryc
   */
  public BigDecimal getRyc() {
    return ryc;
  }

  /**
   * @param ryc the ryc to set
   */
  public void setRyc(BigDecimal ryc) {
    this.ryc = ryc;
  }

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

public int getNo() {
	return no;
}

public void setNo(int no) {
	this.no = no;
}
}
