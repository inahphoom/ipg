package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PolicyClawbackBean implements Serializable {

  private static final long serialVersionUID = 3248609058288856015L;
  
  // Primary Keys
  private BigDecimal transactionId;
  private String scheduleId;
  private BigDecimal ver;
  
  // General fields
  private String clawbackId;
  private String agentCode;
  private String policyNo;
  private String riderNo;
  private String recalScheduleId;
  private Date createDate;
  private BigDecimal createBy;
  private Date updateDate;
  private BigDecimal updateBy;
  private BigDecimal prodTypeId;
  private BigDecimal prodAmount;
  private String agentPosition;
  private int no;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public BigDecimal getVer() {
    return ver;
  }

  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }

  public String getClawbackId() {
    return clawbackId;
  }

  public void setClawbackId(String clawbackId) {
    this.clawbackId = clawbackId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public String getRiderNo() {
    return riderNo;
  }

  public void setRiderNo(String riderNo) {
    this.riderNo = riderNo;
  }

  public String getRecalScheduleId() {
    return recalScheduleId;
  }

  public void setRecalScheduleId(String recalScheduleId) {
    this.recalScheduleId = recalScheduleId;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public BigDecimal getCreateBy() {
    return createBy;
  }

  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public BigDecimal getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(BigDecimal updateBy) {
    this.updateBy = updateBy;
  }

  public BigDecimal getProdTypeId() {
    return prodTypeId;
  }

  public void setProdTypeId(BigDecimal prodTypeId) {
    this.prodTypeId = prodTypeId;
  }

  public BigDecimal getProdAmount() {
    return prodAmount;
  }

  public void setProdAmount(BigDecimal prodAmount) {
    this.prodAmount = prodAmount;
  }

  public String getAgentPosition() {
    return agentPosition;
  }

  public void setAgentPosition(String agentPosition) {
    this.agentPosition = agentPosition;
  }

  public BigDecimal getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(BigDecimal transactionId) {
    this.transactionId = transactionId;
  }
public int getNo() {
	return no;
}

public void setNo(int no) {
	this.no = no;
}

  @Override
  public String toString() {
    return "PolicyClawbackBean [transactionId=" + transactionId + ", scheduleId=" + scheduleId 
        + ", ver=" + ver + ", clawbackId=" + clawbackId + ", agentCode=" + agentCode 
        + ", policyNo=" + policyNo + ", riderNo=" + riderNo + ", recalScheduleId=" 
        + recalScheduleId + ", prodTypeId=" + prodTypeId + ", prodAmount=" + prodAmount 
        + ", agentPosition=" + agentPosition + "]";
  }

}
