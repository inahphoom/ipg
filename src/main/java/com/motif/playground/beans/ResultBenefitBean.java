package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
// import java.util.Date;

public class ResultBenefitBean implements Serializable {


  private static final long serialVersionUID = -6154007254266005598L;
  private String ruleDescription;
  private Long agentCode;
  private String agentTitleTha;
  private String agentFirstnameTha;
  private String agentLastnameTha;
  private String agentTitleEng;
  private String agentFirstnameEng;
  private String agentLastnameEng;
  private String scheduleId;
  private int ver;
  private String policyNo;
  private Long benefitAmount;
  private String benefitCode;
  private String riderNo;
  private BigDecimal benefitAmountMain;
  private BigDecimal benefitAmountCompare;
  private int no;

  public String getRuleDescription() {
    return ruleDescription;
  }

  public void setRuleDescription(String ruleDescription) {
    this.ruleDescription = ruleDescription;
  }

  public Long getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(Long agentCode) {
    this.agentCode = agentCode;
  }

  public String getAgentTitleTha() {
    return agentTitleTha;
  }

  public void setAgentTitleTha(String agentTitleTha) {
    this.agentTitleTha = agentTitleTha;
  }

  public String getAgentFirstnameTha() {
    return agentFirstnameTha;
  }

  public void setAgentFirstnameTha(String agentFirstnameTha) {
    this.agentFirstnameTha = agentFirstnameTha;
  }

  public String getAgentLastnameTha() {
    return agentLastnameTha;
  }

  public void setAgentLastnameTha(String agentLastnameTha) {
    this.agentLastnameTha = agentLastnameTha;
  }

  public String getAgentTitleEng() {
    return agentTitleEng;
  }

  public void setAgentTitleEng(String agentTitleEng) {
    this.agentTitleEng = agentTitleEng;
  }

  public String getAgentFirstnameEng() {
    return agentFirstnameEng;
  }

  public void setAgentFirstnameEng(String agentFirstnameEng) {
    this.agentFirstnameEng = agentFirstnameEng;
  }

  public String getAgentLastnameEng() {
    return agentLastnameEng;
  }

  public void setAgentLastnameEng(String agentLastnameEng) {
    this.agentLastnameEng = agentLastnameEng;
  }

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public int getVer() {
    return ver;
  }

  public void setVer(int ver) {
    this.ver = ver;
  }

  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public Long getBenefitAmount() {
    return benefitAmount;
  }

  public void setBenefitAmount(Long benefitAmount) {
    this.benefitAmount = benefitAmount;
  }

  public String getBenefitCode() {
    return benefitCode;
  }

  public void setBenefitCode(String benefitCode) {
    this.benefitCode = benefitCode;
  }

  public String getRiderNo() {
    return riderNo;
  }

  public void setRiderNo(String riderNo) {
    this.riderNo = riderNo;
  }

  public BigDecimal getBenefitAmountMain() {
    return benefitAmountMain;
  }

  public void setBenefitAmountMain(BigDecimal benefitAmountMain) {
    this.benefitAmountMain = benefitAmountMain;
  }

  public BigDecimal getBenefitAmountCompare() {
    return benefitAmountCompare;
  }

  public void setBenefitAmountCompare(BigDecimal benefitAmountCompare) {
    this.benefitAmountCompare = benefitAmountCompare;
  }

  public int getNo() {
    return no;
  }

  public void setNo(int no) {
    this.no = no;
  }
}
