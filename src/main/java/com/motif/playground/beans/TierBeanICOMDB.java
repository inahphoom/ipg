package com.motif.playground.beans;

import java.io.Serializable;

public class TierBeanICOMDB implements Serializable {

  private static final long serialVersionUID = 318733682900915718L;

  private String scheduleId;
  private String agentCode;
  private String uplineL1Code;
  private String uplineL2Code;
  private String uplineL3Code;
  private String uplineL4Code;
  private String uplineL5Code;
  private String uplineL6Code;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getUplineL1Code() {
    return uplineL1Code;
  }

  public void setUplineL1Code(String uplineL1Code) {
    this.uplineL1Code = uplineL1Code;
  }

  public String getUplineL2Code() {
    return uplineL2Code;
  }

  public void setUplineL2Code(String uplineL2Code) {
    this.uplineL2Code = uplineL2Code;
  }

  public String getUplineL3Code() {
    return uplineL3Code;
  }

  public void setUplineL3Code(String uplineL3Code) {
    this.uplineL3Code = uplineL3Code;
  }

  public String getUplineL4Code() {
    return uplineL4Code;
  }

  public void setUplineL4Code(String uplineL4Code) {
    this.uplineL4Code = uplineL4Code;
  }

  public String getUplineL5Code() {
    return uplineL5Code;
  }

  public void setUplineL5Code(String uplineL5Code) {
    this.uplineL5Code = uplineL5Code;
  }

  public String getUplineL6Code() {
    return uplineL6Code;
  }

  public void setUplineL6Code(String uplineL6Code) {
    this.uplineL6Code = uplineL6Code;
  }

}
