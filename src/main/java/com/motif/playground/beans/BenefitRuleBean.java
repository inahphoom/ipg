package com.motif.playground.beans;

public class BenefitRuleBean {
  private String ruleDesc;
  private String benefitCode;

  public String getRuleDesc() {
    return ruleDesc;
  }

  public void setRuleDesc(String ruleDesc) {
    this.ruleDesc = ruleDesc;
  }

  public String getBenefitCode() {
    return benefitCode;
  }

  public void setBenefitCode(String benefitCode) {
    this.benefitCode = benefitCode;
  }
}
