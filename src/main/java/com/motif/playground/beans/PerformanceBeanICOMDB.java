/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PerformanceBeanICOMDB implements Serializable {

  private static final long serialVersionUID = -8057020603802343227L;

  // Primary Keys
  private BigDecimal transactionId;
  private String scheduleId;
  
  // General fields
  private String policyNo;
  private String agentCode;
  private BigDecimal agentPosId;
  private BigDecimal prodTypeId;
  private BigDecimal policyTypeId;
  private BigDecimal prodAmount;
  private String inputType;
  private int policyYear;
  private String riderCode;
  private Date ackDate;
  private String performanceMonth;
  private String performanceYear;

  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public BigDecimal getAgentPosId() {
    return agentPosId;
  }

  public void setAgentPosId(BigDecimal agentPosId) {
    this.agentPosId = agentPosId;
  }

  public BigDecimal getProdTypeId() {
    return prodTypeId;
  }

  public void setProdTypeId(BigDecimal prodTypeId) {
    this.prodTypeId = prodTypeId;
  }

  public BigDecimal getPolicyTypeId() {
    return policyTypeId;
  }

  public void setPolicyTypeId(BigDecimal policyTypeId) {
    this.policyTypeId = policyTypeId;
  }

  public BigDecimal getProdAmount() {
    return prodAmount;
  }

  public void setProdAmount(BigDecimal prodAmount) {
    this.prodAmount = prodAmount;
  }

  public String getInputType() {
    return inputType;
  }

  public void setInputType(String inputType) {
    this.inputType = inputType;
  }

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public int getPolicyYear() {
    return policyYear;
  }

  public void setPolicyYear(int policyYear) {
    this.policyYear = policyYear;
  }

  public String getRiderCode() {
    return riderCode;
  }

  public void setRiderCode(String riderCode) {
    this.riderCode = riderCode;
  }

  public Date getAckDate() {
    return ackDate;
  }

  public void setAckDate(Date ackDate) {
    this.ackDate = ackDate;
  }

  public String getPerformanceMonth() {
    return performanceMonth;
  }

  public void setPerformanceMonth(String performanceMonth) {
    this.performanceMonth = performanceMonth;
  }

  public String getPerformanceYear() {
    return performanceYear;
  }

  public void setPerformanceYear(String performanceYear) {
    this.performanceYear = performanceYear;
  }

  
  public BigDecimal getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(BigDecimal transactionId) {
    this.transactionId = transactionId;
  }

  @Override
  public String toString() {
    return "PerformanceBeanICOMDB [transactionId=" + transactionId + ", scheduleId=" + scheduleId
        + ", policyNo=" + policyNo + ", agentCode=" + agentCode + ", agentPosId=" + agentPosId
        + ", prodTypeId=" + prodTypeId + ", policyTypeId=" + policyTypeId + ", prodAmount="
        + prodAmount + ", inputType=" + inputType + ", policyYear=" + policyYear + ", riderCode="
        + riderCode + ", ackDate=" + ackDate + ", performanceMonth=" + performanceMonth
        + ", performanceYear=" + performanceYear + "]";
  }

}
