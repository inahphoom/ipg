package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class DownlineAgentsBean implements Serializable {

  private static final long serialVersionUID = 7663125059031305822L;
  private String agentCode;
  private String agentTitleEng;
  private String agentFirstnameEng;
  private String agentLastnameEng;
  private String agentTitleTha;
  private String agentFirstnameTha;
  private String agentLastnameTha;
  private String position;
  private BigDecimal downlineCount;
  private int no;

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getAgentTitleEng() {
    return agentTitleEng;
  }

  public void setAgentTitleEng(String agentTitleEng) {
    this.agentTitleEng = agentTitleEng;
  }

  public String getAgentFirstnameEng() {
    return agentFirstnameEng;
  }

  public void setAgentFirstnameEng(String agentFirstnameEng) {
    this.agentFirstnameEng = agentFirstnameEng;
  }

  public String getAgentLastnameEng() {
    return agentLastnameEng;
  }

  public void setAgentLastnameEng(String agentLastnameEng) {
    this.agentLastnameEng = agentLastnameEng;
  }

  public String getAgentTitleTha() {
    return agentTitleTha;
  }

  public void setAgentTitleTha(String agentTitleTha) {
    this.agentTitleTha = agentTitleTha;
  }

  public String getAgentFirstnameTha() {
    return agentFirstnameTha;
  }

  public void setAgentFirstnameTha(String agentFirstnameTha) {
    this.agentFirstnameTha = agentFirstnameTha;
  }

  public String getAgentLastnameTha() {
    return agentLastnameTha;
  }

  public void setAgentLastnameTha(String agentLastnameTha) {
    this.agentLastnameTha = agentLastnameTha;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public BigDecimal getDownlineCount() {
    return downlineCount;
  }

  public void setDownlineCount(BigDecimal downlineCount) {
    this.downlineCount = downlineCount;
  }

  public int getNo() {
    return no;
  }

  public void setNo(int no) {
    this.no = no;
  }

}
