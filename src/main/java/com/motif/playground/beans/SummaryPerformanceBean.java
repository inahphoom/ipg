package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;

public class SummaryPerformanceBean implements Serializable {

  private static final long serialVersionUID = 2310681127329174031L;
  public String agentCode;
  public String scheduleId;
  public int ver;
  public String policyNumber;
  public String riderNumber;
  public int year;
  public String acFlag;
  public BigDecimal fyp;
  public BigDecimal afyp;
  public BigDecimal fyc;
  public BigDecimal afyc;
  public BigDecimal ryp;
  public BigDecimal aryp;
  public BigDecimal ryc;
  public BigDecimal total;
  public int no;

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public int getVer() {
    return ver;
  }

  public void setVer(int ver) {
    this.ver = ver;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public String getRiderNumber() {
    return riderNumber;
  }

  public void setRiderNumber(String riderNumber) {
    this.riderNumber = riderNumber;
  }

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public String getAcFlag() {
    return acFlag;
  }

  public void setAcFlag(String acFlag) {
    this.acFlag = acFlag;
  }

  public BigDecimal getFyp() {
    return fyp;
  }

  public void setFyp(BigDecimal fyp) {
    this.fyp = fyp;
  }

  public BigDecimal getAfyp() {
    return afyp;
  }

  public void setAfyp(BigDecimal afyp) {
    this.afyp = afyp;
  }

  public BigDecimal getFyc() {
    return fyc;
  }

  public void setFyc(BigDecimal fyc) {
    this.fyc = fyc;
  }

  public BigDecimal getAfyc() {
    return afyc;
  }

  public void setAfyc(BigDecimal afyc) {
    this.afyc = afyc;
  }

  public BigDecimal getRyp() {
    return ryp;
  }

  public void setRyp(BigDecimal ryp) {
    this.ryp = ryp;
  }

  public BigDecimal getAryp() {
    return aryp;
  }

  public void setAryp(BigDecimal aryp) {
    this.aryp = aryp;
  }

  public BigDecimal getRyc() {
    return ryc;
  }

  public void setRyc(BigDecimal ryc) {
    this.ryc = ryc;
  }

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

public int getNo() {
	return no;
}

public void setNo(int no) {
	this.no = no;
}

}
