package com.motif.playground.beans;

import java.io.Serializable;

public class TierBean implements Serializable {

  private static final long serialVersionUID = -5833670589769228519L;
  private String scheduleId;
  private String agentCode;
  private String uplineL1Code;
  private String display;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getUplineL1Code() {
    return uplineL1Code;
  }

  public void setUplineL1Code(String uplineL1Code) {
    this.uplineL1Code = uplineL1Code;
  }

  public String getDisplay() {
    return display;
  }

  public void setDisplay(String display) {
    this.display = display;
  }

}
