/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.beans;

import java.lang.reflect.Field;

/**
 *
 * @author Adichart
 */
public class BasicDropdownBean {
  protected String value;
  protected String message;

  public BasicDropdownBean(String value, String message) {
    this.value = value;
    this.message = message;
  }

  public String printAll() {
    StringBuilder result = new StringBuilder();
    String newLine = System.getProperty("line.separator");

    result.append(this.getClass().getName());
    result.append(" {");
    result.append(newLine);

    // determine fields declared in this class only (no fields of superclass)
    Field[] fields = this.getClass().getDeclaredFields();

    // print field names paired with their values
    for (Field field : fields) {
      result.append("\t");
      try {
        result.append(field.getName());
        result.append(" : ");
        // requires access to private field:
        result.append(field.get(this));
      } catch (IllegalAccessException ex) {
        System.out.println(ex);
      }
      result.append(newLine);
    }
    result.append("}");

    return result.toString();
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }
}
