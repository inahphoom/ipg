package com.motif.playground.beans;

import java.io.Serializable;

public class TierBeanICOM implements Serializable {

  private static final long serialVersionUID = -7221854132579453960L;

  private String scheduleId;
  private String agentCode;
  private String uplineL1Code;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public String getUplineL1Code() {
    return uplineL1Code;
  }

  public void setUplineL1Code(String uplineL1Code) {
    this.uplineL1Code = uplineL1Code;
  }
}
