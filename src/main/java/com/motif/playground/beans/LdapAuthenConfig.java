package com.motif.playground.beans;

public class LdapAuthenConfig {

  private static final String InitialContextFactory = "com.sun.jndi.ldap.LdapCtxFactory";


  /**
   * @return the initialcontextfactory
   */
  public static String getInitialcontextfactory() {
    return InitialContextFactory;
  }

  private String Protocol;
  private String ServerName;
  private int ServerPort;

  private String authenType;
  private String bindPrincipal;
  private String bindPassword;

  private String baseOu;
  private boolean validateCertificate = true;

  private String searchFilter;
  private String searchAttribute;

  private String authenticatingUser;


  /**
   * @return the authenType
   */
  public String getAuthenType() {
    return authenType;
  }


  /**
   * @return the baseOu
   */
  public String getBaseOu() {
    return baseOu;
  }


  /**
   * @return the bindPassword
   */
  public String getBindPassword() {
    return bindPassword;
  }


  /**
   * @return the bindPrincipal
   */
  public String getBindPrincipal() {
    return bindPrincipal;
  }


  /**
   * @return the protocol
   */
  public String getProtocol() {
    return Protocol;
  }


  public String getSearchFilter() {
    return searchFilter;
  }


  /**
   * @return the serverName
   */
  public String getServerName() {
    return ServerName;
  }


  /**
   * @return the serverPort
   */
  public int getServerPort() {
    return ServerPort;
  }


  /**
   * @return the validateCertificate
   */
  public boolean isValidateCertificate() {
    return validateCertificate;
  }


  /**
   * Please set this value using following list
   * <ul>
   * <li><strong>simple</strong> for username and password authentication</li>
   * <li><strong>none</strong> for no authentication</li>
   * <li><emphasis><strong>SASL_mech_name</strong></emphasis> which can be "Anonymous", "CRAM-MD5",
   * "Digest-MD5", "External", "Kerberos V4", "Kerberos V5", ""SecurID", "S/Key"</li>
   * </ul>
   *
   * @param authenType the authenType to set
   */
  public void setAuthenType(String authenType) {
    this.authenType = authenType;
  }


  /**
   * Set the folder in LDAP or AD where the users are. Simple LDAP or AD usually set to "ou=Users"
   * but many are set to something else.
   *
   * @param baseOu the baseOu to set
   */
  public void setBaseOu(String baseOu) {
    this.baseOu = baseOu;
  }


  /**
   * Set to the password of the user whose account is used to read LDAP or AD data.
   *
   * @param bindPassword the bindPassword to set
   */
  public void setBindPassword(String bindPassword) {
    this.bindPassword = bindPassword;
  }


  /**
   * Set to the username of the user whose account is used to read LDAP or AD data. Please be noted
   * that FQDN of the user is required. For example, CN=/<UserName/>,CN=Users,dc=domain,dc=domain.
   * The user here is not necessary in the same ou as the users we want to retrieve.
   *
   * @param bindPrincipal the bindPrincipal to set
   */
  public void setBindPrincipal(String bindPrincipal) {
    this.bindPrincipal = bindPrincipal;
  }


  /**
   * Set to LDAP protocal to be used. Accept values are "ldap" and "ldaps"
   *
   * @param protocol the protocol to set. It can either be "ldap" or "ldaps".
   */
  public void setProtocol(String protocol) {
    Protocol = protocol;
  }


  /**
   * Set user search filter.
   *
   * @param ldapSearchFilter user search filter.
   */
  public void setSearchFilter(String ldapSearchFilter) {
    searchFilter = ldapSearchFilter;
  }


  /**
   * Set name (or IP) of LDAP server.
   *
   * @param serverName the name of the LDAP Server
   */
  public void setServerName(String serverName) {
    ServerName = serverName;
  }


  /**
   * Set LDAP/LDAPS connection port.
   *
   * @param serverPort LDAP or AD connection port. Set it to <strong>389</strong> for ldap and
   *        <strong>636</strong> for ldaps. If you are having problems with AD Server which is also
   *        GC, set this to <strong>3268</strong> for ldap and <strong>3269</strong> for ldaps.
   */
  public void setServerPort(int serverPort) {
    ServerPort = serverPort;
  }


  /**
   * Not used at the moment. Please add LDAPS server certificate to java's trust store instead.
   *
   * @param validateCertificate the validateCertificate to set
   */
  public void setValidateCertificate(boolean validateCertificate) {
    this.validateCertificate = validateCertificate;
  }


  /**
   * @return the searchAttribute
   */
  public String getSearchAttribute() {
    return searchAttribute;
  }


  /**
   * @param searchAttribute the searchAttribute to set
   */
  public void setSearchAttribute(String searchAttribute) {
    this.searchAttribute = searchAttribute;
  }


  /**
   * @return the authenticatingUser
   */
  public String getAuthenticatingUser() {
    return authenticatingUser;
  }


  /**
   * @param authenticatingUser the authenticatingUser to set
   */
  public void setAuthenticatingUser(String authenticatingUser) {
    this.authenticatingUser = authenticatingUser;
  }


  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("LdapAuthenConfig [Protocol=");
    builder.append(Protocol);
    builder.append(", ServerName=");
    builder.append(ServerName);
    builder.append(", ServerPort=");
    builder.append(ServerPort);
    builder.append(", authenType=");
    builder.append(authenType);
    builder.append(", bindPrincipal=");
    builder.append(bindPrincipal);
    builder.append(bindPassword);
    builder.append(", baseOu=");
    builder.append(baseOu);
    builder.append(", validateCertificate=");
    builder.append(validateCertificate);
    builder.append(", searchFilter=");
    builder.append(searchFilter);
    builder.append(", searchAttribute=");
    builder.append(searchAttribute);
    builder.append(", authenticatingUser=");
    builder.append(authenticatingUser);
    builder.append("]");
    return builder.toString();
  }

}
