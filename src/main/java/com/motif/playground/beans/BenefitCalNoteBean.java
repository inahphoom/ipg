package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Data object for Benefit Calculation note (calnote).
 * 
 * @author wittaya
 * @since 2017.09.14
 * @version 2.1.1
 */
public class BenefitCalNoteBean implements Serializable {

  private static final long serialVersionUID = 3511594754230662123L;
  
  private String scheduleId;
  private BigDecimal ver;
  private BigDecimal benefitResultId;
  private BigDecimal calNoteId;
  private String agentCode;
  private String policyNo;
  private String riderNo;
  private String recalScheduleId;
  private String calNote;
  private String calValue;
  private BigDecimal createdBy;
  private Date createdOn;
  private BigDecimal updatedBy;
  private Date updatedOn;
  
  
  public BenefitCalNoteBean() {

  }

  public BenefitCalNoteBean(String scheduleId, BigDecimal ver, BigDecimal benefitResultId,
      BigDecimal calNoteId, String agentCode, String policyNo, String riderNo,
      String recalScheduleId, String calNote, String calValue, BigDecimal createdBy, Date createdOn,
      BigDecimal updatedBy, Date updatedOn) {
    super();
    this.scheduleId = scheduleId;
    this.ver = ver;
    this.benefitResultId = benefitResultId;
    this.calNoteId = calNoteId;
    this.agentCode = agentCode;
    this.policyNo = policyNo;
    this.riderNo = riderNo;
    this.recalScheduleId = recalScheduleId;
    this.calNote = calNote;
    this.calValue = calValue;
    this.createdBy = createdBy;
    this.createdOn = createdOn;
    this.updatedBy = updatedBy;
    this.updatedOn = updatedOn;
  }

  public String getScheduleId() {
    return scheduleId;
  }

  public BigDecimal getVer() {
    return ver;
  }

  public BigDecimal getBenefitResultId() {
    return benefitResultId;
  }

  public BigDecimal getCalNoteId() {
    return calNoteId;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public String getPolicyNo() {
    return policyNo;
  }

  public String getRiderNo() {
    return riderNo;
  }

  public String getRecalScheduleId() {
    return recalScheduleId;
  }

  public String getCalNote() {
    return calNote;
  }

  public String getCalValue() {
    return calValue;
  }

  public BigDecimal getCreatedBy() {
    return createdBy;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public BigDecimal getUpdatedBy() {
    return updatedBy;
  }

  public Date getUpdatedOn() {
    return updatedOn;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }

  public void setBenefitResultId(BigDecimal benefitResultId) {
    this.benefitResultId = benefitResultId;
  }

  public void setCalNoteId(BigDecimal calNoteId) {
    this.calNoteId = calNoteId;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public void setRiderNo(String riderNo) {
    this.riderNo = riderNo;
  }

  public void setRecalScheduleId(String recalScheduleId) {
    this.recalScheduleId = recalScheduleId;
  }

  public void setCalNote(String calNote) {
    this.calNote = calNote;
  }

  public void setCalValue(String calValue) {
    this.calValue = calValue;
  }

  public void setCreatedBy(BigDecimal createdBy) {
    this.createdBy = createdBy;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public void setUpdatedBy(BigDecimal updatedBy) {
    this.updatedBy = updatedBy;
  }

  public void setUpdatedOn(Date updatedOn) {
    this.updatedOn = updatedOn;
  }

  @Override
  public String toString() {
    return "BenefitCalNote [scheduleId=" + scheduleId + ", ver=" + ver + ", benefitResultId="
        + benefitResultId + ", calNoteId=" + calNoteId + ", agentCode=" + agentCode + ", policyNo="
        + policyNo + ", riderNo=" + riderNo + ", recalScheduleId=" + recalScheduleId + ", calNote="
        + calNote + ", calValue=" + calValue + ", createdBy=" + createdBy + ", createdOn="
        + createdOn + ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + "]";
  }

}
