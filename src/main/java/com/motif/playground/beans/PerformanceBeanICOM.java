/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

// import org.apache.log4j.Logger;

public class PerformanceBeanICOM implements Serializable {

  private static final long serialVersionUID = 5512816044217586322L;

  // Primary Keys
  private BigDecimal transactionId;
  private String scheduleId;
  private BigDecimal ver;

  // General fields
  private Date ackDate;
  private String agentCode;
  private BigDecimal agentPosId;
  private String inputType;
  private String policyNo;
  private String performanceMonth;
  private String performanceYear;
  private BigDecimal policyTypeId;
  private int policyYear;
  private BigDecimal prodAmount;
  private BigDecimal prodTypeId;
  private String riderCode;


  public String getPolicyNo() {
    return policyNo;
  }


  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }


  public BigDecimal getVer() {
    return ver;
  }


  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }


  public String getAgentCode() {
    return agentCode;
  }


  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }


  public BigDecimal getAgentPosId() {
    return agentPosId;
  }


  public void setAgentPosId(BigDecimal agentPosId) {
    this.agentPosId = agentPosId;
  }


  public BigDecimal getProdTypeId() {
    return prodTypeId;
  }


  public void setProdTypeId(BigDecimal prodTypeId) {
    this.prodTypeId = prodTypeId;
  }


  public BigDecimal getPolicyTypeId() {
    return policyTypeId;
  }


  public void setPolicyTypeId(BigDecimal policyTypeId) {
    this.policyTypeId = policyTypeId;
  }


  public BigDecimal getProdAmount() {
    return prodAmount;
  }


  public void setProdAmount(BigDecimal prodAmount) {
    this.prodAmount = prodAmount;
  }


  public String getInputType() {
    return inputType;
  }


  public void setInputType(String inputType) {
    this.inputType = inputType;
  }


  public String getScheduleId() {
    return scheduleId;
  }


  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }


  public int getPolicyYear() {
    return policyYear;
  }


  public void setPolicyYear(Integer policyYear) {
    if (null == policyYear) {
      this.policyYear = 0;
    } else {
      this.policyYear = policyYear.intValue();
    }
    // this.policyYear = policyYear;
  }


  public String getRiderCode() {
    return riderCode;
  }


  public void setRiderCode(String riderCode) {
    this.riderCode = riderCode;
  }


  public Date getAckDate() {
    return ackDate;
  }


  public void setAckDate(Date ackDate) {
    this.ackDate = ackDate;
  }


  public String getPerformanceMonth() {
    return performanceMonth;
  }


  public void setPerformanceMonth(String performanceMonth) {
    this.performanceMonth = performanceMonth;
  }


  public String getPerformanceYear() {
    return performanceYear;
  }


  public void setPerformanceYear(String performanceYear) {
    this.performanceYear = performanceYear;
  }

  
  public BigDecimal getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(BigDecimal transactionId) {
    this.transactionId = transactionId;
  }

}
