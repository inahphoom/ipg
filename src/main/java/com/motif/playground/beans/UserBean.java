package com.motif.playground.beans;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 10024111
 *
 */
public class UserBean {

  private BigDecimal userId;
  private String loginName;
  private String password;
  private String isDeleted;
  private String createBy;
  private Date createDate;
  private String updateBy;
  private Date updateDate;
  private String userType;
  private boolean isAdmin = false;

  public BigDecimal getUserId() {
    return userId;
  }

  public void setUserId(BigDecimal userId) {
    this.userId = userId;
  }

  public String getLoginName() {
    return loginName;
  }

  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(String isDeleted) {
    this.isDeleted = isDeleted;
  }

  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }
  
  public String toString() {
    return   "userId=" + userId + "; loginName=" + loginName + "; isDeleted=" + isDeleted 
        + "; userType=" + userType + "; isAdmin=" + isAdmin;
  }

}
