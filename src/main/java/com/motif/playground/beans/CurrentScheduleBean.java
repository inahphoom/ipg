package com.motif.playground.beans;

import java.io.Serializable;
import java.util.Date;

public class CurrentScheduleBean implements Serializable {

  private static final long serialVersionUID = 8553069762088479616L;
  private String scheduleId;
  private String version;
  private Date from;
  private Date to;
  private String desc;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public Date getFrom() {
    return from;
  }

  public void setFrom(Date from) {
    this.from = from;
  }

  public Date getTo() {
    return to;
  }

  public void setTo(Date to) {
    this.to = to;
  }

}
