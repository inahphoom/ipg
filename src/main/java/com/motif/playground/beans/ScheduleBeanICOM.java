package com.motif.playground.beans;

import java.math.BigDecimal;
import java.util.Date;

public class ScheduleBeanICOM {

  private String scheduleId;
  private Date periodFrom;
  private Date periodTo;
  private Date paymentDate;
  private String cycleName;
  private BigDecimal createBy;
  private Date createDate;
  private BigDecimal updateBy;
  private Date updateDate;
  private String scheduleDesc;
  private String isPaid;
  private String isDeleted;
  private String periodYear;
  private String periodMonth;

  public String getScheduleId() {
    return scheduleId;
  }

  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }

  public Date getPeriodFrom() {
    return periodFrom;
  }

  public void setPeriodFrom(Date periodFrom) {
    this.periodFrom = periodFrom;
  }

  public Date getPeriodTo() {
    return periodTo;
  }

  public void setPeriodTo(Date periodTo) {
    this.periodTo = periodTo;
  }

  public Date getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(Date paymentDate) {
    this.paymentDate = paymentDate;
  }

  public String getCycleName() {
    return cycleName;
  }

  public void setCycleName(String cycleName) {
    this.cycleName = cycleName;
  }

  public BigDecimal getCreateBy() {
    return createBy;
  }

  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public BigDecimal getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(BigDecimal updateBy) {
    this.updateBy = updateBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public String getScheduleDesc() {
    return scheduleDesc;
  }

  public void setScheduleDesc(String scheduleDesc) {
    this.scheduleDesc = scheduleDesc;
  }

  public String getIsPaid() {
    return isPaid;
  }

  public void setIsPaid(String isPaid) {
    this.isPaid = isPaid;
  }

  public String getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(String isDeleted) {
    this.isDeleted = isDeleted;
  }

  public String getPeriodYear() {
    return periodYear;
  }

  public void setPeriodYear(String periodYear) {
    this.periodYear = periodYear;
  }

  public String getPeriodMonth() {
    return periodMonth;
  }

  public void setPeriodMonth(String periodMonth) {
    this.periodMonth = periodMonth;
  }

}
