package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BenefitBean implements Serializable {

  private static final long serialVersionUID = -1926166140265342095L;

  private String calScheduleId;
  private BigDecimal ver;
  private BigDecimal benefitResultId;
  private BigDecimal benefitTypeId;
  private String benefitCode;
  private String agentCode;
  private BigDecimal benefitAmount;
  private String policyNo;
  private String riderNo;
  private String recalScheduleId;
  private String isDeleted;
  private Date createDate;
  private BigDecimal createBy;
  private Date updateDate;
  private BigDecimal updateBy;

  public String getCalScheduleId() {
    return calScheduleId;
  }

  public void setCalScheduleId(String calScheduleId) {
    this.calScheduleId = calScheduleId;
  }

  public BigDecimal getVer() {
    return ver;
  }

  public void setVer(BigDecimal ver) {
    this.ver = ver;
  }

  public BigDecimal getBenefitResultId() {
    return benefitResultId;
  }

  public void setBenefitResultId(BigDecimal benefitResultId) {
    this.benefitResultId = benefitResultId;
  }

  public BigDecimal getBenefitTypeId() {
    return benefitTypeId;
  }

  public void setBenefitTypeId(BigDecimal benefitTypeId) {
    this.benefitTypeId = benefitTypeId;
  }

  public String getBenefitCode() {
    return benefitCode;
  }

  public void setBenefitCode(String benefitCode) {
    this.benefitCode = benefitCode;
  }

  public String getAgentCode() {
    return agentCode;
  }

  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  public BigDecimal getBenefitAmount() {
    return benefitAmount;
  }

  public void setBenefitAmount(BigDecimal benefitAmount) {
    this.benefitAmount = benefitAmount;
  }

  public String getPolicyNo() {
    return policyNo;
  }

  public void setPolicyNo(String policyNo) {
    this.policyNo = policyNo;
  }

  public String getRiderNo() {
    return riderNo;
  }

  public void setRiderNo(String riderNo) {
    this.riderNo = riderNo;
  }

  public String getRecalScheduleId() {
    return recalScheduleId;
  }

  public void setRecalScheduleId(String recalScheduleId) {
    this.recalScheduleId = recalScheduleId;
  }

  public String getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(String isDeleted) {
    this.isDeleted = isDeleted;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public BigDecimal getCreateBy() {
    return createBy;
  }

  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public BigDecimal getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(BigDecimal updateBy) {
    this.updateBy = updateBy;
  }

}
