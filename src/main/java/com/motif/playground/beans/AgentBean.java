/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.beans;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adichart
 */
public class AgentBean {
  protected String agentCode;
  protected String name;
  protected String surname;
  protected String nameTH;
  protected String surnameTH;
  protected String channel;
  protected String position;
  protected String status;
  protected String statusDesc;
  protected List<AgentBean> downlineList;
  protected AgentBean uplineAgent;

  /**
   * @return the agentCode
   */
  public String getAgentCode() {
    return agentCode;
  }

  /**
   * @param agentCode the agentCode to set
   */
  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the surname
   */
  public String getSurname() {
    return surname;
  }

  /**
   * @param surname the surname to set
   */
  public void setSurname(String surname) {
    this.surname = surname;
  }

  /**
   * @return the nameTH
   */
  public String getNameTH() {
    return nameTH;
  }

  /**
   * @param nameTH the nameTH to set
   */
  public void setNameTH(String nameTH) {
    this.nameTH = nameTH;
  }

  /**
   * @return the surnameTH
   */
  public String getSurnameTH() {
    return surnameTH;
  }

  /**
   * @param surnameTH the surnameTH to set
   */
  public void setSurnameTH(String surnameTH) {
    this.surnameTH = surnameTH;
  }

  /**
   * @return the channel
   */
  public String getChannel() {
    return channel;
  }

  /**
   * @param channel the channel to set
   */
  public void setChannel(String channel) {
    this.channel = channel;
  }

  /**
   * @return the position
   */
  public String getPosition() {
    return position;
  }

  /**
   * @param position the position to set
   */
  public void setPosition(String position) {
    this.position = position;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
    if ("A".equalsIgnoreCase(status)) {
      this.statusDesc = "Active";
    } else if ("I".equalsIgnoreCase(status)) {
      this.statusDesc = "Inactive";
    } else {
      this.statusDesc = "Unknown";
    }
  }

  /**
   * @return the downlineList
   */
  public List<AgentBean> getDownlineList() {
    if (downlineList == null) {
      downlineList = new ArrayList<AgentBean>();
    }
    return downlineList;
  }

  /**
   * @param downlineList the downlineList to set
   */
  public void setDownlineList(List<AgentBean> downlineList) {
    this.downlineList = downlineList;
  }

  /**
   * @return the uplineAgent
   */
  public AgentBean getUplineAgent() {
    return uplineAgent;
  }

  /**
   * @param uplineAgent the uplineAgent to set
   */
  public void setUplineAgent(AgentBean uplineAgent) {
    this.uplineAgent = uplineAgent;
  }

  public String getStatusDesc() {
    return statusDesc;
  }
}
