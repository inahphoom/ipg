package com.motif.playground.beans;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class AgentsBean implements Serializable {

  private static final long serialVersionUID = 4786798929375237582L;
  private String agentCode;
  private String scheduleId;
  private String nationalId;
  private String agentTitleTha;
  private String agentFirstnameTha;
  private String agentLastnameTha;
  private String agentTitleEng;
  private String agentFirstnameEng;
  private String agentLastnameEng;
  private Date dob;
  private String gender;
  private String agentStatus;
  private String agentStatusDesc;
  private Date appointmentDate;
  private Date contractDate;
  private String addr1;
  private String addr2;
  private String addr3;
  private String addr4;
  private String zipCode;
  private String homePhone;
  private String officePhone;
  private String mobilePhone;
  private String email;
  private Date effectiveDate;
  private String licenseCode;
  private Date startDate;
  private Date expireDate;
  private Date firstIssuedDate;
  private BigDecimal createBy;
  private Date createDate;
  private BigDecimal updateBy;
  private Date updateDate;
  private BigDecimal agentPosId;
  private Date terminateDate;
  private String terminateReason;
  private String isIndividual;
  private String uplineCode;
  private String recruiterCode;
  private BigDecimal workMonth;
  private String saleChannelCode;
  private String position;
  private String nameDisplay;
  private List<DownlineAgentsBean> downlineList;
  private AgentsBean uplineAgent;


  public String getAgentCode() {
    return agentCode;
  }


  public void setAgentCode(String agentCode) {
    this.agentCode = agentCode;
  }


  public String getScheduleId() {
    return scheduleId;
  }


  public void setScheduleId(String scheduleId) {
    this.scheduleId = scheduleId;
  }


  public String getNationalId() {
    return nationalId;
  }


  public void setNationalId(String nationalId) {
    this.nationalId = nationalId;
  }


  public String getAgentTitleTha() {
    return agentTitleTha;
  }


  public void setAgentTitleTha(String agentTitleTha) {
    this.agentTitleTha = agentTitleTha;
  }


  public String getAgentFirstnameTha() {
    return agentFirstnameTha;
  }


  public void setAgentFirstnameTha(String agentFirstnameTha) {
    this.agentFirstnameTha = agentFirstnameTha;
  }


  public String getAgentLastnameTha() {
    return agentLastnameTha;
  }


  public void setAgentLastnameTha(String agentLastnameTha) {
    this.agentLastnameTha = agentLastnameTha;
  }


  public String getAgentTitleEng() {
    return agentTitleEng;
  }


  public void setAgentTitleEng(String agentTitleEng) {
    this.agentTitleEng = agentTitleEng;
  }


  public String getAgentFirstnameEng() {
    return agentFirstnameEng;
  }


  public void setAgentFirstnameEng(String agentFirstnameEng) {
    this.agentFirstnameEng = agentFirstnameEng;
  }


  public String getAgentLastnameEng() {
    return agentLastnameEng;
  }


  public void setAgentLastnameEng(String agentLastnameEng) {
    this.agentLastnameEng = agentLastnameEng;
  }


  public Date getDob() {
    return dob;
  }


  public void setDob(Date dob) {
    this.dob = dob;
  }


  public String getGender() {
    return gender;
  }


  public void setGender(String gender) {
    this.gender = gender;
  }


  public String getAgentStatus() {
    return agentStatus;
  }


  public void setAgentStatus(String agentStatus) {
    this.agentStatus = agentStatus;
    if ("A".equalsIgnoreCase(agentStatus)) {
      this.agentStatusDesc = "Active";
    } else if ("I".equalsIgnoreCase(agentStatus)) {
      this.agentStatusDesc = "Inactive";
    } else {
      this.agentStatus = "Unknown";
    }
  }


  public Date getAppointmentDate() {
    return appointmentDate;
  }


  public void setAppointmentDate(Date appointmentDate) {
    this.appointmentDate = appointmentDate;
  }


  public Date getContractDate() {
    return contractDate;
  }


  public void setContractDate(Date contractDate) {
    this.contractDate = contractDate;
  }


  public String getAddr1() {
    return addr1;
  }


  public void setAddr1(String addr1) {
    this.addr1 = addr1;
  }


  public String getAddr2() {
    return addr2;
  }


  public void setAddr2(String addr2) {
    this.addr2 = addr2;
  }


  public String getAddr3() {
    return addr3;
  }


  public void setAddr3(String addr3) {
    this.addr3 = addr3;
  }


  public String getAddr4() {
    return addr4;
  }


  public void setAddr4(String addr4) {
    this.addr4 = addr4;
  }


  public String getZipCode() {
    return zipCode;
  }


  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }


  public String getHomePhone() {
    return homePhone;
  }


  public void setHomePhone(String homePhone) {
    this.homePhone = homePhone;
  }


  public String getOfficePhone() {
    return officePhone;
  }


  public void setOfficePhone(String officePhone) {
    this.officePhone = officePhone;
  }


  public String getMobilePhone() {
    return mobilePhone;
  }


  public void setMobilePhone(String mobilePhone) {
    this.mobilePhone = mobilePhone;
  }


  public String getEmail() {
    return email;
  }


  public void setEmail(String email) {
    this.email = email;
  }


  public Date getEffectiveDate() {
    return effectiveDate;
  }


  public void setEffectiveDate(Date effectiveDate) {
    this.effectiveDate = effectiveDate;
  }


  public String getLicenseCode() {
    return licenseCode;
  }


  public void setLicenseCode(String licenseCode) {
    this.licenseCode = licenseCode;
  }


  public Date getStartDate() {
    return startDate;
  }


  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }


  public Date getExpireDate() {
    return expireDate;
  }


  public void setExpireDate(Date expireDate) {
    this.expireDate = expireDate;
  }


  public Date getFirstIssuedDate() {
    return firstIssuedDate;
  }


  public void setFirstIssuedDate(Date firstIssuedDate) {
    this.firstIssuedDate = firstIssuedDate;
  }


  public BigDecimal getCreateBy() {
    return createBy;
  }


  public void setCreateBy(BigDecimal createBy) {
    this.createBy = createBy;
  }


  public Date getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }


  public BigDecimal getUpdateBy() {
    return updateBy;
  }


  public void setUpdateBy(BigDecimal updateBy) {
    this.updateBy = updateBy;
  }


  public Date getUpdateDate() {
    return updateDate;
  }


  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }


  public BigDecimal getAgentPosId() {
    return agentPosId;
  }


  public void setAgentPosId(BigDecimal agentPosId) {
    this.agentPosId = agentPosId;
  }


  public Date getTerminateDate() {
    return terminateDate;
  }


  public void setTerminateDate(Date terminateDate) {
    this.terminateDate = terminateDate;
  }


  public String getTerminateReason() {
    return terminateReason;
  }


  public void setTerminateReason(String terminateReason) {
    this.terminateReason = terminateReason;
  }


  public String getIsIndividual() {
    return isIndividual;
  }


  public void setIsIndividual(String isIndividual) {
    this.isIndividual = isIndividual;
  }


  public String getUplineCode() {
    return uplineCode;
  }


  public void setUplineCode(String uplineCode) {
    this.uplineCode = uplineCode;
  }


  public String getRecruiterCode() {
    return recruiterCode;
  }


  public void setRecruiterCode(String recruiterCode) {
    this.recruiterCode = recruiterCode;
  }


  public BigDecimal getWorkMonth() {
    return workMonth;
  }


  public void setWorkMonth(BigDecimal workMonth) {
    this.workMonth = workMonth;
  }


  public String getSaleChannelCode() {
    return saleChannelCode;
  }


  public void setSaleChannelCode(String saleChannelCode) {
    this.saleChannelCode = saleChannelCode;
  }


  public String getPosition() {
    return position;
  }


  public void setPosition(String position) {
    this.position = position;
  }


  public String getNameDisplay() {
    return nameDisplay;
  }


  public void setNameDisplay(String nameDisplay) {
    this.nameDisplay = nameDisplay;
  }


  public List<DownlineAgentsBean> getDownlineList() {
    return downlineList;
  }


  public void setDownlineList(List<DownlineAgentsBean> downlineList) {
    this.downlineList = downlineList;
  }


  public AgentsBean getUplineAgent() {
    return uplineAgent;
  }


  public void setUplineAgent(AgentsBean uplineAgent) {
    this.uplineAgent = uplineAgent;
  }


  /**
   * Get description of agent's status. It should return 'Active', 'Inactive' and so forth. This
   * property is read-only. You can only set it via setAgentStatus()
   * 
   * @return String describing agent's status.
   */
  public String getAgentStatusDesc() {
    return agentStatusDesc;
  }


}
