package com.motif.playground.interceptor;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

import java.util.Map;

//import org.apache.log4j.Logger;

public class LoginInterceptor extends AbstractInterceptor {

  private static final long serialVersionUID = 4156157387668913112L;

  @Override
  public String intercept(ActionInvocation invocation) throws Exception {
    Map<String, Object> session = invocation.getInvocationContext().getSession();

    String loginId = (String) session.get("loginId");

    if (loginId == null) {
      return Action.LOGIN;
    } else {
      return invocation.invoke();
    }
  }

}
