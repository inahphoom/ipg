/**
 * 
 */

package com.motif.playground.dao;

import com.motif.playground.beans.UserBean;

/**
 * Interface for user authenticaation management
 * 
 * @author Nah, Bas
 *
 */
public interface UserDAO {

  /**
   * Carry out an authentication action.
   * 
   * @param userName the login name of the user.
   * @param password non-encrypted password of the user.
   * @return User information bean.
   */
  UserBean login(String userName, String password);

  /**
   * Check whether this user should be validated internally or validated with LDAP.
   * 
   * @param userName user to be checked.
   * @return <strong>True</strong> if the user must be validated internally or
   *         <strong>false</strong> if LDAP is required for validation.
   */
  boolean isInternalUser(String userName);

  /**
   * Get the role of user.
   * 
   * @param userName The user to be checked.
   * @return String identifying user group (either 'IPG_ADMIN' or 'IPG_USER') of specified user.
   *         Empty string will be return if user is not in any group.
   */
  String getRole(String userName);

}
