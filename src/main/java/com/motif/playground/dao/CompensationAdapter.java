package com.motif.playground.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import com.motif.playground.beans.BenefitCalNote;

/**
 * Interface of listing data from iCompensation Main dataase.
 * 
 * @author wittaya
 *
 */
public interface CompensationAdapter {
  static final int batchSizeIcomToStaging = 10000;

  public List<BenefitCalNote> listCalNoteIcomDb(String scheduleId, BigDecimal newVer)
      throws ClassNotFoundException, SQLException;

  void beginTransaction();

  void commitTransaction();

  void rollbackTransaction();

}
