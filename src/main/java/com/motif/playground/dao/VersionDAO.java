package com.motif.playground.dao;

import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;

import java.util.List;

/**
 * Interface for schedule version management.
 * @author Nah
 *
 */
public interface VersionDAO {

  /**
   * List all schedule versions specified by scheduleId from iPlayground main database.
   * 
   * @param scheduleId The schedule id of which versions are to be listed.
   * @return List of versions. In case of error, it should return empty list.
   */
  public List<VersionBean> listVersion(String scheduleId);

  /**
   * List all schedule versions specified by scheduleId from iPlayground Staging database.
   * 
   * @param scheduleId The schedule id of which versions are to be listed.
   * @return List of versions. In case of error, it should return empty list.
   */
  public List<VersionBeanICOM> listVersionICOM(String scheduleId);

}
