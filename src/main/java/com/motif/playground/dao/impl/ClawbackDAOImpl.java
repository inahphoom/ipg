
package com.motif.playground.dao.impl;

import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.dao.ClawbackDAO;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class ClawbackDAOImpl extends HibernateDaoSupport implements ClawbackDAO {
  private static Logger log = Logger.getLogger(ClawbackDAOImpl.class);

  @Override
  public List<PolicyClawbackBean> listAgentClawback(String scheduleId,
      int version, String policyNo, String riderNo) {

    List<PolicyClawbackBean> lstAgentClawback;
    String sql = "SELECT ROWNUM AS No,C.TRANSACTION_ID,C.SCHEDULE_ID, C.VER, C.CLAWBACK_ID, C.AGENT_CODE, C.POLICY_NO,  "
        + "     C.RIDER_NO, C.RECAL_SCHEDULE_ID, C.CREATE_DATE, C.CREATE_BY, C.UPDATE_DATE,  "
        + "     C.UPDATE_BY, M.DESCRIPTION AGENT_POSITION,C.PROD_TYPE_ID "
        + " FROM IPG.IPG_CLAWBACK_AGENT C "
        + "   INNER JOIN IPG.IPG_PSN_INF A ON C.AGENT_CODE = A.AGENT_CODE "
        + "   INNER JOIN ipg.ipg_master_code M ON A.AGENT_POS_ID = M.TYPE_ID "
        + " WHERE POLICY_NO LIKE :policyNo  "
        + "   AND C.SCHEDULE_ID = :scheduleId "
        + "   AND VER = :version "
        + "   AND code_group = 'POSITION TYPE'";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(PolicyClawbackBean.class);
      query.setParameter("policyNo", "%" + policyNo + "%");
      query.setParameter("scheduleId", scheduleId);
      query.setParameter("version", version);
      lstAgentClawback = query.list();
    } catch (DataAccessResourceFailureException exDaf) {
      log.error("An error occurred. Stack trace is:", exDaf);
      throw exDaf;
    } catch (HibernateException exHib) {
      log.error("An error occurred. Stack trace is:", exHib);
      throw exHib;
    } catch (IllegalStateException exIs) {
      log.error("An error occurred. Stack trace is:", exIs);
      throw exIs;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    } 
    return lstAgentClawback;
  }
}
