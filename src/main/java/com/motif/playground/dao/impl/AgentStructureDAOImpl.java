
package com.motif.playground.dao.impl;

import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.DownlineAgentsBean;
import com.motif.playground.beans.SummaryPerformanceBean;
// import com.motif.playground.beans.TierBean;
import com.motif.playground.beans.TierBeanDownLine;
import com.motif.playground.dao.AgentStructureDAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class AgentStructureDAOImpl extends HibernateDaoSupport implements AgentStructureDAO {

  private static Logger log = Logger.getLogger(AgentStructureDAOImpl.class);
  AgentStructureDAO agentStructureDAO;


  @Override
  public List<AgentsBean> searchAgentById(String agentCode) {

    List<AgentsBean> listAgent = null;
    String sql = "SELECT AGENT.*, MAS.DESCRIPTION AS POSITION "
        + " FROM IPG.IPG_PSN_INF AGENT "
        + "   INNER JOIN IPG.IPG_MASTER_CODE MAS ON AGENT.AGENT_POS_ID = MAS.TYPE_ID "
        + "     AND MAS.CODE_GROUP = 'POSITION TYPE' "
        + " WHERE AGENT_CODE LIKE :agentCode ORDER BY AGENT.AGENT_CODE";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(AgentsBean.class);
      query.setParameter("agentCode", "%" + agentCode + "%");
      listAgent = query.list();
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return listAgent;
  }


  @Override
  public List<AgentsBean> searchAgentByName(String agentName) {

    List<AgentsBean> listAgent = new ArrayList<AgentsBean>();
    String sql = "SELECT AGENT.*, MAS.DESCRIPTION AS POSITION "
        + " FROM IPG.IPG_PSN_INF AGENT "
        + "   INNER JOIN IPG.IPG_MASTER_CODE MAS ON AGENT.AGENT_POS_ID = MAS.TYPE_ID "
        + "     AND MAS.CODE_GROUP = 'POSITION TYPE' "
        + " WHERE AGENT_FIRSTNAME_THA LIKE :agentName "
        + "   OR AGENT_LASTNAME_THA LIKE :agentName "
        + "   OR AGENT_FIRSTNAME_ENG LIKE :agentName "
        + "   OR AGENT_LASTNAME_ENG LIKE :agentName "
        + " ORDER BY AGENT.AGENT_CODE ";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(AgentsBean.class);
      query.setParameter("agentName", "%" + agentName + "%");
      listAgent = query.list();
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return listAgent;
  }


  @Override
  public AgentsBean getAgentInfo(String agentCode) {

    AgentsBean agent;
    String sql = "SELECT AGENT.*, MAS.DESCRIPTION AS POSITION "
        + " FROM IPG.IPG_PSN_INF AGENT "
        + "   INNER JOIN IPG.IPG_MASTER_CODE MAS ON AGENT.AGENT_POS_ID = MAS.TYPE_ID "
        + " WHERE AGENT_CODE = :agentCode "
        + "   AND MAS.CODE_GROUP = 'POSITION TYPE' ";
    Session session = null;
    List<?> lst;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(AgentsBean.class);
      query.setParameter("agentCode", agentCode);
      lst = query.list();
      agent = null;
      if (lst.size() > 0) {
        agent = (AgentsBean) lst.get(0);
      }
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      this.releaseSession(session);
    }

    return agent;
  }


  @Override
  public List<SummaryPerformanceBean> listPerformance(String agentCode, String scheduleId,
      int ver) {

    List<SummaryPerformanceBean> performanceList;
    String sql = "SELECT AGENT_CODE, SCHEDULE_ID, VER, POLICY_NO, RIDER_NO,"
        + "    COALESCE(POLICY_YEAR,0) AS POLICY_YEAR, ACK_DATE,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 1 THEN PROD_AMOUNT END) AS FYP,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 2 THEN PROD_AMOUNT END) AS AFYP,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 6 THEN PROD_AMOUNT END) AS FYC,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 7 THEN PROD_AMOUNT END) AS AFYC,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 4 THEN PROD_AMOUNT END) AS RYP,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 5 THEN PROD_AMOUNT END) AS ARYP,"
        + "    SUM(CASE WHEN PROD_TYPE_ID = 8 THEN PROD_AMOUNT END) AS RYC"
        + " FROM IPG.IPG_PSN_PROD"
        + " WHERE AGENT_CODE = :agentCode"
        + "    AND SCHEDULE_ID = :scheduleId AND VER = :ver"
        + " GROUP BY AGENT_CODE, SCHEDULE_ID,VER,POLICY_NO,RIDER_NO,POLICY_YEAR,ACK_DATE";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(SummaryPerformanceBean.class);
      query.setParameter("agentCode", agentCode);
      query.setParameter("scheduleId", scheduleId);
      query.setParameter("ver", ver);
      performanceList = query.list();
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }catch (Exception e){
    	log.error("An error occurred. Stack trace is:", e);
        throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    return performanceList;
  }


  @Override
  public List<DownlineAgentsBean> listDownlineAgents(String agentCode, String scheduleId) {

    List<DownlineAgentsBean> resultList;
    String sql = " SELECT ROWNUM AS No,T.AGENT_CODE, A.AGENT_TITLE_ENG, A.AGENT_FIRSTNAME_ENG,"
        + " A.AGENT_LASTNAME_ENG, A.AGENT_TITLE_THA, A.AGENT_FIRSTNAME_THA, A.AGENT_LASTNAME_THA,"
        + " M.CODE, (SELECT COUNT(AGENT_CODE) FROM IPG.IPG_PSN_TIER T2 "
        + " WHERE UPLINE_L1_CODE = T.AGENT_CODE) AS CNT FROM IPG.IPG_PSN_TIER T"
        + " LEFT JOIN IPG.IPG_PSN_INF A ON A.AGENT_CODE = T.AGENT_CODE"
        + " LEFT JOIN IPG.IPG_MASTER_CODE M ON A.AGENT_POS_ID = M.TYPE_ID"
        + " AND M.CODE_GROUP = 'POSITION TYPE' WHERE UPLINE_L1_CODE = :agentCode"
        + " and A.SCHEDULE_ID = :scheduleId and t.AGENT_CODE <> t.UPLINE_L1_CODE ";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(DownlineAgentsBean.class);
      query.setParameter("agentCode", agentCode);
      query.setParameter("scheduleId", scheduleId);
      resultList = query.list();
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return resultList;
  }


  /**
   * get an agent who is the upline of specified agent.
   * 
   * @param agentCode code of the agent to be searched for his/her/its upline.
   * @return agent bean of the upline or new agent bean object of the upline.
   */
  @Override
  public AgentsBean findUplineAgent(String agentCode) {

    AgentsBean agentsBean = null;
    String sql = "SELECT * FROM IPG.IPG_PSN_TIER WHERE AGENT_CODE = :agentCode";
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(TierBeanDownLine.class);
      query.setParameter("agentCode", agentCode);
      TierBeanDownLine result = null;
      if (query.list().size() > 0) {
        result = (TierBeanDownLine) query.list().get(0);
        agentsBean = getAgentInfo(result.getUplineL1Code());
      } else {
        // Ensure that this method will not return null because it create some weird bug at front.
        agentsBean = new AgentsBean();
      }
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return agentsBean;

  }


  public void setAgentStructureDAO(AgentStructureDAO agentStructureDAO) {
    this.agentStructureDAO = agentStructureDAO;
  }

}
