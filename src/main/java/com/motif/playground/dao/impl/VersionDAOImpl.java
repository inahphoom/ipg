package com.motif.playground.dao.impl;

import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.dao.VersionDAO;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class VersionDAOImpl extends HibernateDaoSupport implements VersionDAO {
  private static Logger log = Logger.getLogger(VersionDAOImpl.class);

  @Override
  public List<VersionBean> listVersion(String scheduleId)
      throws DataAccessResourceFailureException, HibernateException, IllegalStateException {

    List<VersionBean> listVersion = new ArrayList<VersionBean>();
    String sql = "SELECT * FROM IPG.IPG_SCHEDULE_VER WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(VersionBean.class);
      query.setParameter("scheduleId", scheduleId);
      listVersion = query.list();
    } catch (DataAccessResourceFailureException ex) {
      log.error("An error occurred. Stack trace is:", ex);
      throw ex;
    } catch (HibernateException exHib) {
      log.error("An error occurred. Stack trace is:", exHib);
      throw exHib;
    } catch (IllegalStateException exIs) {
      log.error("An error occurred. Stack trace is:", exIs);
      throw exIs;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing;
      }
    }

    return listVersion;
  }

  @Override
  public List<VersionBeanICOM> listVersionICOM(String scheduleId) {

    List<VersionBeanICOM> listVersion;
    String sql = "SELECT * FROM ICOM.ICOM_SCHEDULE_VER WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;
    
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(VersionBeanICOM.class);
      query.setParameter("scheduleId", scheduleId);
      listVersion = query.list();
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return listVersion;
  }

}
