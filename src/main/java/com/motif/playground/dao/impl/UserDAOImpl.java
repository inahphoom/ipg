/**
 *
 */

package com.motif.playground.dao.impl;

import com.motif.playground.beans.UserBean;
import com.motif.playground.dao.UserDAO;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import util.Constants;
import util.DataEncryption;

public class UserDAOImpl extends HibernateDaoSupport implements UserDAO {
  private static Logger log = Logger.getLogger(UserDAOImpl.class);


  @Override
  public UserBean login(String userName, String password)
      throws DataAccessResourceFailureException, HibernateException, IllegalStateException {

    UserBean user;
    String sql = "select * from IPG.IPG_USER where LOGIN_NAME = :userName";
    Session session = null;

    try {
      user = null;
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(UserBean.class);
      query.setParameter("userName", userName);
      List<UserBean> lstuser = query.list();
      try {
        // TODO: we should not retrieve password from database. Encode user-supplied password first
        // and then use it as query parameter.
        if (lstuser.size() > 0) {
          user = lstuser.get(0);
          String pwdInput = DataEncryption.convertToHex(password);
          if (user != null) {
            String pwdDb = user.getPassword();
            if (!pwdInput.equals(pwdDb)) {
              user = null;
            }
          }
        }
      } catch (Exception ex) {
        logger.error("An error occurred while logging in. Stack trace is:", ex);
      }
    } catch (DataAccessResourceFailureException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (HibernateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } catch (IllegalStateException e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return user;
  }


  @Override
  public boolean isInternalUser(String userName) {

    String sql = "select * from IPG.IPG_USER where LOGIN_NAME = :userName";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(UserBean.class);
      query.setParameter("userName", userName);
      UserBean bean = (UserBean) query.uniqueResult();

      if (bean != null) {
        log.debug("USER_TYPE : " + bean.getUserType());
        if (Constants.USER_TYPE_INTERNAL.equalsIgnoreCase(StringUtils.trim(bean.getUserType()))) {
          return true;
        }
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while getting user information. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return false;
  }


  @Override
  public String getRole(String userName) {

    String role = "";
    String sql = "SELECT G.GROUP_NAME FROM IPG.IPG_USER U JOIN IPG.IPG_GROUP_USERS GU"
        + " ON U.USER_ID = GU.USER_ID JOIN IPG.IPG_GROUPS G ON GU.GROUP_ID = G.GROUP_ID"
        + " WHERE U.LOGIN_NAME = :userName";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("userName", userName);
      List<String> listResult = query.list();
      try {
        if (listResult.size() > 0) {
          role = listResult.get(0);
        }
      } catch (Exception ex) {
        logger.error("An error occurred while logging in. Stack trace is:", ex);
        throw ex;
      }
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing
      }
    }

    return role;
  }

}
