package com.motif.playground.dao.impl;

import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.AgentsBeanICOM;
import com.motif.playground.beans.AgentsBeanICOMDB;
import com.motif.playground.beans.BenefitBean;
import com.motif.playground.beans.BenefitBeanICOM;
import com.motif.playground.beans.BenefitBeanICOMDB;
import com.motif.playground.beans.BenefitCalNote;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.MasterBean;
import com.motif.playground.beans.MasterBeanICOM;
import com.motif.playground.beans.MasterBeanICOMDB;
import com.motif.playground.beans.MasterCodeBean;
import com.motif.playground.beans.PerformanceBean;
import com.motif.playground.beans.PerformanceBeanICOM;
import com.motif.playground.beans.PerformanceBeanICOMDB;
import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.beans.PolicyClawbackBeanICOM;
import com.motif.playground.beans.PolicyClawbackBeanICOMDB;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.ScheduleBeanICOM;
import com.motif.playground.beans.ScheduleBeanICOMDB;
import com.motif.playground.beans.TierBean;
import com.motif.playground.beans.TierBeanICOM;
import com.motif.playground.beans.TierBeanICOMDB;
import com.motif.playground.beans.UserBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.dao.ScheduleDAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.jdbc.Work;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import util.ConnectionManager;


public class ScheduleDAOImpl extends HibernateDaoSupport implements ScheduleDAO {

  static final Logger logger = Logger.getLogger(ScheduleDAOImpl.class);
  private static final int batchSizeStagingToIpg = 1000;
  private static final int batchSizeIcomToStaging = 10000;
  private ConnectionManager connectionManager = new ConnectionManager();
  private PreparedStatement pstmt;
  private ResultSet rst;
  private Session session = null;
  private Transaction tx = null;

  @Override
  public void beginTransaction() {

    session = this.getSession();
    tx = session.beginTransaction();

  }

  @Override
  public void commitTransaction() {

    tx.commit();
    this.releaseSession(session);
    session = null;
    tx = null;

  }

  @Override
  public void rollbackTransaction() {

    tx.rollback();
    session = null;
    tx = null;

  }

  /**
   * Select list of scheudle_id from iComensation Staging database.
   */
  @Override
  public List<ScheduleBeanICOM> listScheduleICOM() throws DataAccessResourceFailureException,
      IllegalStateException, HibernateException {

    String sql = "SELECT * FROM icom.icom_schedule_id ORDER BY schedule_id DESC";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(ScheduleBeanICOM.class);
      List<ScheduleBeanICOM> listResult = query.list();
      return listResult;
    } catch (Exception exHib) {
      logger.error("An error occurred while listing schedules from iCompensation Staging DB."
          + " Stack trace is:", exHib);
      throw exHib;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception exNul) {
        // Do Nothing.
      }
    }
  }

  @Override
  public List<ScheduleBeanICOM> listScheduleICOM(String scheduleId)
      throws DataAccessResourceFailureException, IllegalStateException, HibernateException {

    logger.debug("Starts ScheduleDAOImpl listScheduleICOM");
    String sql = "SELECT * FROM ICOM.ICOM_SCHEDULE_ID WHERE SCHEDULE_ID LIKE :scheduleId";
    List<ScheduleBeanICOM> listResult = new ArrayList<ScheduleBeanICOM>();
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(ScheduleBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
      logger.debug("Ends ScheduleDAOImpl listScheduleICOM");
    } catch (Exception ex) {
      logger.error("An error occurred while listing data from ICOM_SCHEDULE_ID. Stack trace is\n",
          ex);
      throw ex;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception exGen) {
        // Do Nothing.
      }
    }
    return listResult;

  }

  /**
   * Select benefits from iCompensation Main Database.
   * 
   * @param scheduleId The schedule only of which the records will be retrieved.
   * @return List of BenefitBeanICOMDB beans.
   * @throws Exception When the method cannot query data from iCompensation Main Database.
   */
  @Override
  public List<BenefitBeanICOMDB> listBenefitICOMDB(String scheduleId)
      throws DataAccessResourceFailureException, IllegalStateException, HibernateException,
      ClassNotFoundException, SQLException {

    logger.debug("Starts ScheduleDAOImpl listBenefitICOMDB");
    List<BenefitBeanICOMDB> listResult = new ArrayList<BenefitBeanICOMDB>();
    String sql = "SELECT res.cal_schedule_id AS schedule_id, res.benefit_result_id,"
        + "     res.benefit_type_id, typ.benefit_type_code AS benefit_code, res.agent_code,"
        + "     res.benefit_amount, null AS policy_no, null AS rider_no, null AS recal_schedule_id,"
        + "     res.is_deleted, res.create_by, res.create_date, res.update_by, res.update_date"
        + " FROM icom.acs_benefit_result res"
        + "   LEFT JOIN icom.acs_benefit_type typ ON res.benefit_type_id = typ.benefit_type_id"
        + " WHERE res.cal_schedule_id LIKE ?";

    try {
      pstmt = connectionManager.getDbConnection().prepareStatement(sql);
      pstmt.setString(1, scheduleId.replace("*", "%"));
      rst = pstmt.executeQuery();
      while (rst.next()) {
        BenefitBeanICOMDB benefitBeanICOMDB = new BenefitBeanICOMDB();
        benefitBeanICOMDB.setCalScheduleId(rst.getString("SCHEDULE_ID"));
        benefitBeanICOMDB.setBenefitResultId(rst.getBigDecimal("BENEFIT_RESULT_ID"));
        benefitBeanICOMDB.setBenefitTypeId(rst.getBigDecimal("BENEFIT_TYPE_ID"));
        benefitBeanICOMDB.setBenefitCode(rst.getString("BENEFIT_CODE"));
        benefitBeanICOMDB.setAgentCode(rst.getString("AGENT_CODE"));
        benefitBeanICOMDB.setBenefitAmount(rst.getBigDecimal("BENEFIT_AMOUNT"));
        benefitBeanICOMDB.setPolicyNo(rst.getString("POLICY_NO"));
        benefitBeanICOMDB.setRiderNo(rst.getString("RIDER_NO"));
        benefitBeanICOMDB.setRecalScheduleId(rst.getString("RECAL_SCHEDULE_ID"));
        benefitBeanICOMDB.setIsDeleted(rst.getString("IS_DELETED"));
        benefitBeanICOMDB.setCreateDate(rst.getDate("CREATE_DATE"));
        benefitBeanICOMDB.setCreateBy(rst.getBigDecimal("CREATE_BY"));
        benefitBeanICOMDB.setUpdateDate(rst.getDate("UPDATE_DATE"));
        benefitBeanICOMDB.setUpdateBy(rst.getBigDecimal("UPDATE_BY"));
        listResult.add(benefitBeanICOMDB);
      }
    } catch (Exception ex) {
      logger.error("Error occurred while querying iCompensation Main Database for Benefit Results."
          + " Stack trace is:", ex);
      throw ex;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listBenefitICOMDB");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do nothing.
      }
    }
    return listResult;
  }

  @Override
  public List<PolicyClawbackBeanICOMDB> listPolicyClawbackICOMDB(String scheduleId)
      throws Exception {
    logger.debug("Starts ScheduleDAOImpl listPolicyClawbackICOMDB");
    List<PolicyClawbackBeanICOMDB> listResult = new ArrayList<PolicyClawbackBeanICOMDB>();
    try {
      String sql = "SELECT p.schedule_id, p.psn_trans_id AS clawback_id, p.agent_code, d.policy_no,"
          + "    c.rider_code AS rider_no, p.schedule_id AS recal_Schedule_id, d.prod_type_id,"
          + "    d.prod_amount, p.create_date, p.create_By, p.create_date as update_date, "
          + "    p.create_by AS update_by"
          + " FROM icom.acs_psn_trans p"
          + "   INNER JOIN icom.acs_com_trans c ON p.psn_trans_id = c.psn_trans_id"
          + "   INNER JOIN icom.acs_tsli_com_detail d ON c.com_trans_id = d.com_trans_id"
          + " WHERE p.schedule_id = :schedule_id AND d.prod_amount < 0";
      pstmt = connectionManager.getDbConnection().prepareStatement(sql);
      pstmt.setString(1, scheduleId.replace("*", "%"));
      rst = pstmt.executeQuery();
      logger.debug("Query executed. Prepare to extract data.");
      while (rst.next()) {
        PolicyClawbackBeanICOMDB policyClawbackBeanICOMDB = new PolicyClawbackBeanICOMDB();
        policyClawbackBeanICOMDB.setScheduleId(rst.getString("SCHEDULE_ID"));
        policyClawbackBeanICOMDB.setClawbackId(rst.getString("CLAWBACK_ID"));
        policyClawbackBeanICOMDB.setAgentCode(rst.getString("AGENT_CODE"));
        policyClawbackBeanICOMDB.setPolicyNo(rst.getString("POLICY_NO"));
        policyClawbackBeanICOMDB.setRiderNo(rst.getString("RIDER_NO"));
        policyClawbackBeanICOMDB.setRecalScheduleId(rst.getString("RECAL_SCHEDULE_ID"));
        policyClawbackBeanICOMDB.setProdTypeId(rst.getBigDecimal("PROD_TYPE_ID"));
        policyClawbackBeanICOMDB.setProdAmount(rst.getBigDecimal("PROD_AMOUNT"));
        policyClawbackBeanICOMDB.setCreateDate(rst.getDate("CREATE_DATE"));
        policyClawbackBeanICOMDB.setCreateBy(rst.getBigDecimal("CREATE_BY"));
        policyClawbackBeanICOMDB.setUpdateDate(rst.getDate("UPDATE_DATE"));
        policyClawbackBeanICOMDB.setUpdateBy(rst.getBigDecimal("UPDATE_BY"));
        listResult.add(policyClawbackBeanICOMDB);
      }
    } catch (Exception ex) {
      logger.error("An error occurred while retrieving clawback data from iCompensation Main DB."
          + " Stack trace is\n", ex);
      throw ex;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listPolicyClawbackICOMDB");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do nothing.
      }
    }
    return listResult;
  }

  /**
   * List all active agents from ACS_PSN_INF in iCompensation main database to IPG_PSN_INF in
   * iPlayground. Please check be sure that the missing (inactive) agents will not cause Exception
   * later in the workflow.
   * 
   * @param scheduleId Schedule to be retrieved. For the iCompensation version that doesn't have
   *        agent structure varies by schedule, This method will use provided parameter as return
   *        value.
   * @return List of active agents in specified scheduleId. If agent table does not have scheduleId,
   *         it returns all active agents.
   * @exception Exception When an error occurred during query.
   */
  @Override
  public List<AgentsBeanICOMDB> listAgentsICOMDB(String scheduleId) throws Exception {

    logger.debug("Starts ScheduleDAOImpl listAgentsICOMDB");
    List<AgentsBeanICOMDB> listResult = new ArrayList<AgentsBeanICOMDB>();
    String sql = "SELECT ? AS schedule_id, agn.agent_code, agn.national_id, agn.agent_title_tha,"
        + "     agn.agent_firstname_tha, agn.agent_lastname_tha, agn.agent_title_eng,"
        + "     agn.agent_firstname_eng, agn.agent_lastname_eng, agn.dob,"
        + "    CASE WHEN agn.gender IS NOT NULL THEN agn.gender"
        + "      WHEN agn.agent_title_tha = 'นาง' THEN 'F'"
        + "      WHEN agn.agent_title_tha = 'นางสาว' THEN 'F'"
        + "      WHEN agn.agent_title_tha = 'นาย' THEN 'M' ELSE 'N' END as gender, "
        + "     agn.agent_status, agn.appointment_date, agn.contract_date, agn.addr_1,"
        + "     agn.addr_2, agn.addr_3, agn.addr_4, agn.zip_code, agn.home_phone,"
        + "     agn.office_phone, agn.mobile_phone, agn.email, agn.effective_date,"
        + "     agn.license_code, agn.start_date, agn.expire_date, agn.first_issued_date,"
        + "     agn.create_by, agn.create_date, agn.update_by, agn.update_date, agn.agent_pos_id,"
        + "     agn.terminate_date, agn.terminate_reason, agn.is_individual, "
        + "     agn.upline_psn_inf_id AS upline_code, agn.recruiter_psn_inf_id AS recruiter_code,"
        + "     agn.work_month, agn.sale_channel_code, pos.agent_pos_desc AS position"
        + " FROM icom.acs_psn_inf agn "
        + "   LEFT JOIn icom.acs_agent_position pos ON agn.agent_pos_id = pos.agent_pos_id";

    try {
      pstmt = connectionManager.getDbConnection().prepareStatement(sql);
      pstmt.setString(1, scheduleId.replace("*", "%"));
      rst = pstmt.executeQuery();
      while (rst.next()) {
        AgentsBeanICOMDB agentsBeanICOMDB = new AgentsBeanICOMDB();
        agentsBeanICOMDB.setScheduleId(rst.getString("SCHEDULE_ID"));
        agentsBeanICOMDB.setAgentCode(rst.getString("AGENT_CODE"));
        agentsBeanICOMDB.setNationalId(rst.getString("NATIONAL_ID"));
        agentsBeanICOMDB.setAgentTitleTha(rst.getString("AGENT_TITLE_THA"));
        agentsBeanICOMDB.setAgentFirstnameTha(rst.getString("AGENT_FIRSTNAME_THA"));
        agentsBeanICOMDB.setAgentLastnameTha(rst.getString("AGENT_LASTNAME_THA"));
        agentsBeanICOMDB.setAgentTitleEng(rst.getString("AGENT_TITLE_ENG"));
        agentsBeanICOMDB.setAgentFirstnameEng(rst.getString("AGENT_FIRSTNAME_ENG"));
        agentsBeanICOMDB.setAgentLastnameEng(rst.getString("AGENT_LASTNAME_ENG"));
        agentsBeanICOMDB.setDob(rst.getDate("DOB"));
        agentsBeanICOMDB.setGender(rst.getString("GENDER"));
        agentsBeanICOMDB.setAgentStatus(rst.getString("AGENT_STATUS"));
        agentsBeanICOMDB.setAppointmentDate(rst.getDate("APPOINTMENT_DATE"));
        agentsBeanICOMDB.setContractDate(rst.getDate("CONTRACT_DATE"));
        agentsBeanICOMDB.setAddr1(rst.getString("ADDR_1"));
        agentsBeanICOMDB.setAddr2(rst.getString("ADDR_2"));
        agentsBeanICOMDB.setAddr3(rst.getString("ADDR_3"));
        agentsBeanICOMDB.setAddr4(rst.getString("ADDR_4"));
        agentsBeanICOMDB.setZipCode(rst.getString("ZIP_CODE"));
        agentsBeanICOMDB.setHomePhone(rst.getString("HOME_PHONE"));
        agentsBeanICOMDB.setOfficePhone(rst.getString("OFFICE_PHONE"));
        agentsBeanICOMDB.setMobilePhone(rst.getString("MOBILE_PHONE"));
        agentsBeanICOMDB.setEmail(rst.getString("EMAIL"));
        agentsBeanICOMDB.setEffectiveDate(rst.getDate("EFFECTIVE_DATE"));
        agentsBeanICOMDB.setLicenseCode(rst.getString("LICENSE_CODE"));
        agentsBeanICOMDB.setStartDate(rst.getDate("START_DATE"));
        agentsBeanICOMDB.setExpireDate(rst.getDate("EXPIRE_DATE"));
        agentsBeanICOMDB.setFirstIssuedDate(rst.getDate("FIRST_ISSUED_DATE"));
        agentsBeanICOMDB.setCreateBy(rst.getBigDecimal("CREATE_BY"));
        agentsBeanICOMDB.setCreateDate(rst.getDate("CREATE_DATE"));
        agentsBeanICOMDB.setUpdateBy(rst.getBigDecimal("UPDATE_BY"));
        agentsBeanICOMDB.setUpdateDate(rst.getDate("UPDATE_DATE"));
        agentsBeanICOMDB.setAgentPosId(rst.getBigDecimal("AGENT_POS_ID"));
        agentsBeanICOMDB.setTerminateDate(rst.getDate("TERMINATE_DATE"));
        agentsBeanICOMDB.setTerminateReason(rst.getString("TERMINATE_REASON"));
        agentsBeanICOMDB.setIsIndividual(rst.getString("IS_INDIVIDUAL"));
        agentsBeanICOMDB.setUplineCode(rst.getString("UPLINE_CODE"));
        agentsBeanICOMDB.setRecruiterCode(rst.getString("RECRUITER_CODE"));
        agentsBeanICOMDB.setWorkMonth(rst.getBigDecimal("WORK_MONTH"));
        agentsBeanICOMDB.setSaleChannelCode(rst.getString("SALE_CHANNEL_CODE"));
        // agentsBeanICOMDB.setPosition(rst.getString("POSITION"));
        listResult.add(agentsBeanICOMDB);
      }
    } catch (Exception ex) {
      logger.error("An error occurred while retrieving data from ACS_PSN_INF and ACS_AGENT_POS in"
          + " iCompenstion Main DB. Stack trace is:", ex);
      throw ex;
    } finally {
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do nothing.
      }
    }
    return listResult;
  }

  /**
   * Select data from ACS_PSN_TRANS table in iCompensation database. This data will be inserted into
   * IPG_PSN_PROD in iCompensation staging DB later.
   * 
   * @param scheduleId Schedule whose data to be selected from main database.
   * @return List of data records from iCompenstation Db.
   */
  @Override
  public List<PerformanceBeanICOMDB> listPerformanceICOMDB(String scheduleId) {

    List<PerformanceBeanICOMDB> listResult = new ArrayList<PerformanceBeanICOMDB>();
    String sql = "SELECT d.policy_no, p.agent_code, p.agent_pos_id, d.prod_type_id,"
        + "    d.policy_type_id, d.prod_amount, 'I' input_type, p.schedule_id, c.policy_year,"
        + "    c.rider_code AS rider_no, c.ack_date, c.performance_month, c.performance_year"
        + " FROM icom.acs_psn_trans p INNER JOIN icom.acs_com_trans c ON p.psn_trans_id = "
        + "   c.psn_trans_id"
        + "   INNER JOIN icom.acs_tsli_com_detail d ON c.com_trans_id = d.com_trans_id"
        + " WHERE p.schedule_id = :schedule_id";

    try {
      pstmt = connectionManager.getDbConnection().prepareStatement(sql);
      pstmt.setString(1, scheduleId.replace("*", "%"));
      logger.debug("Query prepared. About to execute.");
      rst = pstmt.executeQuery();
      // logger.debug("Query executed.");
      long intHeartbeat = 0;
      while (rst.next()) {
        if (++intHeartbeat % 10000 == 0) {
          logger.debug("Finished reading " + intHeartbeat + " rows.");
        }
        PerformanceBeanICOMDB performanceBeanICOMDB = new PerformanceBeanICOMDB();
        performanceBeanICOMDB.setPolicyNo(rst.getString("POLICY_NO"));
        performanceBeanICOMDB.setAgentCode(rst.getString("AGENT_CODE"));
        performanceBeanICOMDB.setAgentPosId(rst.getBigDecimal("AGENT_POS_ID"));
        performanceBeanICOMDB.setProdTypeId(rst.getBigDecimal("PROD_TYPE_ID"));
        performanceBeanICOMDB.setPolicyTypeId(rst.getBigDecimal("POLICY_TYPE_ID"));
        performanceBeanICOMDB.setProdAmount(rst.getBigDecimal("PROD_AMOUNT"));
        performanceBeanICOMDB.setInputType(rst.getString("INPUT_TYPE"));
        performanceBeanICOMDB.setScheduleId(rst.getString("SCHEDULE_ID"));
        performanceBeanICOMDB.setPolicyYear(rst.getInt("POLICY_YEAR"));
        performanceBeanICOMDB.setRiderCode(rst.getString("RIDER_NO"));
        performanceBeanICOMDB.setAckDate(rst.getDate("ACK_DATE"));
        performanceBeanICOMDB.setPerformanceMonth(rst.getString("PERFORMANCE_MONTH"));
        performanceBeanICOMDB.setPerformanceYear(rst.getString("PERFORMANCE_YEAR"));
        listResult.add(performanceBeanICOMDB);
      }
    } catch (Exception ex) {
      logger.error("An error occurred while retrieving data from ACS_PSN_TRANS, ACS_COM_TRANS, or "
          + "ACS_TSLI_COM_DETAIL. Stack trace is:", ex);
    } finally {
      logger.debug("Ends ScheduleDAOImpl listPerformanceICOMDB");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        logger.info("Prepared statement and/or resultset are not properly initiated.");
      }
    }
    return listResult;
  }

  /**
   * List agent code and agent's upline code to build heirarchical structure.
   * 
   * @param scheduleId Schedule of which the data is to be based on.
   * @return List of Agent code along with their ID.
   */
  @Override
  public List<TierBeanICOMDB> listTierICOMDB(String scheduleId)
      throws SQLException, ClassNotFoundException, NumberFormatException, Exception {

    List<TierBeanICOMDB> listResult = new ArrayList<TierBeanICOMDB>();
    final String sqlSchedule = "select period_year, period_month from acs_schedule"
        + " WHERE schedule_id = ?";
    final String sqlTier = "SELECT " + scheduleId + " as schedule_id, downlineid as agent_code,"
        + "    agentid as upline_l1_code"
        + " FROM table(GET_ALL_RELATION_PERFOR(?,?, ?, ?, '', '', ?))  WHERE performanceym = ?";

    try {
      pstmt = connectionManager.getDbConnection().prepareStatement(sqlSchedule);
      pstmt.setLong(1, Integer.parseInt(scheduleId.replaceAll("[^0-9]", "")));
      rst = pstmt.executeQuery();
      if (rst.next()) {
        final String perfYear = rst.getString("period_year");
        final String perfMonth = rst.getString("period_month");

        try (PreparedStatement pstmtInsert =
            connectionManager.getDbConnection().prepareStatement(sqlTier)) {
          // TODO: find a way to remove this extremely ugly hard-coding
          pstmtInsert.setString(1, "221102010");
          pstmtInsert.setString(2, "GM");
          pstmtInsert.setString(3, perfYear);
          pstmtInsert.setString(4, perfMonth);
          pstmtInsert.setString(5, perfYear + perfMonth);
          pstmtInsert.setString(6, perfYear + perfMonth);
          rst = pstmtInsert.executeQuery();

          while (rst.next()) {
            TierBeanICOMDB tierBeanIcomDb = new TierBeanICOMDB();
            // tierBeanICOMDB.setScheduleId(rst.getString("SCHEDULE_ID"));
            tierBeanIcomDb.setScheduleId(scheduleId);
            tierBeanIcomDb.setAgentCode(rst.getString("AGENT_CODE"));
            tierBeanIcomDb.setUplineL1Code(rst.getString("UPLINE_L1_CODE"));
            if (!tierBeanIcomDb.getAgentCode().equals(tierBeanIcomDb.getUplineL1Code())) {
              listResult.add(tierBeanIcomDb);
            }
          }
          // Add root so that Agent Structure page can display this code.
          TierBeanICOMDB tierBeanIcomDb = new TierBeanICOMDB();
          tierBeanIcomDb.setScheduleId(scheduleId);
          tierBeanIcomDb.setAgentCode("221102010");
          listResult.add(tierBeanIcomDb);
        } catch (Exception exGen) {

        }
      } else {
        // The schedule does not exist in TSLI's whatever table GET_RELATION_PERFOR is looking.
        // return blank list.
        return listResult;
      }

    } catch (ClassNotFoundException exCnf) {
      logger.error("An error occurred while trying to load class definistion. Stack trace is\n",
          exCnf);
      throw exCnf;
    } catch (SQLException exSql) {
      logger.error("An error occurred while running sql statement. Stack trace is\n", exSql);
      throw exSql;
    } catch (NumberFormatException exNum) {
      logger.error("The scheudleId '" + scheduleId + "' is not a valid schedule format.");
      throw exNum;
    } catch (Exception ex) {
      // e.printStackTrace();
      logger.error("An error occurred while retrieving data from tier data from iCompensation"
          + " Database. Stack trace is\n", ex);
      throw ex;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listTierICOMDB");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }

    return listResult;
  }

  /**
   * List all schedule starting with provided Id. This function should retrieve all schedules with
   * id started by provided numbers.
   * 
   * @param scheduleId search criteria as a wild card (id*)
   * @return List of schedule whose Id started with search criteria.
   * @throws Exception When the query cannot be executed or database cannot be connected.
   */
  @Override
  public List<ScheduleBeanICOMDB> listScheduleICOMDB(String scheduleId) throws Exception {
    logger.debug("Starts ScheduleDAOImpl listScheduleICOMDB");
    List<ScheduleBeanICOMDB> listResult = new ArrayList<ScheduleBeanICOMDB>();
    try {
      String sql = "SELECT s.schedule_id, s.period_from, s.period_to, s.payment_date, s.cycle_name,"
          + "    s.create_by, s.create_date, s.update_by, s.update_date, s.schedule_desc,"
          + "    CASE s.is_paid WHEN 'T' THEN 'Y' WHEN 'F' THEN 'N' ELSE s.is_paid END AS is_paid,"
          + "    CASE s.is_deleted WHEN 'T' THEN 'Y' WHEN 'F' THEN 'N' ELSE s.is_deleted END"
          + "    AS is_deleted, s.period_year, s.period_month"
          + " FROM icom.acs_schedule s WHERE s.schedule_id LIKE :scheduleId";
      // "ORDER BY s.schedule_id DESC";

      pstmt = connectionManager.getDbConnection().prepareStatement(sql);
      pstmt.setString(1, scheduleId.replace("*", "%"));
      rst = pstmt.executeQuery();
      logger.debug("Query execution completed.");
      int rowCount = 0;
      while (rst.next()) {
        rowCount++;
        ScheduleBeanICOMDB scheduleBeanIcomDb = new ScheduleBeanICOMDB();
        scheduleBeanIcomDb.setScheduleId(rst.getString("SCHEDULE_ID"));
        scheduleBeanIcomDb.setPeriodFrom(rst.getDate("PERIOD_FROM"));
        scheduleBeanIcomDb.setPeriodTo(rst.getDate("PERIOD_TO"));
        scheduleBeanIcomDb.setPaymentDate(rst.getDate("PAYMENT_DATE"));
        scheduleBeanIcomDb.setCycleName(rst.getString("CYCLE_NAME"));
        scheduleBeanIcomDb.setCreateBy(rst.getBigDecimal("CREATE_BY"));
        scheduleBeanIcomDb.setCreateDate(rst.getDate("CREATE_DATE"));
        scheduleBeanIcomDb.setUpdateBy(rst.getBigDecimal("UPDATE_BY"));
        scheduleBeanIcomDb.setUpdateDate(rst.getDate("UPDATE_DATE"));
        scheduleBeanIcomDb.setScheduleDesc(rst.getString("SCHEDULE_DESC"));
        scheduleBeanIcomDb.setIsPaid(rst.getString("IS_PAID"));
        scheduleBeanIcomDb.setIsDeleted(rst.getString("IS_DELETED"));
        scheduleBeanIcomDb.setPeriodYear(rst.getString("PERIOD_YEAR"));
        scheduleBeanIcomDb.setPeriodMonth(rst.getString("PERIOD_MONTH"));
        listResult.add(scheduleBeanIcomDb);
      }
      logger.info("Schedules listed. " + rowCount + " rows were retrieved.");
    } catch (Exception ex) {
      logger.error("An error occurred while retrieving data from ACS_SCHEDULE in iCompensation "
          + "Database. Stack trace is:\n", ex);
      throw ex;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listScheduleICOMDB");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing.
      }
    }
    return listResult;
  }

  @Override
  public List<BenefitBeanICOM> listBenefitICOM(String scheduleId) {
    logger.debug("Starts ScheduleDAOImpl listBenefitICOM");
    String sql = "SELECT * FROM ICOM.ICOM_BENEFIT_CAL WHERE SCHEDULE_ID LIKE :scheduleId";
    List<BenefitBeanICOM> listResult = null;
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(BenefitBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
      logger.debug("ends ScheduleDAOImpl listBenefitICOM");
    } catch (Exception ex) {
      logger.error("An error occurred while listing ICOM_BENEFIT_CAL. Stack trace is\n", ex);
      throw ex;
    } finally {
      this.releaseSession(session);
    }
    return listResult;
  }

  @Override
  public List<PolicyClawbackBeanICOM> listPolicyClawbackICOM(String scheduleId) {
    logger.debug("Starts ScheduleDAOImpl listPolicyClawbackICOM");
    String sql = "SELECT * FROM ICOM.ICOM_CLAWBACK_AGENT WHERE SCHEDULE_ID LIKE :scheduleId";
    List<PolicyClawbackBeanICOM> listResult = new ArrayList<PolicyClawbackBeanICOM>();
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(PolicyClawbackBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
      logger.debug("Ends ScheduleDAOImpl listPolicyClawbackICOM");
    } catch (Exception ex) {
      logger.error("An error occurred while reading ICOM_CLAWBACK_AGENT. Stack trace is\n", ex);
      throw ex;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    return listResult;
  }

  @Override
  public List<AgentsBeanICOM> listAgentsICOM(String scheduleId) {
    logger.debug("Starts ScheduleDAOImpl listAgentsICOM");
    String sql = "SELECT * FROM ICOM.ICOM_PSN_INF WHERE SCHEDULE_ID LIKE :scheduleId";
    Session session = null;
    List<AgentsBeanICOM> listResult = new ArrayList<AgentsBeanICOM>();
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(AgentsBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred while  listing data from ICOM Staging Database."
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing
      }
    }
    System.out.println("Ends ScheduleDAOImpl listAgentsICOM");
    return listResult;
  }

  @Override
  public List<PerformanceBeanICOM> listPerformanceICOM(String scheduleId) {

    logger.debug("Starts ScheduleDAOImpl listPerformanceICOM");
    String sql = "SELECT * FROM ICOM.ICOM_PSN_PROD WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;
    Query query;
    List<PerformanceBeanICOM> listResult = null;
    try {
      logger.debug("Ends ScheduleDAOImpl listPerformanceICOM 1");
      session = this.getSession();
      query = session.createSQLQuery(sql).addEntity(PerformanceBeanICOM.class);
      logger.debug("Ends ScheduleDAOImpl listPerformanceICOM 2");
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      logger.debug("Ends ScheduleDAOImpl listPerformanceICOM 3");
      listResult = query.list();
    } catch (HibernateException exHib) {
      logger.error("Cannot query or set query parameter. Stack trace is\n", exHib);
      throw exHib;
    } catch (IllegalStateException exIl) {
      logger.error("Cannot get session to perform query. Stack trace is\n", exIl);
      throw exIl;
    } catch (DataAccessResourceFailureException exDaf) {
      logger.error("Cannot get session to perform query. Stack trace is\n", exDaf);
      throw exDaf;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing
      }
    }
    return listResult;
  }

  @Override
  public List<TierBeanICOM> listTierICOM(String scheduleId) {
    logger.debug("Starts ScheduleDAOImpl listTierICOM");
    String sql = "SELECT * FROM ICOM.ICOM_PSN_TIER WHERE SCHEDULE_ID LIKE :scheduleId";
    Session session = null;
    List<TierBeanICOM> listResult = new ArrayList<TierBeanICOM>();
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(TierBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred while reading ICOM_PSN_TIER. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    System.out.println("Ends ScheduleDAOImpl listTierICOM");
    logger.debug("Ends ScheduleDAOImpl listTierICOM");
    return listResult;
  }

  @Override
  public List<VersionBeanICOM> listVersionICOM(String scheduleId) {
    logger.debug("Starts ScheduleDAOImpl listVersionICOM");
    Session session = null;
    String sql = "SELECT * FROM ICOM.ICOM_SCHEDULE_VER WHERE SCHEDULE_ID LIKE :scheduleId";
    List<VersionBeanICOM> listResult = new ArrayList<VersionBeanICOM>();
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(VersionBeanICOM.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
    } catch (Exception exGen) {
      logger.error("An error occured while listing data from ICOM_SCHEDULE_VER. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    logger.debug("Ends ScheduleDAOImpl listVersionICOM");
    return listResult;

  }

  @Override
  public List<TierBean> listTier(String scheduleId) {

    logger.debug("Starts listTier");
    String sql = "SELECT T.*, CONCAT(A.AGENT_TITLE_THA ,A.AGENT_FIRSTNAME_THA ,' ',"
        + "     A.AGENT_LASTNAME_THA ,' ','(',M.CODE ,')') AS DISPLAY "
        + " FROM IPG.IPG_PSN_TIER T INNER JOIN IPG.IPG_PSN_INF A ON T.AGENT_CODE = A.AGENT_CODE"
        + "   INNER JOIN IPG.IPG_MASTER_CODE M ON M.TYPE_ID= A.AGENT_POS_ID AND "
        + "     M.CODE_GROUP = 'POSITION TYPE' WHERE T.SCHEDULE_ID LIKE :scheduleId";
    Session session = null;
    List<TierBean> listResult = new ArrayList<TierBean>();

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(TierBean.class);
      query.setParameter("scheduleId", scheduleId.replace("*", "%"));
      listResult = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred while selecting data from IPG_PSN_TIER. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    logger.debug("Ends listTier");
    return listResult;
  }

  @Override
  public ScheduleBeanICOM findScheduleICOM(String scheduleId) {

    logger.debug("Starts findScheduleICOM");
    String sql = "SELECT * FROM ICOM.ICOM_SCHEDULE_ID WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;
    List<ScheduleBeanICOM> listResult = new ArrayList<ScheduleBeanICOM>();

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(ScheduleBeanICOM.class);
      query.setParameter("scheduleId", scheduleId);
      listResult = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred while selecting data from ICMO_SCHEDULE_ID. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    ScheduleBeanICOM scheduleBean = new ScheduleBeanICOM();
    if (listResult.size() > 0) {
      scheduleBean = listResult.get(0);
    }
    logger.debug("Ends findScheduleICOM");
    return scheduleBean;
  }

  @Override
  public int checkSetSchedule() {

    String sql = "SELECT COUNT(*) FROM IPG.IPG_SET_SCH_VER";
    Session session = null;
    int cnt = 0;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      cnt = Integer.parseInt(query.list().get(0).toString());
    } catch (Exception exGen) {
      logger.error("An error occurred while reading IPG_SET_SCH_VER. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return cnt;
  }

  /**
   * Select current active schedule and version from IPG_SET_SCH_VER and IPG_SCHEDULE.
   */
  @Override
  public CurrentScheduleBean getCurrenSchedule() {

    String sql = "SELECT T1.SCHEDULE_ID,T1.MAIN_VER,T2.PERIOD_FROM,T2.PERIOD_TO,T2.SCHEDULE_DESC"
        + " FROM IPG.IPG_SET_SCH_VER T1"
        + "   INNER JOIN IPG.IPG_SCHEDULE_ID T2 ON T1.SCHEDULE_ID = T2.SCHEDULE_ID";
    CurrentScheduleBean currentSche = new CurrentScheduleBean();
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(CurrentScheduleBean.class);
      currentSche = (CurrentScheduleBean) query.list().get(0);
    } catch (HibernateException exHib) {
      logger.error("An error occurred while reading data from IPT_SET_SCH_VER, IPT_SCHEDULE_ID. "
          + "Stack trace is\n", exHib);
      throw exHib;
    } catch (IndexOutOfBoundsException exIob) {
      logger.error("There is no related data in IPT_SET_SCH_VER and IPT_SCHEDULE_ID. "
          + "The table data might be tampered.");
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return currentSche;
  }

  @Override
  public boolean deleteBenefit() throws Exception {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_BENEFIT_CAL";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occured while deleting benefit. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }

    return result;
  }

  @Override
  public boolean deletePolicyClawback() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_CLAWBACK_AGENT";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting from ipg_clawback_agent. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }

    return result;
  }

  @Override
  public boolean deleteAgents() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_PSN_INF";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting from IPG_PSN_INF. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deletePerformance() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_PSN_PROD";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting agent performance. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deleteTier() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_PSN_TIER";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting data from IPG_PSN_TIER. Stack trace is\n",
          exGen);
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deleteSchedule() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_SCHEDULE_ID";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred wile deleting from IPG_SCHEDULE_ID. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deleteVersion() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_SCHEDULE_VER";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurre while deleting from IPG_SCHEDULE_VER. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deleteSetSchedule() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_SET_SCH_VER";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting from IPG_SET_SCH_VER. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    logger.debug("Method ends.");
    return result;
  }

  @Override
  public boolean insertBenefit(BenefitBean benefitBean) {
    logger.debug("Method starts.");

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_BENEFIT_CAL (SCHEDULE_ID, VER, BENEFIT_RESULT_ID,"
        + "     BENEFIT_CODE, AGENT_CODE, BENEFIT_AMOUNT, POLICY_NO, RIDER_NO,"
        + "     RECAL_SCHEDULE_ID, IS_DELETED, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY)"
        + " VALUES (:calScheduleId, :ver, :benefitResultId, :benefitCode,"
        + "     :agentCode, :benefitAmount, :policyNo, :riderNo, :recalScheduleId, :isDeleted,"
        + "     :createDate, :createBy, :updateDate, :updateBy)";
    int rowUpdate = 0;
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("calScheduleId", benefitBean.getCalScheduleId());
      query.setParameter("ver", benefitBean.getVer());
      query.setParameter("benefitResultId", benefitBean.getBenefitResultId());
      // query.setParameter("benefitTypeId", benefitBean.getBenefitTypeId());
      query.setParameter("benefitCode", benefitBean.getBenefitCode());
      query.setParameter("agentCode", benefitBean.getAgentCode());
      query.setParameter("benefitAmount", benefitBean.getBenefitAmount());
      query.setParameter("policyNo", benefitBean.getPolicyNo());
      query.setParameter("riderNo", benefitBean.getRiderNo());
      query.setParameter("recalScheduleId", benefitBean.getRecalScheduleId());
      query.setParameter("isDeleted", benefitBean.getIsDeleted());
      query.setParameter("createDate", benefitBean.getCreateDate());
      query.setParameter("createBy", benefitBean.getCreateBy());
      query.setParameter("updateDate", benefitBean.getUpdateDate());
      query.setParameter("updateBy", benefitBean.getUpdateBy());
      rowUpdate = query.executeUpdate();
      result = true;
    } catch (HibernateException exHib) {
      logger.error("An error occured while inserting data into IPG_BENEFIT_CAL table. for benefit "
          + "id " + benefitBean.getBenefitResultId() + ". Stack trace is\n", exHib);
      throw exHib;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    logger.debug("Method Ends. " + result + " row updated.");
    return result;
  }

  @Override
  public boolean insertListBenefit(final List<BenefitBeanICOM> benefitBeanICOMs) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    try {
      Session session = null;
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {
        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO IPG.IPG_BENEFIT_CAL (SCHEDULE_ID, VER, BENEFIT_RESULT_ID,"
                + "     BENEFIT_CODE, AGENT_CODE, BENEFIT_AMOUNT, POLICY_NO, RIDER_NO,"
                + "     RECAL_SCHEDULE_ID, IS_DELETED, CREATE_DATE, CREATE_BY, UPDATE_DATE, "
                + "     UPDATE_BY)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (BenefitBeanICOM benefitBeanIcom : benefitBeanICOMs) {

              pstmt.setString(1, benefitBeanIcom.getCalScheduleId());
              pstmt.setBigDecimal(2, benefitBeanIcom.getVer());
              pstmt.setBigDecimal(3, benefitBeanIcom.getBenefitResultId());
              pstmt.setString(4, benefitBeanIcom.getBenefitCode());
              pstmt.setString(5, benefitBeanIcom.getAgentCode());
              pstmt.setBigDecimal(6, benefitBeanIcom.getBenefitAmount());
              pstmt.setString(7, benefitBeanIcom.getPolicyNo());
              pstmt.setString(8, benefitBeanIcom.getRiderNo());
              pstmt.setString(9, benefitBeanIcom.getRecalScheduleId());
              pstmt.setString(10, benefitBeanIcom.getIsDeleted());
              pstmt.setDate(11, benefitBeanIcom.getCreateDate() == null ? null
                  : new java.sql.Date(benefitBeanIcom.getCreateDate().getTime()));
              pstmt.setBigDecimal(12, benefitBeanIcom.getCreateBy());
              pstmt.setDate(13, benefitBeanIcom.getUpdateDate() == null ? null
                  : new java.sql.Date(benefitBeanIcom.getUpdateDate().getTime()));
              pstmt.setBigDecimal(14, benefitBeanIcom.getUpdateBy());
              pstmt.addBatch();
              // 10000 : JDBC batch size
              if (i % batchSizeStagingToIpg == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      rowUpdate = benefitBeanICOMs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_BENEFIT_CAL. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  @Override
  public boolean insertPolicyClawback(PolicyClawbackBean policyClawbackBean) {
    logger.debug("Method starts.");

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_CLAWBACK_AGENT (SCHEDULE_ID, VER, CLAWBACK_ID, AGENT_CODE,"
        + "     POLICY_NO, RIDER_NO, RECAL_SCHEDULE_ID, PROD_TYPE_ID, PROD_AMOUNT, CREATE_DATE,"
        + "     CREATE_BY, UPDATE_DATE, UPDATE_BY)"
        + " VALUES (:scheduleId, :ver, :clawbackId, :agentCode, :policyNo, :riderNo,"
        + "     :recalScheduleId, :prodTypeId, :prodAmount, :createDate, :createBy, :updateDate,"
        + "     :updateBy)";
    int rowUpdate = 0;
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", policyClawbackBean.getScheduleId());
      query.setParameter("ver", policyClawbackBean.getVer());
      query.setParameter("clawbackId", policyClawbackBean.getClawbackId());
      query.setParameter("agentCode", policyClawbackBean.getAgentCode());
      query.setParameter("policyNo", policyClawbackBean.getPolicyNo());
      query.setParameter("riderNo", policyClawbackBean.getRiderNo());
      query.setParameter("recalScheduleId", policyClawbackBean.getRecalScheduleId());
      query.setParameter("prodTypeId", policyClawbackBean.getProdTypeId());
      query.setParameter("prodAmount", policyClawbackBean.getProdAmount());
      query.setParameter("createDate", policyClawbackBean.getCreateDate());
      query.setParameter("createBy", policyClawbackBean.getCreateBy());
      query.setParameter("updateDate", policyClawbackBean.getUpdateDate());
      query.setParameter("updateBy", policyClawbackBean.getUpdateBy());
      rowUpdate = query.executeUpdate();
    } catch (HibernateException exHib) {
      logger.error("An error occured while inserting data into IPG_CLAWBACK_AGENT table. "
          + "for benefit id " + policyClawbackBean.getClawbackId() + ". Stack trace is\n", exHib);
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing
      }
    }
    if (rowUpdate > 0) {
      result = true;
    }
    logger.debug("Method ends.");

    return result;

  }

  /**
   * This is the batched version of insertPolicyClawback() method. it will receive the list of
   * PolicyClawbackBeanICOM beans and insert them into IPG_CLAWBACK_AGENT.
   * 
   * @param policyClawbackBeans The list of clawback.
   * @return An integer representing number of rows inserted into IPG_CLAWBACK_AGENT. The negative
   *         numbers represent errors.
   */
  @Override
  public int insertListPolicyClawback(final List<PolicyClawbackBeanICOM> policyClawbackBeans) {

    int rowsInserted = 0;
    Session session = null;

    try {
      session = this.getSession();

      session.doWork(new Work() {
        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO IPG.IPG_CLAWBACK_AGENT (SCHEDULE_ID, VER, CLAWBACK_ID,"
                + "    AGENT_CODE, POLICY_NO, RIDER_NO, RECAL_SCHEDULE_ID, PROD_TYPE_ID, "
                + "    PROD_AMOUNT, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (PolicyClawbackBeanICOM clawbackBean : policyClawbackBeans) {
              pstmt.setString(1, clawbackBean.getScheduleId());
              pstmt.setBigDecimal(2, clawbackBean.getVer());
              pstmt.setString(3, clawbackBean.getClawbackId());
              pstmt.setString(4, clawbackBean.getAgentCode());
              pstmt.setString(5, clawbackBean.getPolicyNo());
              pstmt.setString(6, clawbackBean.getRiderNo());
              pstmt.setString(7, clawbackBean.getRecalScheduleId());
              pstmt.setBigDecimal(8, clawbackBean.getProdTypeId());
              pstmt.setBigDecimal(9, clawbackBean.getProdAmount());
              pstmt.setDate(10, clawbackBean.getCreateDate() == null ? null
                  : new java.sql.Date(clawbackBean.getCreateDate().getTime()));
              pstmt.setBigDecimal(11, clawbackBean.getCreateBy());
              pstmt.setDate(12, clawbackBean.getUpdateDate() == null ? null
                  : new java.sql.Date(clawbackBean.getUpdateDate().getTime()));
              pstmt.setBigDecimal(13, clawbackBean.getUpdateBy());
              pstmt.addBatch();
              if (i % batchSizeStagingToIpg == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      rowsInserted = policyClawbackBeans.size();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_CLAWBACK_AGENT. "
          + "Stack trace is", exGen);
      rowsInserted = -1;
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return rowsInserted;
  }

  @Override
  public boolean insertAgents(AgentsBean agentsBean) {

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_PSN_INF (SCHEDULE_ID, AGENT_CODE, NATIONAL_ID,"
        + "    AGENT_TITLE_THA, AGENT_FIRSTNAME_THA, AGENT_LASTNAME_THA, AGENT_TITLE_ENG,"
        + "    AGENT_FIRSTNAME_ENG, AGENT_LASTNAME_ENG, DOB, GENDER, AGENT_STATUS,"
        + "    APPOINTMENT_DATE, CONTRACT_DATE, ADDR_1, ADDR_2, ADDR_3, ADDR_4, ZIP_CODE,"
        + "    HOME_PHONE, OFFICE_PHONE, MOBILE_PHONE, EMAIL, EFFECTIVE_DATE, LICENSE_CODE,"
        + "    START_DATE, EXPIRE_DATE, FIRST_ISSUED_DATE, CREATE_BY, CREATE_DATE, UPDATE_BY,"
        + "    UPDATE_DATE, AGENT_POS_ID, TERMINATE_DATE, TERMINATE_REASON, IS_INDIVIDUAL,"
        // + " UPLINE_CODE, RECRUITER_CODE, WORK_MONTH, SALE_CHANNEL_CODE, POSITION)"
        + "    UPLINE_CODE, RECRUITER_CODE, WORK_MONTH, SALE_CHANNEL_CODE)"
        + " VALUES (:scheduleId, :agentCode, :nationalId, :agentTitleTha, :agentFirstnameTha,"
        + "    :agentLastnameTha, :agentTitleEng, :agentFirstnameEng, :agentLastnameEng, :dob,"
        + "    :gender, :agentStatus, :appointmentDate, :contractDate, :addr1, :addr2, :addr3,"
        + "    :addr4, :zipCode, :homePhone, :officePhone, :mobilePhone, :email, :effectiveDate,"
        + "    :licenseCode, :startDate, :expireDate, :firstIssuedDate, :createBy, :createDate,"
        + "    :updateBy, :updateDate, :agentPosId, :terminateDate, :terminateReason,"
        // + " :isIndividual, :uplineCode, :recruiterCode, :workMonth, :saleChannelCode, "
        // + " :position)";
        + "    :isIndividual, :uplineCode, :recruiterCode, :workMonth, :saleChannelCode)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", agentsBean.getScheduleId());
      query.setParameter("agentCode", agentsBean.getAgentCode());
      query.setParameter("nationalId", agentsBean.getNationalId());
      query.setParameter("agentTitleTha", agentsBean.getAgentTitleTha());
      query.setParameter("agentFirstnameTha", agentsBean.getAgentFirstnameTha());
      query.setParameter("agentLastnameTha", agentsBean.getAgentLastnameTha());
      query.setParameter("agentTitleEng", agentsBean.getAgentTitleEng());
      query.setParameter("agentFirstnameEng", agentsBean.getAgentFirstnameEng());
      query.setParameter("agentLastnameEng", agentsBean.getAgentLastnameEng());
      query.setParameter("dob", agentsBean.getDob());
      query.setParameter("gender", agentsBean.getGender());
      query.setParameter("agentStatus", agentsBean.getAgentStatus());
      query.setParameter("appointmentDate", agentsBean.getAppointmentDate());
      query.setParameter("contractDate", agentsBean.getContractDate());
      query.setParameter("addr1", agentsBean.getAddr1());
      query.setParameter("addr2", agentsBean.getAddr2());
      query.setParameter("addr3", agentsBean.getAddr3());
      query.setParameter("addr4", agentsBean.getAddr4());
      query.setParameter("zipCode", agentsBean.getZipCode());
      query.setParameter("homePhone", agentsBean.getHomePhone());
      query.setParameter("officePhone", agentsBean.getOfficePhone());
      query.setParameter("mobilePhone", agentsBean.getMobilePhone());
      query.setParameter("email", agentsBean.getEmail());
      query.setParameter("effectiveDate", agentsBean.getEffectiveDate());
      query.setParameter("licenseCode", agentsBean.getLicenseCode());
      query.setParameter("startDate", agentsBean.getStartDate());
      query.setParameter("expireDate", agentsBean.getExpireDate());
      query.setParameter("firstIssuedDate", agentsBean.getFirstIssuedDate());
      query.setParameter("createBy", agentsBean.getCreateBy());
      query.setParameter("createDate", agentsBean.getCreateDate());
      query.setParameter("updateBy", agentsBean.getUpdateBy());
      query.setParameter("updateDate", agentsBean.getUpdateDate());
      query.setParameter("agentPosId", agentsBean.getAgentPosId());
      query.setParameter("terminateDate", agentsBean.getTerminateDate());
      query.setParameter("terminateReason", agentsBean.getTerminateReason());
      query.setParameter("isIndividual", agentsBean.getIsIndividual());
      query.setParameter("uplineCode", agentsBean.getUplineCode());
      query.setParameter("recruiterCode", agentsBean.getRecruiterCode());
      query.setParameter("workMonth", agentsBean.getWorkMonth());
      query.setParameter("saleChannelCode", agentsBean.getSaleChannelCode());
      // query.setParameter("position", agentsBean.getPosition());
      // int rowUpdate =
      query.executeUpdate();
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into IPG_PSN_INF. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;
  }

  @Override
  public boolean insertListAgents(final List<AgentsBeanICOM> agentsBeans) throws Exception {

    boolean result = false;
    int rowUpdate = 0;
    Session session = null;

    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {
        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO IPG.IPG_PSN_INF (SCHEDULE_ID, AGENT_CODE, NATIONAL_ID,"
                + "    AGENT_TITLE_THA, AGENT_FIRSTNAME_THA, AGENT_LASTNAME_THA, AGENT_TITLE_ENG,"
                + "    AGENT_FIRSTNAME_ENG, AGENT_LASTNAME_ENG, DOB, GENDER, AGENT_STATUS,"
                + "    APPOINTMENT_DATE, CONTRACT_DATE, ADDR_1, ADDR_2, ADDR_3, ADDR_4, ZIP_CODE,"
                + "    HOME_PHONE, OFFICE_PHONE, MOBILE_PHONE, EMAIL, EFFECTIVE_DATE, LICENSE_CODE,"
                + "    START_DATE, EXPIRE_DATE, FIRST_ISSUED_DATE, CREATE_BY, CREATE_DATE, "
                + "    UPDATE_BY, UPDATE_DATE, AGENT_POS_ID, TERMINATE_DATE, TERMINATE_REASON, "
                + "    IS_INDIVIDUAL, UPLINE_CODE, RECRUITER_CODE, WORK_MONTH, SALE_CHANNEL_CODE)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                + "        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (AgentsBeanICOM agentsBean : agentsBeans) {
              pstmt.setString(1, agentsBean.getScheduleId());
              pstmt.setString(2, agentsBean.getAgentCode());
              pstmt.setString(3, agentsBean.getNationalId());
              pstmt.setString(4, agentsBean.getAgentTitleTha());
              pstmt.setString(5, agentsBean.getAgentFirstnameTha());
              pstmt.setString(6, agentsBean.getAgentLastnameTha());
              pstmt.setString(7, agentsBean.getAgentTitleEng());
              pstmt.setString(8, agentsBean.getAgentFirstnameEng());
              pstmt.setString(9, agentsBean.getAgentLastnameEng());
              pstmt.setDate(10, agentsBean.getDob() == null ? null
                  : new java.sql.Date(agentsBean.getDob().getTime()));
              pstmt.setString(11, agentsBean.getGender());
              pstmt.setString(12, agentsBean.getAgentStatus());
              pstmt.setDate(13, agentsBean.getAppointmentDate() == null ? null
                  : new java.sql.Date(agentsBean.getAppointmentDate().getTime()));
              pstmt.setDate(14, agentsBean.getContractDate() == null ? null
                  : new java.sql.Date(agentsBean.getContractDate().getTime()));
              pstmt.setString(15, agentsBean.getAddr1());
              pstmt.setString(16, agentsBean.getAddr2());
              pstmt.setString(17, agentsBean.getAddr3());
              pstmt.setString(18, agentsBean.getAddr4());
              pstmt.setString(19, agentsBean.getZipCode());
              pstmt.setString(20, agentsBean.getHomePhone());
              pstmt.setString(21, agentsBean.getOfficePhone());
              pstmt.setString(22, agentsBean.getMobilePhone());
              pstmt.setString(23, agentsBean.getEmail());
              pstmt.setDate(24, agentsBean.getEffectiveDate() == null ? null
                  : new java.sql.Date(agentsBean.getEffectiveDate().getTime()));
              pstmt.setString(25, agentsBean.getLicenseCode());
              pstmt.setDate(26, agentsBean.getStartDate() == null ? null
                  : new java.sql.Date(agentsBean.getStartDate().getTime()));
              pstmt.setDate(27, agentsBean.getExpireDate() == null ? null
                  : new java.sql.Date(agentsBean.getExpireDate().getTime()));
              pstmt.setDate(28, agentsBean.getFirstIssuedDate() == null ? null
                  : new java.sql.Date(agentsBean.getFirstIssuedDate().getTime()));
              pstmt.setBigDecimal(29, agentsBean.getCreateBy());
              pstmt.setDate(30, agentsBean.getCreateDate() == null ? null
                  : new java.sql.Date(agentsBean.getCreateDate().getTime()));
              pstmt.setBigDecimal(31, agentsBean.getUpdateBy());
              pstmt.setDate(32, agentsBean.getUpdateDate() == null ? null
                  : new java.sql.Date(agentsBean.getUpdateDate().getTime()));
              pstmt.setBigDecimal(33, agentsBean.getAgentPosId());
              pstmt.setDate(34, agentsBean.getTerminateDate() == null ? null
                  : new java.sql.Date(agentsBean.getTerminateDate().getTime()));
              pstmt.setString(35, agentsBean.getTerminateReason());
              pstmt.setString(36, agentsBean.getIsIndividual());
              pstmt.setString(37, agentsBean.getUplineCode());
              pstmt.setString(38, agentsBean.getRecruiterCode());
              pstmt.setBigDecimal(39, agentsBean.getWorkMonth());
              pstmt.setString(40, agentsBean.getSaleChannelCode());
              pstmt.addBatch();
              if (i % batchSizeStagingToIpg == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      // rowUpdate = agentsBeans.size();
      // if (rowUpdate > 0) {
      result = true;
      // }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_INF. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return result;
  }

  @Override
  public boolean insertPerformance(PerformanceBean performanceBean) {
    // logger.debug("Method starts.");

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_PSN_PROD (POLICY_NO, VER, AGENT_CODE, AGENT_POS_ID,"
        + "     PROD_TYPE_ID, POLICY_TYPE_ID, PROD_AMOUNT, INPUT_TYPE, SCHEDULE_ID, POLICY_YEAR,"
        + "     RIDER_NO, ACK_DATE, PERFORMANCE_MONTH, PERFORMANCE_YEAR)"
        + " VALUES (:policyNo, :ver, :agentCode, :agentPosId, :prodTypeId, :policyTypeId, "
        + "     :prodAmount, :inputType, :scheduleId, :policyYear, :riderCode, :ackDate,"
        + "     :performanceMonth, :performanceYear)";
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("policyNo", performanceBean.getPolicyNo());
      query.setParameter("ver", performanceBean.getVer());
      query.setParameter("agentCode", performanceBean.getAgentCode());
      query.setParameter("agentPosId", performanceBean.getAgentPosId());
      query.setParameter("prodTypeId", performanceBean.getProdTypeId());
      query.setParameter("policyTypeId", performanceBean.getPolicyTypeId());
      query.setParameter("prodAmount", performanceBean.getProdAmount());
      query.setParameter("inputType", performanceBean.getInputType());
      query.setParameter("scheduleId", performanceBean.getScheduleId());
      query.setParameter("policyYear", performanceBean.getPolicyYear());
      query.setParameter("riderCode", performanceBean.getRiderCode());
      query.setParameter("ackDate", performanceBean.getAckDate());
      query.setParameter("performanceMonth", performanceBean.getPerformanceMonth());
      query.setParameter("performanceYear", performanceBean.getPerformanceYear());
      // int rowUpdate = query.executeUpdate();
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_INF. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;

  }

  @Override
  public boolean insertListPerformance(final List<PerformanceBeanICOM> performanceBeanICOMs)
      throws Exception {

    boolean result = false;
    int rowUpdate = 0;
    Session session = null;
    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {
        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO IPG.IPG_PSN_PROD (POLICY_NO, VER, AGENT_CODE, AGENT_POS_ID,"
                + "     PROD_TYPE_ID, POLICY_TYPE_ID, PROD_AMOUNT, INPUT_TYPE, SCHEDULE_ID,"
                + "     POLICY_YEAR, RIDER_NO, ACK_DATE, PERFORMANCE_MONTH, PERFORMANCE_YEAR)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (PerformanceBeanICOM performanceBeanIcom : performanceBeanICOMs) {
              pstmt.setString(1, performanceBeanIcom.getPolicyNo());
              pstmt.setBigDecimal(2, performanceBeanIcom.getVer());
              pstmt.setString(3, performanceBeanIcom.getAgentCode());
              pstmt.setBigDecimal(4, performanceBeanIcom.getAgentPosId());
              pstmt.setBigDecimal(5, performanceBeanIcom.getProdTypeId());
              pstmt.setBigDecimal(6, performanceBeanIcom.getPolicyTypeId());
              pstmt.setBigDecimal(7, performanceBeanIcom.getProdAmount());
              pstmt.setString(8, performanceBeanIcom.getInputType());
              pstmt.setString(9, performanceBeanIcom.getScheduleId());
              pstmt.setInt(10, performanceBeanIcom.getPolicyYear());
              pstmt.setString(11, performanceBeanIcom.getRiderCode());
              pstmt.setDate(12, performanceBeanIcom.getAckDate() == null ? null
                  : new java.sql.Date(performanceBeanIcom.getAckDate().getTime()));
              pstmt.setString(13, performanceBeanIcom.getPerformanceMonth());
              pstmt.setString(14, performanceBeanIcom.getPerformanceYear());
              pstmt.addBatch();
              // 10000 : JDBC batch size
              if (i % batchSizeStagingToIpg == 0) {
                pstmt.executeBatch();
              }
              i++;
              if (i % 100 == 0) {
                logger.debug("Performance beans looped through " + i + " records.");
                System.out.println("Performance beans looped through " + i + " records.");
              }
            }
            pstmt.executeBatch();
          } finally {
            try {
              pstmt.close();
            } catch (NullPointerException exNull) {
              // Do Nothing.
            }
          }
        }
      });
      rowUpdate = performanceBeanICOMs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_PROD. "
          + " Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return result;
  }

  @Override
  public boolean insertTier(TierBean tierBean) {

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_PSN_TIER (SCHEDULE_ID, AGENT_CODE, UPLINE_L1_CODE)"
        + " VALUES (:scheduleId, :agentCode, :uplineL1Code)";
    int rowUpdate = 0;
    Session session = null;

    try {
      logger.debug("SQL executed with parameter: " + tierBean.getScheduleId() + "; "
          + tierBean.getAgentCode() + "; " + tierBean.getUplineL1Code());
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", tierBean.getScheduleId());
      query.setParameter("agentCode", tierBean.getAgentCode());
      query.setParameter("uplineL1Code", tierBean.getUplineL1Code());
      rowUpdate = query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_TIER. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;

  }

  @Override
  public boolean insertSchedule(ScheduleBean scheduleBean) {

    boolean result = false;
    logger.debug("Method starts.");

    String sql = "INSERT INTO IPG.IPG_SCHEDULE_ID (SCHEDULE_ID, PERIOD_FROM, PERIOD_TO,"
        + "     PAYMENT_DATE, CYCLE_NAME, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE,"
        + "     SCHEDULE_DESC, IS_PAID, IS_DELETED, PERIOD_YEAR, PERIOD_MONTH)"
        + " VALUES (:scheduleId, :periodFrom, :periodTo, :paymentDate, :cycleName, :createBy, "
        + "    :createDate, :updateBy, :updateDate, :scheduleDesc, :isPaid, :isDeleted,"
        + "    :periodYear, :periodMonth)";
    // int rowUpdate = 0;
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", scheduleBean.getScheduleId());
      query.setParameter("periodFrom", scheduleBean.getPeriodFrom());
      query.setParameter("periodTo", scheduleBean.getPeriodTo());
      query.setParameter("paymentDate", scheduleBean.getPaymentDate());
      query.setParameter("cycleName", scheduleBean.getCycleName());
      query.setParameter("createBy", scheduleBean.getCreateBy());
      query.setParameter("createDate", scheduleBean.getCreateDate());
      query.setParameter("updateBy", scheduleBean.getUpdateBy());
      query.setParameter("updateDate", scheduleBean.getUpdateDate());
      query.setParameter("scheduleDesc", scheduleBean.getScheduleDesc());
      query.setParameter("isPaid", scheduleBean.getIsPaid());
      query.setParameter("isDeleted", scheduleBean.getIsDeleted());
      query.setParameter("periodYear", scheduleBean.getPeriodYear());
      query.setParameter("periodMonth", scheduleBean.getPeriodMonth());
      // rowUpdate = query.executeUpdate();
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_SCHEDULE_ID. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;

  }

  @Override
  public boolean insertVersion(VersionBean versionBean) {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;

    String sql = "INSERT INTO IPG.IPG_SCHEDULE_VER (SCHEDULE_ID, VER, CREATE_BY, CREATE_DATE,"
        + "     IS_DELETED) VALUES (:scheduleId, :ver, :createBy, :createDate, :isDeleted)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", versionBean.getScheduleId());
      query.setParameter("ver", versionBean.getVer());
      query.setParameter("createBy", versionBean.getCreateBy());
      query.setParameter("createDate", versionBean.getCreateDate());
      query.setParameter("isDeleted", versionBean.getIsDelete());
      rowUpdate = query.executeUpdate();
      // this method must ALWAYS insert one record regardless of the data. No rows means error.
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_INF. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;
  }

  @Override
  public boolean insertSetSchedule(String scheduleId, BigDecimal mainVersion, BigDecimal createBy) {

    boolean result = false;
    logger.debug("Method starts.");
    String sql = "INSERT INTO IPG.IPG_SET_SCH_VER VALUES(:scheduleId, :mainVersion, :createBy, "
        + "GETDATE())";
    // int rowUpdate = 0;
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", scheduleId);
      query.setParameter("mainVersion", mainVersion);
      query.setParameter("createBy", createBy);
      // rowUpdate = query.executeUpdate();
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_SET_SCH_VER. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }

    return result;
  }

  @Override
  public boolean deleteBenefitICOM(String scheduleId) {

    boolean result = false;
    logger.info("Clearing data from ICOM_BENEFIT_CAL of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_BENEFIT_CAL WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sqldel);
      query.setParameter("scheduleId", scheduleId);
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_BENEFIT_CAL. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    return result;
  }

  @Override
  public boolean deletePolicyClawbackICOM(String scheduleId) {
    logger.debug("Method starts.");

    boolean result = false;
    logger.info("Clearing data from ICOM_CLAWBACK_AGENT of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_CLAWBACK_AGENT WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sqldel);
      query.setParameter("scheduleId", scheduleId);
      // int rowUpdate = queryDel.executeUpdate();
      // if (rowUpdate > 0) {
      // logger.info(rowUpdate + " records deleted from ICOM_CLAWBACK_AGENT.");
      // }
    } catch (Exception exGen) {
      logger.error(
          "An error occurred while trying to insert data into ICOM_CLAWBACK_AGENT. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    logger.debug("Method ends.");
    return result;

  }

  @Override
  public boolean deleteAgentsICOM(String scheduleId) {
    logger.debug("Method starts.");

    boolean result = false;
    logger.info("Clearing data from ICOM_PSN_INF of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_PSN_INF WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("scheduleId", scheduleId);
      // int rowUpdate =
      queryDel.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_INF. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return result;
  }

  @Override
  public boolean deletePerformanceICOM(String scheduleId) {

    logger.debug("Method starts.");
    boolean result = false;
    logger.info("Clearing data from ICOM_PSN_PROD of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_PSN_PROD WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("scheduleId", scheduleId);
      queryDel.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_PSN_PROD. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    return result;
  }

  @Override
  public boolean deleteTierICOM(String scheduleId) {
    logger.debug("Method starts.");

    boolean result = false;
    logger.info("Clearing data from ICOM_PSN_TIER of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_PSN_TIER WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("scheduleId", scheduleId);
      queryDel.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_PSN_TIER. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    logger.debug("Method ends.");
    return result;

  }

  @Override
  public boolean deleteScheduleICOM(String scheduleId) {
    logger.debug("Method starts.");

    boolean result = false;
    logger.info("Clearing data from ICOM_SCHEDULE_ID of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_SCHEDULE_ID WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("scheduleId", scheduleId);
      queryDel.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_SCHEDULE_ID. "
          + "Stack trace is", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    logger.debug("Method ends.");
    return result;

  }

  @Override
  public boolean deleteVersionICOM(String scheduleId) {

    logger.debug("Method starts.");
    boolean result = false;
    logger.info("Clearing data from ICOM_SCHEDULE_VER of iCompensation staging DB.");
    String sqldel = "DELETE FROM ICOM.ICOM_SCHEDULE_VER WHERE SCHEDULE_ID = :scheduleId";
    Session session = null;
    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("scheduleId", scheduleId);
      queryDel.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into IPG_PSN_INF. Stack trace is",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    logger.debug("Method ends.");
    return result;
  }

  @Override
  public BigDecimal getNewVer(String scheduleId) {

    BigDecimal newVer = null;
    logger.debug("Method starts.");
    String sql = "SELECT COALESCE(MAX(ver),0) + 1 AS newver FROM icom.icom_schedule_ver "
        + " WHERE schedule_id = :scheduleId";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", scheduleId);
      List<Object> listResult = query.list();
      newVer = (BigDecimal) listResult.get(0);
    } catch (Exception exGen) {
      logger.error("An error occurred while searching for new schedule version. Stack trace is\n",
          exGen);
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    logger.debug("Method ends.");

    return newVer;

  }

  @Override
  public boolean insertBenefitICOM(BenefitBeanICOM benefitBeanICOM) {

    boolean result = false;
    logger.debug("Method starts.");
    String sql = "INSERT INTO ICOM.ICOM_BENEFIT_CAL (SCHEDULE_ID, VER, BENEFIT_RESULT_ID,"
        + "    BENEFIT_CODE, AGENT_CODE, BENEFIT_AMOUNT, POLICY_NO, RIDER_NO,"
        + "    RECAL_SCHEDULE_ID, IS_DELETED, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY) "
        + " VALUES (:calScheduleId, :ver, :benefitResultId, :benefitCode, "
        + "    :agentCode, :benefitAmount, :policyNo, :riderNo, :recalScheduleId, :isDeleted, "
        + "    :createDate, :createBy, :updateDate, :updateBy)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("calScheduleId", benefitBeanICOM.getCalScheduleId());
      query.setParameter("ver", benefitBeanICOM.getVer());
      query.setParameter("benefitResultId", benefitBeanICOM.getBenefitResultId());
      query.setParameter("benefitCode", benefitBeanICOM.getBenefitCode());
      query.setParameter("agentCode", benefitBeanICOM.getAgentCode());
      query.setParameter("benefitAmount", benefitBeanICOM.getBenefitAmount());
      query.setParameter("policyNo", benefitBeanICOM.getPolicyNo());
      query.setParameter("riderNo", benefitBeanICOM.getRiderNo());
      query.setParameter("recalScheduleId", benefitBeanICOM.getRecalScheduleId());
      query.setParameter("isDeleted", benefitBeanICOM.getIsDeleted());
      query.setParameter("createDate", benefitBeanICOM.getCreateDate());
      query.setParameter("createBy", benefitBeanICOM.getCreateBy());
      query.setParameter("updateDate", benefitBeanICOM.getUpdateDate());
      query.setParameter("updateBy", benefitBeanICOM.getUpdateBy());
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting into ICOM_BENEFIT_CAL. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing
      }
    }

    result = true;
    logger.debug("Method ends.");
    return result;

  }

  @Override
  public boolean insertPolicyClawbackICOM(PolicyClawbackBeanICOM policyClawbackBeanICOM)
      throws Exception {
    logger.debug("Method starts.");

    boolean result = false;
    String sql = "INSERT INTO ICOM.ICOM_CLAWBACK_AGENT (SCHEDULE_ID, VER, CLAWBACK_ID, AGENT_CODE,"
        + "    POLICY_NO, RIDER_NO, RECAL_SCHEDULE_ID, PROD_TYPE_ID, PROD_AMOUNT, CREATE_DATE, "
        + "    CREATE_BY, UPDATE_DATE, UPDATE_BY) "
        + " VALUES (:scheduleId, :ver, :clawbackId, :agentCode, :policyNo, :riderNo, "
        + "    :recalScheduleId, :prodTypeId, :prodAmount, :createDate, :createBy, :updateDate, "
        + "    :updateBy)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", policyClawbackBeanICOM.getScheduleId());
      query.setParameter("ver", policyClawbackBeanICOM.getVer());
      query.setParameter("clawbackId", policyClawbackBeanICOM.getClawbackId());
      query.setParameter("agentCode", policyClawbackBeanICOM.getAgentCode());
      query.setParameter("policyNo", policyClawbackBeanICOM.getPolicyNo());
      query.setParameter("riderNo", policyClawbackBeanICOM.getRiderNo());
      query.setParameter("recalScheduleId", policyClawbackBeanICOM.getRecalScheduleId());
      query.setParameter("prodTypeId", policyClawbackBeanICOM.getProdTypeId());
      query.setParameter("prodAmount", policyClawbackBeanICOM.getProdAmount());
      query.setParameter("createDate", policyClawbackBeanICOM.getCreateDate());
      query.setParameter("createBy", policyClawbackBeanICOM.getCreateBy());
      query.setParameter("updateDate", policyClawbackBeanICOM.getUpdateDate());
      query.setParameter("updateBy", policyClawbackBeanICOM.getUpdateBy());
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into ICOM_CLAWBACK_AGENT. "
          + "Stack trace is", exGen);
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;

    logger.debug("Method ends.");
    return result;
  }

  @Override
  public boolean insertAgentsICOM(AgentsBeanICOM agentsBeanICOM) {
    logger.debug("Method starts.");

    boolean result = false;
    String sql = "INSERT INTO ICOM.ICOM_PSN_INF (SCHEDULE_ID, AGENT_CODE, NATIONAL_ID,"
        + "    AGENT_TITLE_THA, AGENT_FIRSTNAME_THA, AGENT_LASTNAME_THA, AGENT_TITLE_ENG,"
        + "    AGENT_FIRSTNAME_ENG, AGENT_LASTNAME_ENG, DOB, GENDER, AGENT_STATUS,"
        + "    APPOINTMENT_DATE, CONTRACT_DATE, ADDR_1, ADDR_2, ADDR_3, ADDR_4, ZIP_CODE,"
        + "    HOME_PHONE, OFFICE_PHONE, MOBILE_PHONE, EMAIL, EFFECTIVE_DATE, LICENSE_CODE,"
        + "    START_DATE, EXPIRE_DATE, FIRST_ISSUED_DATE, CREATE_BY, CREATE_DATE, UPDATE_BY,"
        + "    UPDATE_DATE, AGENT_POS_ID, TERMINATE_DATE, TERMINATE_REASON, IS_INDIVIDUAL,"
        + "    UPLINE_CODE, RECRUITER_CODE, WORK_MONTH, SALE_CHANNEL_CODE) "
        + " VALUES (:scheduleId, :agentCode, :nationalId, :agentTitleTha, :agentFirstnameTha, "
        + "    :agentLastnameTha, :agentTitleEng, :agentFirstnameEng, :agentLastnameEng, :dob, "
        + "    :gender, :agentStatus, :appointmentDate, :contractDate, :addr1, :addr2, :addr3, "
        + "    :addr4, :zipCode, :homePhone, :officePhone, :mobilePhone, :email, :effectiveDate, "
        + "    :licenseCode, :startDate, :expireDate, :firstIssuedDate, :createBy, :createDate, "
        + "    :updateBy, :updateDate, :agentPosId, :terminateDate, :terminateReason, "
        + "    :isIndividual, :uplineCode, :recruiterCode, :workMonth, :saleChannelCode) ";
    int rowUpdate = 0;
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", agentsBeanICOM.getScheduleId());
      query.setParameter("agentCode", agentsBeanICOM.getAgentCode());
      query.setParameter("nationalId", agentsBeanICOM.getNationalId());
      query.setParameter("agentTitleTha", agentsBeanICOM.getAgentTitleTha());
      query.setParameter("agentFirstnameTha", agentsBeanICOM.getAgentFirstnameTha());
      query.setParameter("agentLastnameTha", agentsBeanICOM.getAgentLastnameTha());
      query.setParameter("agentTitleEng", agentsBeanICOM.getAgentTitleEng());
      query.setParameter("agentFirstnameEng", agentsBeanICOM.getAgentFirstnameEng());
      query.setParameter("agentLastnameEng", agentsBeanICOM.getAgentLastnameEng());
      query.setParameter("dob", agentsBeanICOM.getDob());
      query.setParameter("gender", agentsBeanICOM.getGender());
      query.setParameter("agentStatus", agentsBeanICOM.getAgentStatus());
      query.setParameter("appointmentDate", agentsBeanICOM.getAppointmentDate());
      query.setParameter("contractDate", agentsBeanICOM.getContractDate());
      query.setParameter("addr1", agentsBeanICOM.getAddr1());
      query.setParameter("addr2", agentsBeanICOM.getAddr2());
      query.setParameter("addr3", agentsBeanICOM.getAddr3());
      query.setParameter("addr4", agentsBeanICOM.getAddr4());
      query.setParameter("zipCode", agentsBeanICOM.getZipCode());
      query.setParameter("homePhone", agentsBeanICOM.getHomePhone());
      query.setParameter("officePhone", agentsBeanICOM.getOfficePhone());
      query.setParameter("mobilePhone", agentsBeanICOM.getMobilePhone());
      query.setParameter("email", agentsBeanICOM.getEmail());
      query.setParameter("effectiveDate", agentsBeanICOM.getEffectiveDate());
      query.setParameter("licenseCode", agentsBeanICOM.getLicenseCode());
      query.setParameter("startDate", agentsBeanICOM.getStartDate());
      query.setParameter("expireDate", agentsBeanICOM.getExpireDate());
      query.setParameter("firstIssuedDate", agentsBeanICOM.getFirstIssuedDate());
      query.setParameter("createBy", agentsBeanICOM.getCreateBy());
      query.setParameter("createDate", agentsBeanICOM.getCreateDate());
      query.setParameter("updateBy", agentsBeanICOM.getUpdateBy());
      query.setParameter("updateDate", agentsBeanICOM.getUpdateDate());
      query.setParameter("agentPosId", agentsBeanICOM.getAgentPosId());
      query.setParameter("terminateDate", agentsBeanICOM.getTerminateDate());
      query.setParameter("terminateReason", agentsBeanICOM.getTerminateReason());
      query.setParameter("isIndividual", agentsBeanICOM.getIsIndividual());
      query.setParameter("uplineCode", agentsBeanICOM.getUplineCode());
      query.setParameter("recruiterCode", agentsBeanICOM.getRecruiterCode());
      query.setParameter("workMonth", agentsBeanICOM.getWorkMonth());
      query.setParameter("saleChannelCode", agentsBeanICOM.getSaleChannelCode());
      // query.setParameter("positionId", agentsBeanICOM.getPosition());
      rowUpdate = query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into ICOM_PSN_INF. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    result = true;
    return result;
  }

  @Override
  public boolean insertPerformanceICOM(PerformanceBeanICOM performanceBeanICOM) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");

    String sql = "INSERT INTO ICOM.ICOM_PSN_PROD (POLICY_NO, VER, AGENT_CODE, AGENT_POS_ID,"
        + "    PROD_TYPE_ID, POLICY_TYPE_ID, PROD_AMOUNT, INPUT_TYPE, SCHEDULE_ID, POLICY_YEAR,"
        + " RIDER_NO, ACK_DATE, PERFORMANCE_MONTH, PERFORMANCE_YEAR) "
        + " VALUES (:policyNo, :ver, :agentCode, :agentPosId, :prodTypeId, :policyTypeId, "
        + "    :prodAmount, :inputType, :scheduleId, :policyYear, :riderCode, :ackDate, "
        + "    :performanceMonth, :performanceYear)";
    Session session = null;
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("policyNo", performanceBeanICOM.getPolicyNo());
      query.setParameter("ver", performanceBeanICOM.getVer());
      query.setParameter("agentCode", performanceBeanICOM.getAgentCode());
      query.setParameter("agentPosId", performanceBeanICOM.getAgentPosId());
      query.setParameter("prodTypeId", performanceBeanICOM.getProdTypeId());
      query.setParameter("policyTypeId", performanceBeanICOM.getPolicyTypeId());
      query.setParameter("prodAmount", performanceBeanICOM.getProdAmount());
      query.setParameter("inputType", performanceBeanICOM.getInputType());
      query.setParameter("scheduleId", performanceBeanICOM.getScheduleId());
      query.setParameter("policyYear", performanceBeanICOM.getPolicyYear());
      query.setParameter("riderCode", performanceBeanICOM.getRiderCode());
      query.setParameter("ackDate", performanceBeanICOM.getAckDate());
      query.setParameter("performanceMonth", performanceBeanICOM.getPerformanceMonth());
      query.setParameter("performanceYear", performanceBeanICOM.getPerformanceYear());
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into icom_psn_prod. Stack trace is\n",
          exGen);
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    return result;
  }

  /**
   * Insert data from iCompensation main database into IPG staging database's ICOM_PSN_TIER table.
   * 
   * @param tierBeanICOM the data row to be added
   * @return Boolean <strong>true</strong> if adding was successful or <strong>false</strong> if
   *         not.
   */
  @Override
  public boolean insertTierICOM(TierBeanICOM tierBeanICOM) throws Exception {

    boolean result = false;
    String sql = "INSERT INTO icom.icom_psn_tier (schedule_id, agent_code, upline_l1_code)"
        + "    VALUES (:scheduleId, :agentCode, :uplineL1Code)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", tierBeanICOM.getScheduleId());
      query.setParameter("agentCode", tierBeanICOM.getAgentCode());
      query.setParameter("uplineL1Code", tierBeanICOM.getUplineL1Code());
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into ICOM_PSN_TIER. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    return result;
  }

  /**
   * Insert data into ICOM's ICOM_SCHEDULE_ID Table.
   * 
   * @param scheduleBeanICOM The schedule data to be inserted.
   * @return <strong>True</strong> if insertion was a success; <strong>false</strong> otherwise.
   */
  @Override
  public boolean insertScheduleICOM(ScheduleBeanICOM scheduleBeanICOM) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");

    String sql = "INSERT INTO ICOM.ICOM_SCHEDULE_ID (SCHEDULE_ID, PERIOD_FROM, PERIOD_TO,"
        + "    PAYMENT_DATE, CYCLE_NAME, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE,"
        + "    SCHEDULE_DESC, IS_PAID, IS_DELETED, PERIOD_YEAR, PERIOD_MONTH)"
        + " VALUES (:scheduleId, :periodFrom, :periodTo, :paymentDate, :cycleName, :createBy,"
        + "    :createDate, :updateBy, :updateDate, :scheduleDesc, :isPaid, :isDeleted,"
        + "    :periodYear, :periodMonth)";
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("scheduleId", scheduleBeanICOM.getScheduleId());
      query.setParameter("periodFrom", scheduleBeanICOM.getPeriodFrom());
      query.setParameter("periodTo", scheduleBeanICOM.getPeriodTo());
      query.setParameter("paymentDate", scheduleBeanICOM.getPaymentDate());
      query.setParameter("cycleName", scheduleBeanICOM.getCycleName());
      query.setParameter("createBy", scheduleBeanICOM.getCreateBy());
      query.setParameter("createDate", scheduleBeanICOM.getCreateDate());
      query.setParameter("updateBy", scheduleBeanICOM.getUpdateBy());
      query.setParameter("updateDate", scheduleBeanICOM.getUpdateDate());
      query.setParameter("scheduleDesc", scheduleBeanICOM.getScheduleDesc());
      query.setParameter("isPaid", scheduleBeanICOM.getIsPaid());
      query.setParameter("isDeleted", scheduleBeanICOM.getIsDeleted());
      query.setParameter("periodYear", scheduleBeanICOM.getPeriodYear());
      query.setParameter("periodMonth", scheduleBeanICOM.getPeriodMonth());
      query.executeUpdate();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_SCHEDULE_ID. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    result = true;
    return result;
  }

  @Override
  public boolean insertVersionICOM(VersionBeanICOM versionBeanICOM) throws Exception {

    boolean result = false;
    String sql = "INSERT INTO ICOM.ICOM_SCHEDULE_VER (SCHEDULE_ID, VER, CREATE_BY, CREATE_DATE,"
        + " IS_DELETED) VALUES (:scheduleId, :ver, :createBy, :createDate, :isDelete)";
    Query query = null;

    try {
      if (this.session == null) {
        query = this.getSession().createSQLQuery(sql);
      } else {
        query = this.session.createSQLQuery(sql);
      }
      query.setParameter("scheduleId", versionBeanICOM.getScheduleId());
      query.setParameter("ver", versionBeanICOM.getVer());
      query.setParameter("createBy", versionBeanICOM.getCreateBy());
      query.setParameter("createDate", versionBeanICOM.getCreateDate());
      query.setParameter("isDelete", versionBeanICOM.getIsDelete());
      int rowUpdate = query.executeUpdate();

      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while insertint ICOM_SCHEDULE_VER. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    return result;
  }

  /**
   * List users from iCompensation main database.
   * 
   * @return List of user who are in IPG_USER or IPG_ADMIN group (in group description)
   * @throws ClassNotFoundException When hibernate cannot load database driver.
   * @throws SQLException When the connection cannot be made, query contains errors, or the ORM
   *         encountered an error.
   */
  @Override
  public List<UserBean> listUser() throws ClassNotFoundException, SQLException {

    List<UserBean> resultList = new ArrayList<UserBean>();

    // This query is more complex than necessary because we have no idea how users group gonna
    // be made. They might add just IPG_ADMIN or IPG_USER or might add both.
    String sql = "SELECT u.user_id, u.login_name, u.password, 'INTERNAL' as user_type,"
        + "    CASE u.is_deleted WHEN 'T' THEN 'Y' ELSE 'N' END AS is_deleted,"
        + "    u.create_by, u.create_date, u.update_by, SYSDATE as update_date,"
        + "    COALESCE(ug.group_user,'N') AS isuser, COALESCE(ua.group_admin,'N') AS isadmin"
        + " FROM ACS_USER u"
        + "   LEFT JOIN (SELECT user_id, 'Y' AS group_user FROM acs_user_group ug"
        + "         INNER JOIN acs_group g ON ug.group_id = g.group_id"
        + "       WHERE g.group_name = 'IPG_USER') ug ON u.user_id = ug.user_id"
        + "   LEFT JOIN (SELECT useR_id, 'Y' AS group_admin FROM acs_user_group ug"
        + "         INNER JOIN acs_group g ON ug.group_id = g.group_id"
        + "         WHERE g.group_name = 'IPG_ADMIN') ua ON u.user_id = ua.user_id"
        + " WHERE group_admin IS NOT NULL OR (group_user IS NOT NULL)";

    try (PreparedStatement pstmt = connectionManager.getDbConnection().prepareStatement(sql)) {
      rst = pstmt.executeQuery();
      logger.info("Tier query prepared. Begin executing");
      while (rst.next()) {
        UserBean newUser = new UserBean();
        newUser.setUserId(rst.getBigDecimal("user_id"));
        newUser.setLoginName(rst.getString("login_name"));
        newUser.setPassword(rst.getString("password"));
        newUser.setUserType(rst.getString("user_type"));
        newUser.setIsDeleted(rst.getString("is_deleted"));
        newUser.setCreateBy(rst.getString("create_by"));
        newUser.setCreateDate(rst.getDate("create_date"));
        newUser.setUpdateBy(rst.getString("update_by"));
        newUser.setUpdateDate(rst.getDate("update_date"));
        newUser.setAdmin("Y".equalsIgnoreCase(rst.getString("isadmin")));
        resultList.add(newUser);
      }
    } catch (ClassNotFoundException exCnf) {
      logger.error("An error occurred while trying to load class definistion. Stack trace is\n",
          exCnf);
      exCnf.printStackTrace();
      throw exCnf;
    } catch (SQLException exSql) {
      logger.error("An error occurred while running sql statement. Stack trace is\n", exSql);
      exSql.printStackTrace();
      throw exSql;
    } catch (Exception exGen) {
      logger.error("An error occurred while retrieving data from ACS_USER from iCompensation"
          + " Database. Stack trace is\n", exGen);
      exGen.printStackTrace();
      throw exGen;
    } finally {
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return resultList;
  }

  @Override
  public boolean deleteUser() {

    boolean result = false;
    String sqldel = "DELETE FROM IPG.IPG_USER WHERE LOGIN_NAME <> 'admin'";
    String sqlGroup = "DELETE FROM ipg.ipg_group_users g WHERE g.user_id NOT IN ("
        + " SELECT user_id FROM ipg.ipg_user u WHERE u.login_name = 'admin')";
    Session session = null;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      int rowUpdate = queryDel.executeUpdate();
      logger.info(rowUpdate + " users deleted from IPG_USER table.");
      Query querydelGroup = session.createSQLQuery(sqlGroup);
      int rowUpdateGroup = querydelGroup.executeUpdate();
      logger.info(rowUpdateGroup + " user-group relations deleted from IPG_USER table.");
      logger.info("There is no non-admin user-group relations to delete.");
      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while deleting groups and users. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    return result;
  }

  /**
   * Insert a user from ACS_USERS into IPG_USER and its group in IPG_GROUP_USER.
   */
  @Override
  public boolean insertUser(UserBean userBean) {

    boolean result = false;
    String sql = "INSERT INTO IPG.IPG_USER (USER_ID, LOGIN_NAME, PASSWORD, is_deleted, user_type,"
        + "    CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE)"
        + " VALUES (:userId, :loginName, :password, :isDeleted, :userType,"
        + "    :createBy, :createDate, :updateBy, :updateDate)";
    String sqlG = "INSERT INTO IPG.IPG_GROUP_USERS (USER_ID, GROUP_ID) VALUES (:uId, :gId)";
    int rowUpdate = 0;
    int groupRowInserted = 0;
    Session session = null;

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql);
      query.setParameter("userId", userBean.getUserId());
      query.setParameter("loginName", userBean.getLoginName());
      query.setParameter("password", userBean.getPassword());
      query.setParameter("userType", userBean.getUserType());
      query.setParameter("isDeleted", userBean.getIsDeleted());
      query.setParameter("createBy", userBean.getCreateBy());
      query.setParameter("createDate", userBean.getCreateDate());
      query.setParameter("updateBy", userBean.getUpdateBy());
      query.setParameter("updateDate", new Date());
      rowUpdate = query.executeUpdate();

      Query queryGroup = session.createSQLQuery(sqlG);
      queryGroup.setParameter("uId", userBean.getUserId());
      // 1 is IPG_ADMIN, 2 is IPG_USER. Try moving this configuration out.
      queryGroup.setParameter("gId", userBean.isAdmin() ? 1 : 2);
      groupRowInserted = queryGroup.executeUpdate();

    } catch (Exception exGen) {
      logger.error("An error occurred while updating user tables. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }

    if (groupRowInserted > 0 & rowUpdate > 0) {
      result = true;
    }
    return result;
  }

  @Override
  public boolean insertListScheduleICOM(final List<ScheduleBeanICOMDB> scheduleBeanICOMDBs)
      throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    Session session = null;

    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO ICOM.ICOM_SCHEDULE_ID (SCHEDULE_ID, PERIOD_FROM, PERIOD_TO,"
                + "    PAYMENT_DATE, CYCLE_NAME, CREATE_BY, CREATE_DATE, UPDATE_BY, UPDATE_DATE,"
                + "    SCHEDULE_DESC, IS_PAID, IS_DELETED, PERIOD_YEAR, PERIOD_MONTH)"
                + " VALUES (?, ?, ?, ?, ?, ?,"
                + "    ?, ?, ?, ?, ?, ?,"
                + "    ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (ScheduleBeanICOMDB scheduleBeanICOMDB : scheduleBeanICOMDBs) {
              pstmt.setString(1, scheduleBeanICOMDB.getScheduleId());
              pstmt.setDate(2, scheduleBeanICOMDB.getPeriodFrom() == null ? null
                  : new java.sql.Date(scheduleBeanICOMDB.getPeriodFrom().getTime()));
              pstmt.setDate(3, scheduleBeanICOMDB.getPeriodTo() == null ? null
                  : new java.sql.Date(scheduleBeanICOMDB.getPeriodTo().getTime()));
              pstmt.setDate(4, scheduleBeanICOMDB.getPaymentDate() == null ? null
                  : new java.sql.Date(scheduleBeanICOMDB.getPaymentDate().getTime()));
              pstmt.setString(5, scheduleBeanICOMDB.getCycleName());
              pstmt.setBigDecimal(6, scheduleBeanICOMDB.getCreateBy());
              pstmt.setDate(7, scheduleBeanICOMDB.getCreateDate() == null ? null
                  : new java.sql.Date(scheduleBeanICOMDB.getCreateDate().getTime()));
              pstmt.setBigDecimal(8, scheduleBeanICOMDB.getUpdateBy());
              pstmt.setDate(9, scheduleBeanICOMDB.getUpdateDate() == null ? null
                  : new java.sql.Date(scheduleBeanICOMDB.getUpdateDate().getTime()));
              pstmt.setString(10, scheduleBeanICOMDB.getScheduleDesc());
              pstmt.setString(11, scheduleBeanICOMDB.getIsPaid());
              pstmt.setString(12, scheduleBeanICOMDB.getIsDeleted());
              pstmt.setString(13, scheduleBeanICOMDB.getPeriodYear());
              pstmt.setString(14, scheduleBeanICOMDB.getPeriodMonth());
              pstmt.addBatch();
              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      rowUpdate = scheduleBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_SCHEDULE_ID. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;
  }

  @Override
  public boolean insertListBenefitICOM(final List<BenefitBeanICOMDB> benefitBeanICOMDBs,
      final BigDecimal newVer) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    Session session = null;

    try {

      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }

      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {

          PreparedStatement pstmt = null;

          try {

            String sql = "INSERT INTO ICOM.ICOM_BENEFIT_CAL (SCHEDULE_ID, VER, BENEFIT_RESULT_ID,"
                + "    BENEFIT_CODE, AGENT_CODE, BENEFIT_AMOUNT, POLICY_NO, RIDER_NO,"
                + "    RECAL_SCHEDULE_ID, IS_DELETED, CREATE_DATE, CREATE_BY, UPDATE_DATE,"
                + "    UPDATE_BY) "
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (BenefitBeanICOMDB benefitBeanICOMDB : benefitBeanICOMDBs) {

              pstmt.setString(1, benefitBeanICOMDB.getCalScheduleId());
              pstmt.setBigDecimal(2, newVer);
              pstmt.setBigDecimal(3, benefitBeanICOMDB.getBenefitResultId());
              // pstmt.setString(4, benefitBeanICOMDB.getBenefitCode());
              pstmt.setBigDecimal(4, benefitBeanICOMDB.getBenefitTypeId());
              pstmt.setString(5, benefitBeanICOMDB.getAgentCode());
              pstmt.setBigDecimal(6, benefitBeanICOMDB.getBenefitAmount());
              pstmt.setString(7, benefitBeanICOMDB.getPolicyNo());
              pstmt.setString(8, benefitBeanICOMDB.getRiderNo());
              pstmt.setString(9, benefitBeanICOMDB.getRecalScheduleId());
              pstmt.setString(10, benefitBeanICOMDB.getIsDeleted());
              pstmt.setDate(11, benefitBeanICOMDB.getCreateDate() == null ? null
                  : new java.sql.Date(benefitBeanICOMDB.getCreateDate().getTime()));
              pstmt.setBigDecimal(12, benefitBeanICOMDB.getCreateBy());
              pstmt.setDate(13, benefitBeanICOMDB.getUpdateDate() == null ? null
                  : new java.sql.Date(benefitBeanICOMDB.getUpdateDate().getTime()));
              pstmt.setBigDecimal(14, benefitBeanICOMDB.getUpdateBy());
              pstmt.addBatch();

              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });

      rowUpdate = benefitBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }

    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_BENEFIT_CAL. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;
  }

  @Override
  public boolean insertListPolicyClawbackICOM(
      final List<PolicyClawbackBeanICOMDB> policyClawbackBeanICOMDBs, final BigDecimal newVer)
      throws Exception {

    boolean result = false;
    logger.debug("Method starts.");

    int rowUpdate = 0;
    Session session = null;

    try {

      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }

      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {

          PreparedStatement pstmt = null;

          try {
            String sql = "INSERT INTO ICOM.ICOM_CLAWBACK_AGENT (SCHEDULE_ID, VER, CLAWBACK_ID, "
                + "    AGENT_CODE, POLICY_NO, RIDER_NO, RECAL_SCHEDULE_ID, PROD_TYPE_ID,"
                + "    PROD_AMOUNT, CREATE_DATE, CREATE_BY, UPDATE_DATE, UPDATE_BY) "
                + " VALUES (?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (PolicyClawbackBeanICOMDB policyClawbackBeanICOMDB : policyClawbackBeanICOMDBs) {
              pstmt.setString(1, policyClawbackBeanICOMDB.getScheduleId());
              pstmt.setBigDecimal(2, newVer);
              pstmt.setString(3, policyClawbackBeanICOMDB.getClawbackId());
              pstmt.setString(4, policyClawbackBeanICOMDB.getAgentCode());
              pstmt.setString(5, policyClawbackBeanICOMDB.getPolicyNo());
              pstmt.setString(6, policyClawbackBeanICOMDB.getRiderNo());
              pstmt.setString(7, policyClawbackBeanICOMDB.getRecalScheduleId());
              pstmt.setBigDecimal(8, policyClawbackBeanICOMDB.getProdTypeId());
              pstmt.setBigDecimal(9, policyClawbackBeanICOMDB.getProdAmount());
              pstmt.setDate(10, policyClawbackBeanICOMDB.getCreateDate() == null ? null
                  : new java.sql.Date(policyClawbackBeanICOMDB.getCreateDate().getTime()));
              pstmt.setBigDecimal(11, policyClawbackBeanICOMDB.getCreateBy());
              pstmt.setDate(12, policyClawbackBeanICOMDB.getUpdateDate() == null ? null
                  : new java.sql.Date(policyClawbackBeanICOMDB.getUpdateDate().getTime()));
              pstmt.setBigDecimal(13, policyClawbackBeanICOMDB.getUpdateBy());
              pstmt.addBatch();
              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      rowUpdate = policyClawbackBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_CLAWBACK_AGENT. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;

  }

  /* PLEASE KEEP METHODS AFTER THIS LINE SORTED IN ALPHABETICAL ORDER */

  @Override
  public long deleteMasterCodeIpg(String codeGroup) throws DataAccessResourceFailureException,
      IllegalStateException, HibernateException {

    String sqldel = "DELETE FROM ipg.ipg_master_code WHERE code_group = :group";
    Session session = null;
    int rowDeleted = 0;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("group", codeGroup);
      rowDeleted = queryDel.executeUpdate();
      logger.info(rowDeleted + " were deleted from master code table.");
    } catch (Exception exGen) {
      rowDeleted = -1;
      logger.error("An error occurred while deleting data from IPG_MASTER_CODE. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return rowDeleted;
  }

  @Override
  public long deleteMasterCodeStaging(String codeGroup) throws DataAccessResourceFailureException,
      IllegalStateException, HibernateException {

    String sqldel = "DELETE FROM icom.icom_master_code WHERE code_group = :group";
    Session session = null;
    int rowDeleted = 0;

    try {
      session = this.getSession();
      Query queryDel = session.createSQLQuery(sqldel);
      queryDel.setParameter("group", codeGroup);
      rowDeleted = queryDel.executeUpdate();
      logger.info(rowDeleted + " were deleted from master code table.");
    } catch (Exception exGen) {
      rowDeleted = -1;
      logger.error("An error occurred while deleting data from IPG_MASTER_CODE. Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return rowDeleted;
  }

  @Override
  public boolean insertBenefitCalNoteStaging(final List<BenefitCalNote> calNoteList)
      throws SQLException, DataAccessResourceFailureException, IllegalStateException {

    boolean result = false;
    Session session = null;

    try {
      session = this.getSession();
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO icom.icom_benefit_calnote (schedule_id, ver,"
                + "    benefit_result_id, calnote_id, agent_code, policy_no, rider_no,"
                + "    recal_schedule_id, cal_note, cal_value, create_by, create_date, update_by,"
                + "    update_date)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (BenefitCalNote calNoteItem : calNoteList) {
              if (i % 1000 == 0) {
                logger.debug("inserting CalNote : " + calNoteItem.toString());
              }
              pstmt.setString(1, calNoteItem.getScheduleId());
              pstmt.setBigDecimal(2, calNoteItem.getVer());
              pstmt.setBigDecimal(3, calNoteItem.getBenefitResultId());
              pstmt.setBigDecimal(4, calNoteItem.getCalNoteId());
              pstmt.setString(5, calNoteItem.getAgentCode());
              pstmt.setString(6, calNoteItem.getPolicyNo());
              pstmt.setString(7, calNoteItem.getRiderNo());
              pstmt.setString(8, calNoteItem.getRecalScheduleId());
              pstmt.setString(9, calNoteItem.getCalNote());
              pstmt.setString(10, calNoteItem.getCalValue());
              pstmt.setBigDecimal(11, calNoteItem.getCreatedBy());
              pstmt.setDate(12, calNoteItem.getCreatedOn() == null ? null
                  : new java.sql.Date(calNoteItem.getCreatedOn().getTime()));
              pstmt.setBigDecimal(13, calNoteItem.getUpdatedBy());
              pstmt.setDate(14, calNoteItem.getUpdatedOn() == null ? null
                  : new java.sql.Date(calNoteItem.getUpdatedOn().getTime()));
              pstmt.addBatch();
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });

      result = true;
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into icom_benefit_calnote. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing
      }
    }
    return result;
  }

  @Override
  public boolean insertListAgentsICOM(final List<AgentsBeanICOMDB> agentsBeanICOMDBs)
      throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    Session session = null;
    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO ICOM.ICOM_PSN_INF (SCHEDULE_ID, AGENT_CODE, NATIONAL_ID,"
                + "    AGENT_TITLE_THA, AGENT_FIRSTNAME_THA, AGENT_LASTNAME_THA, AGENT_TITLE_ENG,"
                + "    AGENT_FIRSTNAME_ENG, AGENT_LASTNAME_ENG, DOB, GENDER, AGENT_STATUS,"
                + "    APPOINTMENT_DATE, CONTRACT_DATE, ADDR_1, ADDR_2, ADDR_3, ADDR_4, ZIP_CODE,"
                + "    HOME_PHONE, OFFICE_PHONE, MOBILE_PHONE, EMAIL, EFFECTIVE_DATE, LICENSE_CODE,"
                + "    START_DATE, EXPIRE_DATE, FIRST_ISSUED_DATE, CREATE_BY, CREATE_DATE,"
                + "    UPDATE_BY, UPDATE_DATE, AGENT_POS_ID, TERMINATE_DATE, TERMINATE_REASON,"
                + "    IS_INDIVIDUAL, UPLINE_CODE, RECRUITER_CODE, WORK_MONTH, SALE_CHANNEL_CODE) "
                + " VALUES (?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?) ";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (AgentsBeanICOMDB agentsBeanICOMDB : agentsBeanICOMDBs) {
              pstmt.setString(1, agentsBeanICOMDB.getScheduleId());
              pstmt.setString(2, agentsBeanICOMDB.getAgentCode());
              pstmt.setString(3, agentsBeanICOMDB.getNationalId());
              pstmt.setString(4, agentsBeanICOMDB.getAgentTitleTha());
              pstmt.setString(5, agentsBeanICOMDB.getAgentFirstnameTha());
              pstmt.setString(6, agentsBeanICOMDB.getAgentLastnameTha());
              pstmt.setString(7, agentsBeanICOMDB.getAgentTitleEng());
              pstmt.setString(8, agentsBeanICOMDB.getAgentFirstnameEng());
              pstmt.setString(9, agentsBeanICOMDB.getAgentLastnameEng());
              pstmt.setDate(10, agentsBeanICOMDB.getDob() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getDob().getTime()));
              pstmt.setString(11, agentsBeanICOMDB.getGender());
              pstmt.setString(12, agentsBeanICOMDB.getAgentStatus());
              pstmt.setDate(13, agentsBeanICOMDB.getAppointmentDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getAppointmentDate().getTime()));
              pstmt.setDate(14, agentsBeanICOMDB.getContractDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getContractDate().getTime()));
              pstmt.setString(15, agentsBeanICOMDB.getAddr1());
              pstmt.setString(16, agentsBeanICOMDB.getAddr2());
              pstmt.setString(17, agentsBeanICOMDB.getAddr3());
              pstmt.setString(18, agentsBeanICOMDB.getAddr4());
              pstmt.setString(19, agentsBeanICOMDB.getZipCode());
              pstmt.setString(20, agentsBeanICOMDB.getHomePhone());
              pstmt.setString(21, agentsBeanICOMDB.getOfficePhone());
              pstmt.setString(22, agentsBeanICOMDB.getMobilePhone());
              pstmt.setString(23, agentsBeanICOMDB.getEmail());
              pstmt.setDate(24, agentsBeanICOMDB.getEffectiveDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getEffectiveDate().getTime()));
              pstmt.setString(25, agentsBeanICOMDB.getLicenseCode());
              pstmt.setDate(26, agentsBeanICOMDB.getStartDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getStartDate().getTime()));
              pstmt.setDate(27, agentsBeanICOMDB.getExpireDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getExpireDate().getTime()));
              pstmt.setDate(28, agentsBeanICOMDB.getFirstIssuedDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getFirstIssuedDate().getTime()));
              pstmt.setBigDecimal(29, agentsBeanICOMDB.getCreateBy());
              pstmt.setDate(30, agentsBeanICOMDB.getCreateDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getCreateDate().getTime()));
              pstmt.setBigDecimal(31, agentsBeanICOMDB.getUpdateBy());
              pstmt.setDate(32, agentsBeanICOMDB.getUpdateDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getUpdateDate().getTime()));
              pstmt.setBigDecimal(33, agentsBeanICOMDB.getAgentPosId());
              pstmt.setDate(34, agentsBeanICOMDB.getTerminateDate() == null ? null
                  : new java.sql.Date(agentsBeanICOMDB.getTerminateDate().getTime()));
              pstmt.setString(35, agentsBeanICOMDB.getTerminateReason());
              pstmt.setString(36, agentsBeanICOMDB.getIsIndividual());
              pstmt.setString(37, agentsBeanICOMDB.getUplineCode());
              pstmt.setString(38, agentsBeanICOMDB.getRecruiterCode());
              pstmt.setBigDecimal(39, agentsBeanICOMDB.getWorkMonth());
              pstmt.setString(40, agentsBeanICOMDB.getSaleChannelCode());
              pstmt.addBatch();

              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });

      rowUpdate = agentsBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error(
          "An error occurred while trying to insert data into ICOM_PSN_INF. " + "Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;
  }

  public long insertListMasterCodeIpg(final List<MasterCodeBean> masterCodeBeans) {

    long rowsInserted = 0;
    Session session = null;

    try {
      session = this.getSession();

      session.doWork(new Work() {
        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO ipg.ipg_master_code (code_group, type_id, code, "
                + " description) VALUES (:codeGroup, :typeId, :codeData, :descData)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (MasterCodeBean masterCode : masterCodeBeans) {
              pstmt.setString(1, masterCode.getCodeGroup());
              pstmt.setBigDecimal(2, masterCode.getTypeId());
              pstmt.setString(3, masterCode.getCode());
              pstmt.setString(4, masterCode.getDescription());
              pstmt.addBatch();
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            try {
              if (pstmt != null) {
                pstmt.close();
              }
            } catch (Exception ex) {
              // Do nothing.
            }
          }
        }
      });
      rowsInserted = masterCodeBeans.size();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_MASTER_CODE. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing
      }
    }
    return rowsInserted;
  }

  public long insertListMasterCodeStaging(final List<MasterCodeBean> masterCodeBeans) {

    long rowsInserted = 0;
    Session session = null;

    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO icom.icom_master_code (code_group, type_id, code, "
                + " description) VALUES (:codeGroup, :typeId, :codeData, :descData)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (MasterCodeBean masterCode : masterCodeBeans) {
              pstmt.setString(1, masterCode.getCodeGroup());
              pstmt.setBigDecimal(2, masterCode.getTypeId());
              pstmt.setString(3, masterCode.getCode());
              pstmt.setString(4, masterCode.getDescription());
              pstmt.addBatch();
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });
      rowsInserted = masterCodeBeans.size();
    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_MASTER_CODE. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }

    return rowsInserted;
  }

  @Override
  public boolean insertListPerformanceICOM(final List<PerformanceBeanICOMDB> performanceBeanICOMDBs,
      final BigDecimal newVer) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    Session session = null;

    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }

      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {

          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO ICOM.ICOM_PSN_PROD (POLICY_NO, VER, AGENT_CODE, AGENT_POS_ID,"
                + "    PROD_TYPE_ID, POLICY_TYPE_ID, PROD_AMOUNT, INPUT_TYPE, SCHEDULE_ID,"
                + "    POLICY_YEAR, RIDER_NO, ACK_DATE, PERFORMANCE_MONTH, PERFORMANCE_YEAR) "
                + " VALUES (?, ?, ?, ?, ?, ?, "
                + "    ?, ?, ?, ?, ?, ?, "
                + "    ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (PerformanceBeanICOMDB performanceBeanICOMDB : performanceBeanICOMDBs) {
              pstmt.setString(1, performanceBeanICOMDB.getPolicyNo());
              pstmt.setBigDecimal(2, newVer);
              pstmt.setString(3, performanceBeanICOMDB.getAgentCode());
              pstmt.setBigDecimal(4, performanceBeanICOMDB.getAgentPosId());
              pstmt.setBigDecimal(5, performanceBeanICOMDB.getProdTypeId());
              pstmt.setBigDecimal(6, performanceBeanICOMDB.getPolicyTypeId());
              pstmt.setBigDecimal(7, performanceBeanICOMDB.getProdAmount());
              pstmt.setString(8, performanceBeanICOMDB.getInputType());
              pstmt.setString(9, performanceBeanICOMDB.getScheduleId());
              pstmt.setInt(10, performanceBeanICOMDB.getPolicyYear());
              pstmt.setString(11, performanceBeanICOMDB.getRiderCode());
              pstmt.setDate(12, performanceBeanICOMDB.getAckDate() == null ? null
                  : new java.sql.Date(performanceBeanICOMDB.getAckDate().getTime()));
              pstmt.setString(13, performanceBeanICOMDB.getPerformanceMonth());
              pstmt.setString(14, performanceBeanICOMDB.getPerformanceYear());
              pstmt.addBatch();

              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });

      rowUpdate = performanceBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error(
          "An error occurred while trying to insert data into ICOM_PSN_PROD. " + "Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;
  }

  @Override
  public long insertListPolicyClawbackIpg(final List<PolicyClawbackBeanICOM> policyClawbackBeans) {

    long rowsInserted = -1;
    Session session = null;
    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          int i = 0;
          try {
            String sql = "INSERT INTO ipg.ipg_schedule_id (schedule_id, ver, clawback_id,"
                + "    agent_code, policy_no, rider_no, recal_schedule_id, prod_type_id, "
                + "    prod_amount, create_by, create_date, update_by, update_date)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            for (PolicyClawbackBeanICOM clawbackBean : policyClawbackBeans) {
              pstmt.setString(1, clawbackBean.getScheduleId());
              pstmt.setBigDecimal(2, clawbackBean.getVer());
              pstmt.setString(3, clawbackBean.getClawbackId());
              pstmt.setString(4, clawbackBean.getAgentCode());
              pstmt.setString(5, clawbackBean.getPolicyNo());
              pstmt.setString(6, clawbackBean.getRiderNo());
              pstmt.setString(7, clawbackBean.getRecalScheduleId());
              pstmt.setBigDecimal(8, clawbackBean.getProdTypeId());
              pstmt.setBigDecimal(9, clawbackBean.getProdAmount());
              pstmt.setBigDecimal(10, clawbackBean.getCreateBy());
              pstmt.setDate(11, clawbackBean.getCreateDate() == null ? null
                  : new java.sql.Date(clawbackBean.getCreateDate().getTime()));
              pstmt.setBigDecimal(12, clawbackBean.getUpdateBy());
              pstmt.setDate(13, clawbackBean.getUpdateDate() == null ? null
                  : new java.sql.Date(clawbackBean.getUpdateDate().getTime()));

              pstmt.addBatch();
              i++;
              if (i % batchSizeStagingToIpg == 0) {
                pstmt.executeBatch();
              }
            }
            pstmt.executeBatch();
          } finally {
            try {
              if (pstmt != null) {
                pstmt.close();
              }
            } catch (Exception exIgnore) {
              // Do Nothing.
            }
          }
        }
      });
      rowsInserted = policyClawbackBeans.size();

    } catch (Exception exGen) {
      logger.error("An error occurred while trying to insert data into ICOM_SCHEDULE_ID. "
          + "Stack trace is\n", exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return rowsInserted;
  }

  @Override
  public boolean insertListTierICOM(final List<TierBeanICOMDB> tierBeanICOMDBs) throws Exception {

    boolean result = false;
    logger.debug("Method starts.");
    int rowUpdate = 0;
    Session session = null;
    try {
      if (this.session == null) {
        session = this.getSession();
      } else {
        session = this.session;
      }
      session.doWork(new Work() {

        @Override
        public void execute(Connection conn) throws SQLException {
          PreparedStatement pstmt = null;
          try {
            String sql = "INSERT INTO icom.icom_psn_tier (schedule_id, agent_code, upline_l1_code)"
                + "    VALUES (?, ?, ?)";
            pstmt = conn.prepareStatement(sql);
            int i = 0;
            for (TierBeanICOMDB tierBeanICOMDB : tierBeanICOMDBs) {
              pstmt.setString(1, tierBeanICOMDB.getScheduleId());
              pstmt.setString(2, tierBeanICOMDB.getAgentCode());
              pstmt.setString(3, tierBeanICOMDB.getUplineL1Code());
              pstmt.addBatch();
              // 10000 : JDBC batch size
              if (i % batchSizeIcomToStaging == 0) {
                pstmt.executeBatch();
              }
              i++;
            }
            pstmt.executeBatch();
          } finally {
            pstmt.close();
          }
        }
      });

      rowUpdate = tierBeanICOMDBs.size();
      if (rowUpdate > 0) {
        result = true;
      }
    } catch (Exception exGen) {
      logger.error(
          "An error occurred while trying to insert data into ICOM_PSN_TIER. " + "Stack trace is\n",
          exGen);
      throw exGen;
    } finally {
      this.releaseSession(session);
    }
    return result;
  }

  /**
   * List agent positions from iCompensation main database.
   * 
   * @return List of master codes with group 'POSITION TYPE'
   * @throws Exception when the database tables cannot be read.
   */
  @Override
  public List<MasterCodeBean> listMasterAgentPositionIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterAgentPositionIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT p.agent_pos_id as type_id, c.nm_short_cd as code,"
          + "     p.agent_pos_name as description"
          + " FROM acs_agent_position INNER JOIN i_tsli_code c ON c.new_cd = p.agent_pos_code";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("POSITION TYPE", rst.getBigDecimal("AGENT_POS_ID"),
            rst.getString("CODE"), rst.getString("DESCRIPTION")));
      }

    } catch (Exception ex) {
      logger.error("Error occurred while querying iCompensation Main Database for Position Codes."
          + " Stack trace is:", ex);
      throw ex;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterAgentPositionIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

  @Override
  public List<MasterCodeBean> listMasterBenefitTypeIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterBenefitTypeIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT benefit_type_id, benefit_type_code, benefit_type_name"
          + " FROM acs_benefit_type";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("BENEFIT TYPE", rst.getBigDecimal("BENEFIT_TYPE_ID"),
            rst.getString("BENEFIT_TYPE_CODE"), rst.getString("BENEFIT_TYPE_NAME")));
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for Benefit Codes."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterBenefitTypeIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

  @Override
  public List<MasterCodeBean> listMasterDepartmentIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterDepartmentIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT dep_id, dep_code, dep_name FROM acs_department";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("DEPARTMENT", rst.getBigDecimal("DEP_ID"),
            rst.getString("DEP_CODE"), rst.getString("DEP_NAME")));
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for Departments."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterDepartmentIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

  @Override
  public List<MasterCodeBean> listMasterPolicyTypeIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterPolicyTypeIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT policy_type_id, policy_type_name, policy_type_desc"
          + " FROM acs_policy_type";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("POLICY TYPE", rst.getBigDecimal("POLICY_TYPE_ID"),
            rst.getString("POLICY_TYPE_NAME"), rst.getString("POLICY_TYPE_DESC")));
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for Policy Types."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterPolicyTypeIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

  @Override
  public List<MasterCodeBean> listMasterProductionTypeIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterProductionTypeIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT prod_type_id, prod_type_code, prod_type_name"
          + " FROM acs_production_type";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("PRODUCTION TYPE", rst.getBigDecimal("PROD_TYPE_ID"),
            rst.getString("PROD_TYPE_CODE"), rst.getString("PROD_TYPE_NAME")));
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for Departments."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterProductionTypeIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

  @Override
  public List<MasterCodeBean> listMasterSalesChannelIcomDb() throws Exception {

    logger.debug("Starts ScheduleDAOImpl listMasterSalesChannelIcomDb");
    List<MasterCodeBean> listResult = new ArrayList<MasterCodeBean>();
    try {
      String strSql = "SELECT distinct sale_channel_code FROM acs_psn_inf"
          + " WHERE sale_channel_code IS NOT NULL";
      pstmt = connectionManager.getDbConnection().prepareStatement(strSql);
      rst = pstmt.executeQuery();

      while (rst.next()) {
        listResult.add(new MasterCodeBean("SALE CHANNEL", rst.getBigDecimal("SALE_CHANNEL_CODE"),
            rst.getString("SALE_CHANNEL_CODE"), rst.getString("SALE_CHANNEL_CODE")));
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for Sales Channel."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterSalesChannelIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
        if (pstmt != null) {
          pstmt.close();
        }
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }
  @Override
  public boolean insertListMasterICOM(final List<MasterBeanICOMDB> masterBeanICOMDBs) throws Exception {
	  
	  boolean result = false;
	  logger.debug("Method starts.");
	  int rowUpdate = 0;
	  
	  try {
		  Session session = null;
		  if (this.session == null) {
			  session = this.getSession();
		  } else {
			  session = this.session;
		  }
		  session.doWork(new Work() {
			  
			  @Override
			  public void execute(Connection conn) throws SQLException {
				  PreparedStatement pstmt = null;
				  try {
					  String sql = "INSERT INTO ICOM.ICOM_MASTER_CODE (CODE_GROUP, TYPE_ID, CODE, DESCRIPTION)"
							  + "    VALUES (?, ?, ?, ?)";
					  pstmt = conn.prepareStatement(sql);
					  int i = 0;
					  for (MasterBeanICOMDB masterBeanICOMDB : masterBeanICOMDBs) {
						  pstmt.setString(1, masterBeanICOMDB.getCodeGroup());
						  pstmt.setBigDecimal(2, masterBeanICOMDB.getTypeId());
						  pstmt.setString(3, masterBeanICOMDB.getCode());
						  pstmt.setString(4, masterBeanICOMDB.getDescription());
						  pstmt.addBatch();
						  // 10000 : JDBC batch size
						  if (i % batchSizeIcomToStaging == 0) {
							  pstmt.executeBatch();
						  }
						  i++;
					  }
					  pstmt.executeBatch();
				  } finally {
					  pstmt.close();
				  }
			  }
		  });
		  
		  rowUpdate = masterBeanICOMDBs.size();
		  if (rowUpdate > 0) {
			  result = true;
		  }
	  } catch (Exception exGen) {
		  logger.error(
				  "An error occurred while trying to insert data into ICOM_MASTER_CODE. " + "Stack trace is\n",
				  exGen);
		  throw exGen;
	  }
	  return result;
  }
  @Override
  public boolean deleteMasterICOM() {
	  logger.debug("Method starts.");
	  
	  boolean result = false;
	  logger.info("Clearing data from ICOM_MASTER_CODE of iCompensation staging DB.");
	  String sqldel = "DELETE FROM ICOM.ICOM_MASTER_CODE";
	  Query queryDel = this.getSession().createSQLQuery(sqldel);
	  result = true;
	  logger.debug("Method ends.");
	  return result;
	  
  }
  @Override
  public boolean insertMaster(MasterBean masterBean) {
	  
	  boolean result = false;
	  logger.debug("Method starts.");
	  int rowUpdate = 0;
	  
	  String sql = "INSERT INTO IPG.IPG_MASTER_CODE (CODE_GROUP, TYPE_ID, CODE, DESCRIPTION) VALUES (:codeGroup, :typeId, :code, :description)";
	  
	  try {
		  Query query = this.getSession().createSQLQuery(sql);
		  query.setParameter("codeGroup", masterBean.getCodeGroup());
		  query.setParameter("typeId", masterBean.getTypeId());
		  query.setParameter("code", masterBean.getCode());
		  query.setParameter("description", masterBean.getDescription());
		  rowUpdate = query.executeUpdate();
		  if (rowUpdate > 0) {
			  result = true;
		  }
	  } catch (HibernateException exHib) {
		  logger.error("An error occurred while inserting data in IPG_MASTER_CODE ");
	  } catch (Exception exGen) {
		  logger.error("A generic exception occurred while insert data in IPG_MASTER_CODE");
	  }
	  
	  logger.debug("Method ends.");
	  
	  return result;
	  
  }
  
  @Override
  public List<MasterBeanICOMDB> listMasterICOMDB() throws Exception {
	  logger.debug("Starts ScheduleDAOImpl listMasterICOMDB");
	  List<MasterBeanICOMDB> listResult = new ArrayList<MasterBeanICOMDB>();
	  try {
		  //TODO pull master data query. suggest to use union.
		  String sql = "SELECT CODE_GROUP,TYPE_ID,CODE,DESCRIPTION FROM ("
				  +"SELECT 'AGENT STATUS' AS CODE_GROUP, "
				  +"       1              AS TYPE_ID, "
				  +"       'A'            AS CODE, "
				  +"       'ACTIVE'       AS DESCRIPTION"
				  +"UNION"
				  +"SELECT 'AGENT STATUS' AS CODE_GROUP, "
				  +"       2              AS TYPE_ID, "
				  +"       'I'            AS CODE, "
				  +"       'INACTIVE'     AS DESCRIPTION"
				  +"UNION"
				  +"SELECT 'BENEFIT TYPE'                   AS CODE_GROUP, "
				  +"       BENEFIT_TYPE_ID                  AS TYPE_ID, "
				  +"       SUBSTR(BENEFIT_TYPE_CODE, 1, 20) AS CODE, "
				  +"       BENEFIT_TYPE_NAME                AS DESCRIPTION "
				  +"FROM   ACS_BENEFIT_TYPE"
				  +"UNION"
				  +"SELECT 'DEPARTMENT'             AS CODE_GROUP, "
				  +"       DEPT_ID                  AS TYPE_ID, "
				  +"       SUBSTR(DEPT_CODE, 1, 20) AS CODE, "
				  +"       DEP_NAME                 AS DESCRIPTION "
				  +"FROM   ACS_DEPARTMENT"
				  +"UNION"
				  +"SELECT 'POLICY TYPE'                        AS CODE_GROUP, "
				  +"       POLICY_TYPE_ID                       AS TYPE_ID, "
				  +"       SUBSTR(POLICY_TYPE_CODE_CODE, 1, 20) AS CODE, "
				  +"       SUBSTR(POLICY_TYPE_DESC, 1, 500)     AS DESCRIPTION "
				  +"FROM   ACS_POLICY_TYPE"
				  +"UNION"
				  +"SELECT 'POSITION'                   AS CODE_GROUP, "
				  +"       POS_ID                       AS TYPE_ID, "
				  +"       SUBSTR(POS_CODE_CODE, 1, 20) AS CODE, "
				  +"       POS_NAME                     AS DESCRIPTION "
				  +"FROM   ACS_POSITION"
				  +"UNION"
				  +"SELECT 'POSITION TYPE' AS CODE_GROUP, "
				  +"       AGENT_POS_ID    AS TYPE_ID, "
				  +"       AGENT_POS_CODE  AS CODE, "
				  +"       AGENT_POS_NAME  AS DESCRIPTION "
				  +"FROM   ACS_AGENT_POSITION"
				  +"UNION"
				  +"SELECT 'PRODUCTION TYPE'             AS CODE_GROUP, "
				  +"       PROD_TYPE_ID                  AS TYPE_ID, "
				  +"       SUBSTR(PROD_TYPE_CODE, 1, 20) AS CODE, "
				  +"       PROD_TYPE_NAME                AS DESCRIPTION "
				  +"FROM   ACS_PRODUCTION_TYPE"
				  +"UNION"
				  +"SELECT 'SALE CHANNEL'        AS CODE_GROUP, "
				  +"       ID                    AS TYPE_ID, "
				  +"       SUBSTR(NEW_CD, 1, 20) AS CODE, "
				  +"       NM_FULL_CD_ENG        AS DESCRIPTION "
				  +"FROM   I_TSLI_CODE"
				  +")";
		  
		  pstmt = connectionManager.getDbConnection().prepareStatement(sql);
		  rst = pstmt.executeQuery();
		  logger.debug("Query execution completed.");
		  int rowCount = 0;
		  while (rst.next()) {
			  rowCount++;
			  MasterBeanICOMDB masterBeanICOMDB = new MasterBeanICOMDB();
			  
			  masterBeanICOMDB.setCodeGroup(rst.getString("CODE_GROUP"));
			  masterBeanICOMDB.setTypeId(rst.getBigDecimal("TYPE_ID"));
			  masterBeanICOMDB.setCode(rst.getString("CODE"));
			  masterBeanICOMDB.setDescription(rst.getString("DESCRIPTION"));
			  
			  listResult.add(masterBeanICOMDB);
		  }
		  logger.info("Schedules listed. " + rowCount + " rows were retrieved.");
	  } catch (Exception ex) {
		  logger.error("An error occurred while retrieving data from master table in iCompensation "
				  + "Database. Stack trace is:\n", ex);
		  throw ex;
	  } finally {
		  logger.debug("Ends ScheduleDAOImpl listMasterICOMDB");
		  try {
			  if (rst != null) {
				  rst.close();
			  }
			  if (pstmt != null) {
				  pstmt.close();
			  }
			  connectionManager.closeConnection();
		  } catch (Exception e) {
			  // Do Nothing.
		  }
	  }
	  return listResult;
  }
  
  @Override
  public List<MasterBeanICOM> listMasterICOM() {
	  logger.debug("Starts ScheduleDAOImpl listMasterICOM");
	  String sql = "SELECT * FROM ICOM.ICOM_MASTER_CODE";
	  Query query = this.getSession().createSQLQuery(sql).addEntity(MasterBeanICOM.class);
	  List<MasterBeanICOM> listResult = query.list();
	  logger.debug("Ends ScheduleDAOImpl listMasterICOM");
	  return listResult;
	  
  }
  
  @Override
  public boolean deleteMaster() {
	  logger.debug("Starts ScheduleDAOImpl deleteMaster() {");
	  boolean result = false;
	  
	  String sqldel = "DELETE FROM IPG.IPG_MASTER_CODE";
	  Query queryDel = this.getSession().createSQLQuery(sqldel);
	  int rowUpdate = queryDel.executeUpdate();
	  
	  if (rowUpdate > 0) {
	  }
	  result = true;
	  logger.debug("ends ScheduleDAOImpl deleteMaster");
	  return result;
  }
}
