package com.motif.playground.dao.impl;

//import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.dao.SelectDataDAO;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

public class SelectDataDAOImpl extends HibernateDaoSupport implements SelectDataDAO {

  @Override
  public List<VersionBean> listScheduleData() {
    return getHibernateTemplate().find("from ScheduleVersionBean");
  }
}
