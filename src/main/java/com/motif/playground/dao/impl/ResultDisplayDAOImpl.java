
package com.motif.playground.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.motif.playground.beans.BenefitRuleBean;
import com.motif.playground.beans.ResultBenefitBean;
import com.motif.playground.dao.ResultDisplayDAO;



public class ResultDisplayDAOImpl extends HibernateDaoSupport implements ResultDisplayDAO {

  protected static final Logger log = Logger.getLogger(ResultDisplayDAOImpl.class);

  @Override
  public List<BenefitRuleBean> getRule() {

    String sql = "SELECT DISTINCT (cal.benefit_code), masc.DESCRIPTION"
        + " FROM ipg.ipg_benefit_cal cal "
        + " INNER JOIN ipg.ipg_master_code masc ON cal.benefit_code = masc.type_id"
        + " WHERE masc.code_group = 'BENEFIT TYPE'";
    Session session = null;
    List<BenefitRuleBean> listRule = new ArrayList<BenefitRuleBean>();

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(BenefitRuleBean.class);
      listRule = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred while listing benefit codes. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    return listRule;
  }


  @Override
  public List<ResultBenefitBean> listBenefitByVersion(String rule,
      String scheduleId, int mainversion, int compareversion, String agentCode) {

    String sql = "SELECT  ROWNUM AS No,R.* FROM (SELECT ROWNUM AS No,M.DESCRIPTION, A.AGENT_CODE, A.AGENT_TITLE_THA, A.AGENT_FIRSTNAME_THA,"
        + "    A.AGENT_LASTNAME_THA, A.AGENT_TITLE_ENG, A.AGENT_FIRSTNAME_ENG,"
        + "    A.AGENT_LASTNAME_ENG, T1.SCHEDULE_ID, T1.BENEFIT_CODE,"
        + "    COALESCE(T1.POLICY_NO,'* Non-Life') POLICY_NO,COALESCE( T1.RIDER_NO,'* Non-Life') RIDER_NO, T1.MAIN_VERSION_AMT, T2.COMPARE_VERSION_AMT"
        + " FROM ("
        + "     SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE, B.POLICY_NO, B.RIDER_NO,"
        + "         B.BENEFIT_AMOUNT MAIN_VERSION_AMT "
        + "     FROM IPG.IPG_BENEFIT_CAL B"
        + "     WHERE B.VER = :mainversion "
        + "   ) T1 INNER JOIN ("
        + "     SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE, B.POLICY_NO, B.RIDER_NO,"
        + "         B.BENEFIT_AMOUNT COMPARE_VERSION_AMT"
        + "     FROM IPG.IPG_BENEFIT_CAL B"
        + "     WHERE B.VER = :compareversion"
        + "   ) T2 ON T1.SCHEDULE_ID = T2.SCHEDULE_ID"
        + "       AND T1.BENEFIT_CODE = T2.BENEFIT_CODE"
        + "       AND COALESCE(T1.RIDER_NO,0) = COALESCE(T2.RIDER_NO,0)"
        + "       AND T1.AGENT_CODE = T2.AGENT_CODE"
        + "   INNER JOIN ipg.ipg_master_code M ON T1.BENEFIT_CODE = M.TYPE_ID "
        + "       AND CODE_GROUP = 'BENEFIT TYPE' "
        + "   INNER JOIN IPG.IPG_PSN_INF A ON T1.AGENT_CODE = A.AGENT_CODE "
        + " WHERE T1.AGENT_CODE LIKE :agentCode "
        + "  AND T1.BENEFIT_CODE = :rule "
        + "  AND T1.SCHEDULE_ID = :scheduleId ) R";
    Session session = null;
    List<ResultBenefitBean> listBenefit = new ArrayList<ResultBenefitBean>();

    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(ResultBenefitBean.class);
      query.setParameter("agentCode", "%" + agentCode + "%");
      query.setParameter("rule", rule);
      query.setParameter("scheduleId", scheduleId);
      query.setParameter("mainversion", mainversion);
      query.setParameter("compareversion", compareversion);
      System.out.println(query.getQueryString());
      listBenefit = query.list();
    } catch (Exception exGen) {
      logger.error("An error occurred whiel getting benefit compare data. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do Nothing.
      }
    }
    return listBenefit;
  }
  
  
  @Override
  public List<ResultBenefitBean> listBenefitByVersionByName(String rule,
		  String scheduleId, int mainversion, int compareversion, String agentName) {
	  
	  String sql = "SELECT ROWNUM AS No,M.DESCRIPTION, A.AGENT_CODE, A.AGENT_TITLE_THA, A.AGENT_FIRSTNAME_THA,"
			  + "    A.AGENT_LASTNAME_THA, A.AGENT_TITLE_ENG, A.AGENT_FIRSTNAME_ENG,"
			  + "    A.AGENT_LASTNAME_ENG, T1.SCHEDULE_ID, T1.AGENT_CODE, T1.BENEFIT_CODE,"
			  + "    COALESCE(T1.POLICY_NO,'* Non-Life') POLICY_NO,COALESCE( T1.RIDER_NO,'* Non-Life') RIDER_NO, T1.MAIN_VERSION_AMT, T2.COMPARE_VERSION_AMT"
			  + " FROM ("
			  + "     SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE, B.POLICY_NO, B.RIDER_NO,"
			  + "         B.BENEFIT_AMOUNT MAIN_VERSION_AMT "
			  + "     FROM IPG.IPG_BENEFIT_CAL B"
			  + "     WHERE B.VER = :mainversion "
			  + "   ) T1 INNER JOIN ("
			  + "     SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE, B.POLICY_NO, B.RIDER_NO,"
			  + "         B.BENEFIT_AMOUNT COMPARE_VERSION_AMT"
			  + "     FROM IPG.IPG_BENEFIT_CAL B"
			  + "     WHERE B.VER = :compareversion"
			  + "   ) T2 ON T1.SCHEDULE_ID = T2.SCHEDULE_ID"
			  + "       AND T1.BENEFIT_CODE = T2.BENEFIT_CODE"
			  + "       AND COALESCE(T1.RIDER_NO,0) = COALESCE(T2.RIDER_NO,0)"
			  + "       AND T1.AGENT_CODE = T2.AGENT_CODE"
			  + "   INNER JOIN ipg.ipg_master_code M ON T1.BENEFIT_CODE = M.TYPE_ID "
			  + "       AND CODE_GROUP = 'BENEFIT TYPE' "
			  + "   INNER JOIN IPG.IPG_PSN_INF A ON T1.AGENT_CODE = A.AGENT_CODE "
			  + " WHERE (A.AGENT_FIRSTNAME_THA LIKE :agentName OR A.AGENT_LASTNAME_THA LIKE :agentName OR A.AGENT_FIRSTNAME_ENG LIKE :agentName OR A.AGENT_LASTNAME_ENG LIKE :agentName) "
			  + "  AND T1.BENEFIT_CODE = :rule "
			  + "  AND T1.SCHEDULE_ID = :scheduleId ";
	  Session session = null;
	  List<ResultBenefitBean> listBenefit = new ArrayList<ResultBenefitBean>();
	  
	  try {
		  session = this.getSession();
		  Query query = session.createSQLQuery(sql).addEntity(ResultBenefitBean.class);
		  query.setParameter("agentName", "%" + agentName + "%");
		  query.setParameter("rule", rule);
		  query.setParameter("scheduleId", scheduleId);
		  query.setParameter("mainversion", mainversion);
		  query.setParameter("compareversion", compareversion);
		  System.out.println(query.getQueryString());
		  listBenefit = query.list();
	  } catch (Exception exGen) {
		  logger.error("An error occurred whiel getting benefit compare data. Stack trace is\n", exGen);
		  throw exGen;
	  } finally {
		  try {
			  this.releaseSession(session);
		  } catch (Exception ex) {
			  // Do Nothing.
		  }
	  }
	  return listBenefit;
  }


  @Override
  public ResultBenefitBean getBenefitDetail(String rule, String scheduleId,
      int mainversion, int compareversion, String agentCode,
      String policyNo, String riderNo) {

    log.debug("Input values: benefit_code=" + rule + ", scheduleId=" + scheduleId + ", version="
        + ", compareversion=" + compareversion + ", agent=" + agentCode + ", policy=" + policyNo
        + ", rider=" + riderNo); // TODO Auto-generated method stub
    String sql = "SELECT ROWNUM AS No,M.DESCRIPTION, A.AGENT_CODE, A.AGENT_TITLE_THA, A.AGENT_FIRSTNAME_THA,"
        + "     A.AGENT_LASTNAME_THA, A.AGENT_TITLE_ENG, A.AGENT_FIRSTNAME_ENG,"
        + "     A.AGENT_LASTNAME_ENG, T1.SCHEDULE_ID, T1.AGENT_CODE, T1.BENEFIT_CODE, T1.POLICY_NO,"
        + "     T1.RIDER_NO,T1.MAIN_VERSION_AMT, T2.COMPARE_VERSION_AMT "
        + " FROM( "
        + "     SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE , B.POLICY_NO, "
        + "         B.RIDER_NO, B.BENEFIT_AMOUNT MAIN_VERSION_AMT "
        + "     FROM IPG.IPG_BENEFIT_CAL B  "
        + "     WHERE B.VER = :mainversion "
        + "   ) T1 INNER JOIN ( "
        + "       SELECT B.SCHEDULE_ID, B.VER, B.AGENT_CODE, B.BENEFIT_CODE, B.POLICY_NO, "
        + "           B.RIDER_NO, B.BENEFIT_AMOUNT COMPARE_VERSION_AMT "
        + "       FROM IPG.IPG_BENEFIT_CAL B  "
        + "       WHERE B.VER = :compareversion "
        + "   ) T2 ON T1.SCHEDULE_ID = T2.SCHEDULE_ID  "
        + "     AND T1.BENEFIT_CODE = T2.BENEFIT_CODE "
        + "     AND COALESCE(T1.RIDER_NO,0) = COALESCE(T2.RIDER_NO,0) "
        + "     AND COALESCE(T1.POLICY_NO,0) = COALESCE(T2.POLICY_NO,0) "
        + "     AND T1.AGENT_CODE = T2.AGENT_CODE "
        + "   INNER JOIN ipg.ipg_master_code M ON T1.BENEFIT_CODE = M.TYPE_ID"
        + "     AND CODE_GROUP = 'BENEFIT TYPE' "
        + "   INNER JOIN IPG.IPG_PSN_INF A ON T1.AGENT_CODE = A.AGENT_CODE "
        + " WHERE T1.AGENT_CODE LIKE :agentCode "
        + "   AND T1.BENEFIT_CODE = :rule "
        + "   AND T1.SCHEDULE_ID = :scheduleId ";
    Session session = null;
    ResultBenefitBean benefit = null;
    
    try {
      session = this.getSession();
      Query query = session.createSQLQuery(sql).addEntity(ResultBenefitBean.class);
      query.setParameter("agentCode", "%" + agentCode + "%");
      query.setParameter("rule", rule);
      query.setParameter("scheduleId", scheduleId);
      query.setParameter("mainversion", mainversion);
      query.setParameter("compareversion", compareversion);
      List<ResultBenefitBean> listBenefit = query.list();
      
      if (listBenefit.size() > 0) {
        benefit = listBenefit.get(0);
      }
    } catch (Exception exGen) {
      logger.error("An error occurred while getting benefits data. Stack trace is\n", exGen);
      throw exGen;
    } finally {
      try {
        this.releaseSession(session);
      } catch (Exception ex) {
        // Do nothing.
      }
    }
    return benefit;
  }

}
