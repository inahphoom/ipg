package com.motif.playground.dao.impl;

import com.motif.playground.beans.BenefitCalNote;
import com.motif.playground.beans.MasterCodeBean;
import com.motif.playground.dao.CompensationAdapter;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import util.ConnectionManager;


public class CompensationAdapterImpl extends HibernateDaoSupport implements CompensationAdapter {

  private static final Logger logger = Logger.getLogger(CompensationAdapterImpl.class);
  private ConnectionManager connectionManager = new ConnectionManager();
  private Session session = null;
  private Transaction tx = null;

  @Override
  public void beginTransaction() {

    session = this.getSession();
    tx = session.beginTransaction();

  }

  @Override
  public void commitTransaction() {

    tx.commit();
    this.releaseSession(session);
    session = null;
    tx = null;

  }

  @Override
  public void rollbackTransaction() {

    tx.rollback();
    session = null;
    tx = null;

  }

  @Override
  public List<BenefitCalNote> listCalNoteIcomDb(String scheduleId, BigDecimal newVer)
      throws ClassNotFoundException, SQLException {

    logger.info("########### method starts ####################");

    long startCalNoteId = 1;
    List<BenefitCalNote> calNoteList = null;

    try {
      calNoteList = listCalNoteIcomItems(scheduleId, newVer, startCalNoteId);
    } catch (Exception exGen) {
      logger.error("Error calling listCalNote. Empty list will be used.");
      calNoteList = new ArrayList<BenefitCalNote>();
    }
    return calNoteList;
  }

  /*
   * public List<BenefitCalNote> listCalNoteIcomDbItems(final String scheduleId, final String
   * tableName, final long StartId, final HashMap<String, String> fieldList) throws
   * ClassNotFoundException, SQLException {
   * 
   * List<BenefitCalNote> listResult = new ArrayList<BenefitCalNote>(); Iterator fieldItem =
   * fieldList.entrySet().iterator(); String selectStatement = ""; String pivotStatement = "";
   * 
   * for (String keyItem : fieldList.keySet()) { selectStatement += selectStatement.length() > 0 ?
   * ", " + keyItem : keyItem; pivotStatement += (pivotStatement.length() > 0 ? ", ": "") + "\"" +
   * fieldList.get(keyItem) + "\""; }
   * 
   * 
   * String sql =
   * "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no," +
   * "     recal_schedule_id, 'QB-AB' AS cal_group, cal_note, cal_value, create_by," +
   * "     create_date, update_by, update_date FROM acs_tsli_detail_qb_ab" +
   * " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\"," +
   * "    \"BONUS_RATE\", \"PERSIS_CREDIT_HOLD\", \"PERSIS_CREDIT_PAY\"," +
   * "    \"PERSIS_CREDIT_RELEASE\", \"SUMAB_HOLD\", \"SUMAB_PAY\", \"SUMAB_RELEASE\"," +
   * "    \"SUMQB_HOLD\", \"SUMQB_PAY\", \"SUMQB_RELEASE\", \"PERSIS_RATE\") )" +
   * " WHERE schedule_id = ?";
   * 
   * return listResult; }
   */

  public List<BenefitCalNote> listCalNoteIcomItems(String scheduleId, final BigDecimal newVer,
      final long startId)
      throws ClassNotFoundException, SQLException {

    logger.info(" ####### method starts. ##########");
    List<BenefitCalNote> listResult = new ArrayList<BenefitCalNote>();
    // ACS_TSLI_QB_AB
    String sql = "SELECT schedule_id, benefit_result_id, agent_id AS agent_code, '' AS policy_no,"
        + "     '' AS rider_no, recal_schedule_id, 'QB-AB' AS cal_group, cal_note, cal_value,"
        + "     create_by, create_date, update_by, update_date FROM acs_tsli_detail_qb_ab"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"BONUS_RATE\", \"PERSIS_CREDIT_HOLD\", \"PERSIS_CREDIT_PAY\","
        + "    \"PERSIS_CREDIT_RELEASE\", \"SUMAB_HOLD\", \"SUMAB_PAY\", \"SUMAB_RELEASE\","
        + "    \"SUMQB_HOLD\", \"SUMQB_PAY\", \"SUMQB_RELEASE\", \"PERSIS_RATE\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // ACS_TSLI_DETAIL_RA
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'RA' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date FROM acs_tsli_detail_ra"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"ACC_AFYP\", \"AFYP\", \"CAL_AMOUNT\","
        + "    \"PAYMENT_RATE\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // ACS_TSLI_DETAIL_NASB
        + " UNION "
        + " SELECT schedule_id, benefit_result_id, psn_info_id, '' AS policy_no, '' AS rider_no,"
        + "     recal_schedule_id, 'NASB' AS cal_group, cal_note, cal_value, create_by,"
        + "     create_date, update_by, update_date FROM acs_tsli_detail_nasb"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"ACC_AFYP\", \"AFYP_PAY_AMOUNT\","
        + "     \"APE_PAY_AMOUNT\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // ACS_TSLI_DETAIL_CB_PB
        + " UNION "
        + " SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "     recal_schedule_id, 'CB-PB' AS cal_group, cal_note, cal_value, create_by,"
        + "     create_date, update_by, update_date FROM acs_tsli_detail_cb_pb"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"CASE_COUNT\", \"TOTAL_AFYP\", \"TOTAL_ARYP2\","
        + "     \"TOTAL_PC\", \"TOTAL_FYC\", \"PERSIS_RATE\", \"BONUS_RATE\","
        + "     \"BONUS_RATE_BEFORE\", \"BONUS_RATE_AFTER\", \"RYC_TRANSFER\", \"RYC\","
        + "     \"TOTAL_RYC\", \"RYC_BEFORE_TRANSFER\", \"RYC_BEFORE\", \"TOTAL_RYC_BEFORE\","
        + "     \"RYC_AFTER_TRANSFER\", \"RYC_AFTER\", \"TOTAL_RYC_AFTER\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // ACS_TSLI_DETAIL_FMA
        + " UNION"
        + " SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no, "
        + "     recal_schedule_id, 'FMA' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "     update_by, update_date FROM acs_tsli_detail_fma"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"FMA_PAID\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // ACS_TSLI_DETAIL_FA_ASA
        + " UNION "
        + " SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'FA-ASA' AS cal_group, cal_note, cal_value, create_by,"
        + "    create_date, update_by, update_date FROM acs_tsli_detail_fa_asa"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"PAID_AMT\", \"PERSIS_RATE\", \"RATE\", \"TOTAL_PC_CATCHUP\", \"AVG_PC_CATCHUP\","
        + "    \"PAID_AMT_CATCHUP\", \"RESULT_CATCHUP\", \"OUTSTANDING_AMOUNT\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 07: ACS_TSLI_DETAIL_MOv
        + " UNION "
        + " SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'MOV' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date FROM acs_tsli_detail_mov"
        + " UNPIVOT (cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "     \"ACTIVE_AGENT\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 08: ACS_TSLI_DETAIL_ODI_MOTHER
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "     recal_schedule_id, 'ODI - MOTHER' AS cal_group, cal_note, cal_value, create_by,"
        + "     create_date, update_by, update_date FROM acs_tsli_detail_odi_mother"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"VALID_AFYP\", \"VALID_ARYP2\", \"VALID_PC\","
        + "     \"VALIDATE_TARGET\", \"VALID_AFYP_DOWNLINE\", \"VALID_ARYP2_DOWNLINE\","
        + "     \"VALID_PC_DOWNLINE\", \"VALID_PC_DOWNLINE_75\", \"CATCHUP_PC_QTD\","
        + "     \"CATCHUP_TARGET\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 09: ACS_TSLI_ODI_CHILD
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "     recal_schedule_id, 'ODI - CHILD' AS cal_group, cal_note, cal_value, create_by,"
        + "     create_date, update_by, update_date"
        + " FROM (SELECT c.schedule_id, m.benefit_result_id, c.agent_id, '' AS policy_no,"
        + "     '' AS rider_no, c.recal_schedule_id, 'ODI - CHILD' AS cal_group, c.create_by,"
        + "     c.create_date, c.update_by, c.update_date, c.total_afyp, c.total_aryp2, c.total_pc,"
        + "     c.validate_target, c.cal_amount, c.paymeny_amount"
        + "     FROM acs_tsli_detail_odi_child c INNER JOIN acs_tsli_detail_odi_mother m"
        + "      ON c.detail_odi_mother_id = m.detail_odi_mother_id)"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "     \"VALIDATE_TARGET\", \"CAL_AMOUNT\", \"PAYMENY_AMOUNT\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 10: ACS_TSLI_DETAIL_BOB
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'BOB' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date"
        + " FROM acs_tsli_detail_bob"
        + " UNPIVOT (cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 11: ACS_TSLI_DETAIL_PMB
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'PMB' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date FROM acs_tsli_detail_pmb"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"SALARY_RATE\", \"PERSIS_RATE\", \"PERSIS_CREDIT\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 12: ACS_TSLI_DETAIL_INCENTIVe
        + " UNION "
        + " SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'Incentive' AS cal_group, cal_note, cal_value, create_by,"
        + "    create_date, update_by, update_date FROM acs_tsli_detail_incentive"
        + " UNPIVOT (cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"GRADER\", \"PAYMENT_RATE\", \"SUM_SOLI_BIZRT\", \"SUM_PRST_BIZRT\","
        + "    \"PERSIS_RATE\", \"PERSIS_MODIFY\", \"TOTAL_AGENT\", \"TOTAL_PRODUCER\","
        + "    \"RETENTION\", \"RATIO\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 13: ACS_TSLI_PRODUCER
        // 14: ACS_TSLI_DETAIL_ALLOWANCE
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    RECAL_SCHEDULE_ID, 'Allowance' AS cal_group, cal_note, cal_value, create_by,"
        + "    create_date, update_by, update_date FROM acs_tsli_detail_allowance"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"PC_TARGET\", \"ACHIEVE_RATE\", \"GRADER\", \"PAYMENT_RATE\", \"ALLOWANCE\") )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 15: ACS_TSLI_DETAIL_OE
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_id, '' AS policy_no, '' AS rider_no,"
        + "    RECAL_SCHEDULE_ID, 'OE' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date FROM acs_tsli_detail_oe"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"NEW_OE_AMT\", \"BASIC_AMT\", \"GRADER\","
        + "    \"BASIC_PAY_AMT\", \"PERSIS_RATE\", \"RATIO\", \"TOTAL_AFYP\", \"TOTAL_ARYP2\","
        + "    \"TOTAL_PC\", \"PC_RATE\", \"PC_AMT\", \"TOTAL_ARYP\", \"ARYP_RATE\", \"ARYP_AMT\","
        + "    \"PRODUCTION_AMT\", \"NUMBER_MM\", \"MM_RATE\", \"MM_AMT\", \"NUMBER_BM\","
        + "    \"BM_RATE\", \"BM_AMT\", \"PRODUCER_CASE\", \"PRODUCER_AMOUNT\", \"GRADER_AMOUNT\","
        + "    \"ORG_AMOUNT\", \"OE_AMOUNT\", \"OE_OUTSTANDING\")  )"
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL"
        // 18: ACS_TSLI_DETAIL_AO
        + " UNION "
        + "SELECT schedule_id, benefit_result_id, agent_code, '' AS policy_no, '' AS rider_no,"
        + "    recal_schedule_id, 'AO' AS cal_group, cal_note, cal_value, create_by, create_date,"
        + "    update_by, update_date"
        + " FROM (SELECT ao.schedule_id, ao.benefit_result_id, ao.recal_schedule_id, ao.total_afyp,"
        + "      ao.total_aryp2, ao.total_pc, ao.total_ryp_year2, ao.total_ryp_year3, ao.rate,"
        + "      ao.cal_ao1, ao.cal_ao2, ao.cal_ao3, ao.create_by, ao.create_date, ao.update_by,"
        + "      ao.update_date, psn.agent_code FROM acs_tsli_detail_ao ao INNER JOIN "
        + "      acs_psn_inf psn ON ao.psn_inf_id = psn.psn_info_id)"
        + " UNPIVOT ( cal_value FOR cal_note IN (\"TOTAL_AFYP\", \"TOTAL_ARYP2\", \"TOTAL_PC\","
        + "    \"TOTAL_RYP_YEAR2\", \"TOTAL_RYP_YEAR3\", \"RATE\", \"CAL_AO1\", \"CAL_AO2\","
        + "    \"CAL_AO3\") )"
        + " WHERE schedule_id = 659 AND benefit_result_id IS NOT NULL"
        // 21: ACS_TSLI_DETAIL_SERP
        + "SELECT schedule_id, benefit_result_id, agent_code, '' AS policy_no, '' AS rider_no," 
        + "    recal_schedule_id, 'SERP' AS cal_group, cal_note, cal_value, create_by, "
        + "    create_date, update_by, update_date" 
        + " FROM (SELECT serp.schedule_id, serp.benefit_result_id, serp.recal_schedule_id, "
        + "      serp.afyp, serp.aryp2, serp.total_afyp_aryp2, serp.total_paid_month,"
        + "      serp.persistency, serp.persistency_credit, serp.create_by, serp.create_date,"
        + "      serp.update_by, serp.update_date, psn.agent_code FROM acs_tsli_detail_serp serp "
        + "      INNER JOIN acs_psn_inf psn ON serp.psn_info_id = psn.psn_info_id)" 
        + " UNPIVOT ( cal_value FOR cal_note IN (\"AFYP\", \"ARYP2\", \"TOTAL_AFYP_ARYP2\","
        + "    \"TOTAL_PAID_MONTH\", \"PERSISTENCY\", \"PERSISTENCY_CREDIT\") )" 
        + " WHERE schedule_id = ? AND benefit_result_id IS NOT NULL;";
    // 13: ACS_TSLI_PRODUCER --> cannot tie to agent
    // 16: ACS_TSLI_DETAIL_TELEPHONE -> need benefit_result_id
    // 17: ACS_TSLI_DETAIL_TELE_OE --> need benefit_Result_id
    // 19: ACS_TSLI_PROMOTION --> need benefit_result_id
    // 20: ACS_TSLI_FRInGE --> needs benefit_result_id


    logger.debug("SQL to be executed: " + sql);
    ResultSet rst = null;

    try (PreparedStatement pstmt = connectionManager.getDbConnection().prepareStatement(sql)) {
      for (int i = 1; i <= 15; i++) {
        pstmt.setLong(i, Long.parseLong(scheduleId));
      }
      rst = pstmt.executeQuery();
      long calNoteId = startId;
      while (rst.next()) {
        // By specification, calnote_id should be unique only in the same benefit_result_id but this
        // is more memory- and processing-intensive than just roll the id around.
        BenefitCalNote cn = new BenefitCalNote(rst.getString("SCHEDULE_ID"), newVer,
            rst.getBigDecimal("BENEFIT_RESULT_ID"), new BigDecimal(calNoteId++),
            rst.getString("AGENT_CODE"), rst.getString("POLICY_NO"),
            rst.getString("RIDER_NO"), rst.getString("RECAL_SCHEDULE_ID"),
            rst.getString("CAL_NOTE"), rst.getString("CAL_VALUE"),
            rst.getBigDecimal("CREATE_BY"), rst.getTimestamp("CREATE_DATE"),
            rst.getBigDecimal("UPDATE_BY"), rst.getTimestamp("UPDATE_DATE"));
        listResult.add(cn);
        if (calNoteId % 10000 == 0) {
          logger.debug("CalNote has " + calNoteId + " members. Latest one is : " + cn.toString());
        }
      }
    } catch (Exception exGen) {
      logger.error("Error occurred while querying iCompensation Main Database for CalNotes."
          + " Stack trace is:", exGen);
      throw exGen;
    } finally {
      logger.debug("Ends ScheduleDAOImpl listMasterProductionTypeIcomDb");
      try {
        if (rst != null) {
          rst.close();
        }
      } catch (Exception ex) {
        // Do nothing.
      }
      try {
        connectionManager.closeConnection();
      } catch (Exception e) {
        // Do Nothing
      }
    }
    return listResult;
  }

}
