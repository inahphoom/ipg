package com.motif.playground.dao;

import com.motif.playground.beans.PolicyClawbackBean;

import java.util.List;

public interface ClawbackDAO {

  List<PolicyClawbackBean> listAgentClawback(String scheduleId, int version, String policyNo,
      String riderNo);
}
