package com.motif.playground.dao;

//import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;

import java.util.List;

public interface SelectDataDAO {

  List<VersionBean> listScheduleData();
}
