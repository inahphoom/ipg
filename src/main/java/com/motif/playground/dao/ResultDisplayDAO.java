package com.motif.playground.dao;

import com.motif.playground.beans.BenefitRuleBean;
import com.motif.playground.beans.ResultBenefitBean;

import java.util.List;

public interface ResultDisplayDAO {

  /**
   * List benefit type code and description for drop-down list in Result Display options page.
   * 
   * @return list of benefits
   */
  List<BenefitRuleBean> getRule();

  /**
   * List differences between version of the benefit results.
   * 
   * @return List of benefits
   */
  List<ResultBenefitBean> listBenefitByVersion(String rule, String scheduleId, int mainversion,
      int compareversion, String agentCode);
  List<ResultBenefitBean> listBenefitByVersionByName(String rule, String scheduleId, int mainversion,
		  int compareversion, String agentName);

  ResultBenefitBean getBenefitDetail(String rule, String scheduleId, int mainversion,
      int compareversion, String agentCode, String policyNo, String riderNo);
}
