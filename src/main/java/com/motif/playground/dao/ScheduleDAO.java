package com.motif.playground.dao;

import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.AgentsBeanICOM;
import com.motif.playground.beans.AgentsBeanICOMDB;
import com.motif.playground.beans.BenefitBean;
import com.motif.playground.beans.BenefitBeanICOM;
import com.motif.playground.beans.BenefitBeanICOMDB;
import com.motif.playground.beans.BenefitCalNote;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.MasterBean;
import com.motif.playground.beans.MasterBeanICOM;
import com.motif.playground.beans.MasterBeanICOMDB;
import com.motif.playground.beans.MasterCodeBean;
import com.motif.playground.beans.PerformanceBean;
import com.motif.playground.beans.PerformanceBeanICOM;
import com.motif.playground.beans.PerformanceBeanICOMDB;
import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.beans.PolicyClawbackBeanICOM;
import com.motif.playground.beans.PolicyClawbackBeanICOMDB;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.ScheduleBeanICOM;
import com.motif.playground.beans.ScheduleBeanICOMDB;
import com.motif.playground.beans.TierBean;
import com.motif.playground.beans.TierBeanICOM;
import com.motif.playground.beans.TierBeanICOMDB;
import com.motif.playground.beans.UserBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.dao.DataAccessResourceFailureException;


public interface ScheduleDAO {

  /**
   * 
   * @return
   */
  public List<ScheduleBeanICOM> listScheduleICOM() throws DataAccessResourceFailureException,
      IllegalStateException, HibernateException;

  /**
   * Select benefits from iCompensation Main Database.
   * 
   * @param scheduleId The schedule to be searched. This is an exact value; no wildcard search.
   * @return List of benefit calculation results records.
   * @throws Exception When the method cannot query data from iCompensation Main Database.
   */
  public List<BenefitBeanICOMDB> listBenefitICOMDB(String scheduleId) throws Exception;

  /**
   * 
   * @param scheduleId
   * @return
   * @throws Exception
   */
  public List<PolicyClawbackBeanICOMDB> listPolicyClawbackICOMDB(String scheduleId)
      throws Exception;

  /**
   * List all active agents from ACS_PSN_INF in iCompensation main database to IPG_PSN_INF in
   * iPlayground. Please check be sure that the missing (inactive) agents will not cause Exception
   * later in the workflow.
   * 
   * @param scheduleId Schedule to be retrieved. For the iCompensation version that doesn't have
   *        agent structure varies by schedule, This method will use provided parameter as return
   *        value.
   * @return List of active agents in specified scheduleId. If agent table does not have scheduleId,
   *         it returns all active agents.
   * @exception Exception When an error occurred during query.
   */
  public List<AgentsBeanICOMDB> listAgentsICOMDB(String scheduleId) throws Exception;

  /**
   * Select data from ACS_PSN_TRANS table in iCompensation database. This data will be inserted into
   * IPG_PSN_PROD in iCompensation staging DB later.
   * 
   * @param scheduleId Schedule whose data to be selected from main database.
   * @return List of data records from iCompenstation Db.
   */
  public List<PerformanceBeanICOMDB> listPerformanceICOMDB(String scheduleId);

  /**
   * List agent code and agent's upline code to build heirarchical structure.
   * 
   * @param scheduleId Schedule of which the data is to be based on.
   * @return List of Agent code along with their ID.
   * @throws Exception
   * @throws ClassNotFoundException
   * @throws SQLException
   */
  public List<TierBeanICOMDB> listTierICOMDB(String scheduleId)
      throws SQLException, ClassNotFoundException, Exception;

  /**
   * List all schedule starting with provided Id. This function should retrieve all schedules with
   * id started by provided numbers.
   * 
   * @param scheduleId search criteria as a wild card (id*)
   * @return List of schedule whose Id started with search criteria.
   * @throws Exception When the query cannot be executed or database cannot be connected.
   */
  public List<ScheduleBeanICOMDB> listScheduleICOMDB(String scheduleId) throws Exception;
  
  public List<MasterBeanICOMDB> listMasterICOMDB() throws Exception;

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<BenefitBeanICOM> listBenefitICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<PolicyClawbackBeanICOM> listPolicyClawbackICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<AgentsBeanICOM> listAgentsICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<PerformanceBeanICOM> listPerformanceICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<TierBeanICOM> listTierICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<ScheduleBeanICOM> listScheduleICOM(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<VersionBeanICOM> listVersionICOM(String scheduleId);
  
  
  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<MasterBeanICOM> listMasterICOM();

  /**
   * 
   * @param scheduleId
   * @return
   */
  public List<TierBean> listTier(String scheduleId);

  /**
   * 
   * @param scheduleId
   * @return
   */
  public ScheduleBeanICOM findScheduleICOM(String scheduleId);

  /**
   * 
   * @return
   */
  public int checkSetSchedule();

  /**
   * 
   * @return
   */
  public CurrentScheduleBean getCurrenSchedule();

  /**
   * 
   * @return
   */
  public boolean deleteBenefit() throws Exception;

  /**
   * 
   * @return
   */
  public boolean deletePolicyClawback() throws Exception;

  /**
   * 
   * @return
   */
  public boolean deleteAgents() throws Exception;

  /**
   * 
   * @return
   */
  public boolean deletePerformance() throws Exception;

  /**
   * 
   * @return
   */
  public boolean deleteTier() throws Exception;

  /**
   * 
   * @return
   */
  public boolean deleteSchedule() throws Exception;

  
  public boolean deleteMaster()throws Exception;


  public boolean deleteVersion()throws Exception;

  public boolean deleteSetSchedule() throws Exception;

  public boolean insertBenefit(BenefitBean benefitBean) throws Exception;

  public boolean insertPolicyClawback(PolicyClawbackBean policyClawbackBean) throws Exception;

  public boolean insertAgents(AgentsBean agentsBean) throws Exception;

  public boolean insertPerformance(PerformanceBean performanceBean) throws Exception;

  public boolean insertTier(TierBean tierBean) throws Exception;

  public boolean insertSchedule(ScheduleBean scheduleBean) throws Exception;


  public boolean insertVersion(VersionBean versionBean)throws Exception;
  
  
  public boolean insertMaster(MasterBean masterBean)throws Exception;

  public boolean insertSetSchedule(String scheduleId, BigDecimal mainVersion, BigDecimal createBy)
      throws Exception;

  /**
   * Get the next available version number of specified scheduleId
   * 
   * @param scheduleId the scheduleId to be checked.
   * @return the number to be used as the next version
   */
  public BigDecimal getNewVer(String scheduleId);

  public boolean deleteBenefitICOM(String scheduleId) throws Exception;

  public boolean deletePolicyClawbackICOM(String scheduleId) throws Exception;

  public boolean deleteAgentsICOM(String scheduleId) throws Exception;

  public boolean deletePerformanceICOM(String scheduleId) throws Exception;

  public boolean deleteTierICOM(String scheduleId) throws Exception;

  public boolean deleteScheduleICOM(String scheduleId) throws Exception;


  public boolean deleteVersionICOM(String scheduleId)throws Exception;
  
  
  public boolean deleteMasterICOM()throws Exception;

  public boolean insertBenefitICOM(BenefitBeanICOM benefitBeanICOM) throws Exception;

  public boolean insertPolicyClawbackICOM(PolicyClawbackBeanICOM policyClawbackBeanICOM)
      throws Exception;

  public boolean insertAgentsICOM(AgentsBeanICOM agentsBeanICOM) throws Exception;

  public boolean insertPerformanceICOM(PerformanceBeanICOM performanceBeanICOM) throws Exception;

  /**
   * Insert data from iCompensation main database into IPG staging database's ICOM_PSN_TIER table.
   * 
   * @param tierBeanICOM the data row to be added
   * @return Boolean <strong>true</strong> if adding was successful or <strong>false</strong> if
   *         not.
   */
  public boolean insertTierICOM(TierBeanICOM tierBeanICOM) throws Exception;

  /**
   * Insert data into ICOM's ICOM_SCHEDULE_ID Table.
   * 
   * @param scheduleBeanICOM The schedule data to be inserted.
   * @return <strong>True</strong> if insertion was a success; <strong>false</strong> otherwise.
   */
  public boolean insertScheduleICOM(ScheduleBeanICOM scheduleBeanICOM) throws Exception;

  public boolean insertVersionICOM(VersionBeanICOM versionBeanICOM) throws Exception;

  /**
   * List users from iCompensation main database.
   * 
   * @return List of user who are in IPG_USER or IPG_ADMIN group (in group description)
   * @throws ClassNotFoundException When hibernate cannot load database driver.
   * @throws SQLException When the connection cannot be made, query contains errors, or the ORM
   *         encountered an error.
   */
  public List<UserBean> listUser() throws ClassNotFoundException, SQLException;

  public boolean deleteUser();

  public boolean insertUser(UserBean userBean);

  public void beginTransaction();

  public void commitTransaction();

  public void rollbackTransaction();

  public boolean insertListScheduleICOM(List<ScheduleBeanICOMDB> scheduleBeanICOMDBs)
      throws Exception;

  public boolean insertListBenefitICOM(List<BenefitBeanICOMDB> benefitBeanICOMDBs,
      BigDecimal newVer) throws Exception;

  public boolean insertListPolicyClawbackICOM(
      List<PolicyClawbackBeanICOMDB> policyClawbackBeanICOMDBs, BigDecimal newVer) throws Exception;

  public boolean insertListAgentsICOM(List<AgentsBeanICOMDB> agentsBeanICOMDBs) throws Exception;

  public boolean insertListPerformanceICOM(List<PerformanceBeanICOMDB> performanceBeanICOMDBs,
      BigDecimal newVer) throws Exception;

  public boolean insertListTierICOM(List<TierBeanICOMDB> tierBeanICOMDBs) throws Exception;

  public boolean insertListMasterICOM(List<MasterBeanICOMDB> masterBeanICOMDBs) throws Exception;

  /**
   * Insert data of selected scheduleId from iCompensation Staging database to IPG tables.
   * 
   * @param agentsBeans List of type AgentsBeanICOM to be inserted into IPG table.
   * @return Boolean <strong>true</strong> if the inserting was successful. <strong>false</strong>
   *         if not.
   * @throws Exception Generic exception. One must check stack trace for detail.
   */
  boolean insertListAgents(List<AgentsBeanICOM> agentsBeans) throws Exception;

  /**
   * Insert data of selected scheduleId from iCompensation Staging database to IPG_BENEFIT_CAL.
   * 
   * @param agentsBeans List of type AgentsBeanICOM to be inserted into IPG table.
   * @return Boolean <strong>true</strong> if the inserting was successful. <strong>false</strong>
   *         if not.
   * @throws Exception Generic exception. One must check stack trace for detail.
   */
  boolean insertListBenefit(List<BenefitBeanICOM> benefitBeanICOMs)
      throws Exception;

  /**
   * Insert data of selected scheduleId from iCom Staging to IPG_PSN_PROD. This method commits data
   * in batch.
   * 
   * @param performanceBeanICOMDBs List of data to be inserted into IPG_PSN_PROD
   * @return
   * @throws Exception
   */
  boolean insertListPerformance(List<PerformanceBeanICOM> performanceBeanICOMs) throws Exception;

  /**
   * This is the batched version of insertPolicyClawback() method. it will receive the list of
   * PolicyClawbackBeanICOM beans and insert them into IPG_CLAWBACK_AGENT.
   * 
   * @param policyClawbackBeans The list of clawback.
   * @return An integer representing number of rows inserted into IPG_CLAWBACK_AGENT. The negative
   *         numbers represent errors.
   */
  int insertListPolicyClawback(List<PolicyClawbackBeanICOM> policyClawbackBeans);


  /**
   * Insert Clawback data into ipg_clawback_agent table.
   * 
   * @param policyClawbackBeans Clawback data.
   * @return Number or rows inserted. Negative number means there were errors running the method.
   */
  long insertListPolicyClawbackIpg(List<PolicyClawbackBeanICOM> policyClawbackBeans);

  /**
   * List agent positions from iCompensation main database.
   * 
   * @return List of master codes with group 'POSITION TYPE'
   * @throws Exception when the database tables cannot be read.
   */
  List<MasterCodeBean> listMasterAgentPositionIcomDb() throws Exception;

  /**
   * Delete data from iPlayground Staging's ICOM_MASTER_CODE table whose group_code is specified by
   * codeGroup parameter.
   * 
   * @param codeGroup Value in CODE_GROUP field of which matching rows will be deleted.
   * @return number or rows deleted. Negative number means error occurred while deleting.
   * @throws DataAccessResourceFailureException When the method cannot connect to database.
   * @throws IllegalStateException When there is a problem regarding session state or transaction.
   * @throws HibernateException For all other database-related errors.
   */
  long deleteMasterCodeStaging(String codeGroup) throws DataAccessResourceFailureException,
      IllegalStateException, HibernateException;

  /**
   * Delete data from iPlayground Staging's ICOM_MASTER_CODE table whose group_code is specified by
   * codeGroup parameter.
   * 
   * @param codeGroup Value in CODE_GROUP field of which matching rows will be deleted.
   * @return number or rows deleted. Negative number means error occurred while deleting.
   * @throws DataAccessResourceFailureException When the method cannot connect to database.
   * @throws IllegalStateException When there is a problem regarding session state or transaction.
   * @throws HibernateException For all other database-related errors.
   */
  long deleteMasterCodeIpg(String codeGroup)
      throws DataAccessResourceFailureException, IllegalStateException, HibernateException;

  /**
   * Insert list of calculation notes into ICOM_BENEFIT_CALNOTE.
   * 
   * @param calNoteList List of benefit calculation notes to be inserted.
   * @return <strong>True</strong> when insertion was successful (even though there are now rows to
   *         inserted); <strong>False</strong> otherwise.
   * @throws SQLException When the query cannot be executed.
   * @throws DataAccessResourceFailureException When database session cannot be created.
   * @throws IllegalStateException When database session cannot be created.
   */
  boolean insertBenefitCalNoteStaging(List<BenefitCalNote> calNoteList)
      throws SQLException, DataAccessResourceFailureException, IllegalStateException;

  /**
   * Retrieve list of benefit types from iCompensation main database.
   * 
   * @return List of MasterCode beans. If there is an error or the table doesn't contain data, this
   *         list should be an empty list, not a null object.
   * @throws Exception When the table cannot be read or connection cannot be made.
   */
  List<MasterCodeBean> listMasterBenefitTypeIcomDb() throws Exception;

  List<MasterCodeBean> listMasterDepartmentIcomDb() throws Exception;

  List<MasterCodeBean> listMasterPolicyTypeIcomDb() throws Exception;

  List<MasterCodeBean> listMasterProductionTypeIcomDb() throws Exception;

  List<MasterCodeBean> listMasterSalesChannelIcomDb() throws Exception;

}
