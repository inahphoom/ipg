package com.motif.playground.common;

import java.io.File;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.PropertyConfigurator;

public class Log4JInitServlet implements ServletContextListener {

	private static final long serialVersionUID = 1L;


	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		System.out.println("Log4JInitServlet is initializing log4j");
		String log4jLocation = sce.getServletContext().getInitParameter("log4j-config-location");
		String osName = System.getProperty("os.name");
		System.out.println("OS : "+osName);
		ServletContext sc = sce.getServletContext();

		if (log4jLocation == null) {
			System.err.println("*** No log4j-properties-location init param, so initializing log4j with BasicConfigurator");
			BasicConfigurator.configure();
		} else {
			String webAppPath = sc.getRealPath("/");
			String log4jProp = webAppPath + log4jLocation;
			File logPropertiesFile = new File(log4jProp);
			if (logPropertiesFile.exists()) {
				System.out.println("Initializing log4j with: " + log4jProp);
				String logFileLocation="";
				if(osName.contains("Windows")){
					logFileLocation = sce.getServletContext().getInitParameter("window-log-path");
					System.out.println("Log file location : "+logFileLocation);
					System.setProperty("log4j.folder", logFileLocation);
				}else{
					logFileLocation = sce.getServletContext().getInitParameter("linux-log-path");
					System.out.println("Log file location : "+logFileLocation);
					System.setProperty("log4j.folder", logFileLocation);
				}
				PropertyConfigurator.configure(log4jProp);
			} else {
				System.err.println("*** " + log4jProp + " file not found, so initializing log4j with BasicConfigurator");
				BasicConfigurator.configure();
			}
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}
}