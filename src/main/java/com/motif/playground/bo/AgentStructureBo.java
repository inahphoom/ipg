package com.motif.playground.bo;

import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.DownlineAgentsBean;
import com.motif.playground.beans.SummaryPerformanceBean;

import java.util.List;

public interface AgentStructureBo {

  public List<AgentsBean> searchAgentById(String agentCode);

  public List<AgentsBean> searchAgentByName(String agentName);

  public AgentsBean getAgentInfo(String agentCode);

  public List<SummaryPerformanceBean> listPerformance(String agentCode, String scheduleId, int ver);

  public List<DownlineAgentsBean> listDownlineAgents(String agentCode, String scheduleId);

  public AgentsBean findUplineAgent(String agentCode);

}
