package com.motif.playground.bo;

import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.dao.VersionDAO;

import java.util.List;

public interface VersionBo {

  public List<VersionBean> listVersion(String scheduleId);

  public List<VersionBeanICOM> listVersionICOM(String scheduleId);

  public void setVersionDAO(VersionDAO versionDAO);

}
