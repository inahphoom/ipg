package com.motif.playground.bo;

import com.motif.playground.beans.PolicyClawbackBean;

import java.util.List;

public interface ClawbackBo {
  List<PolicyClawbackBean> listAgentClawback(String scheduleId, int version, String policyNo,
      String riderNo) throws Exception;
}
