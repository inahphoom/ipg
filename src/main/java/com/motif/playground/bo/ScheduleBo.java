package com.motif.playground.bo;

import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.ScheduleBeanICOM;
import com.motif.playground.beans.ScheduleBeanICOMDB;
import com.motif.playground.beans.TierBean;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

public interface ScheduleBo {

  public List<TierBean> listTier(String scheduleId);

  public List<ScheduleBeanICOM> listScheduleICOM();

  public List<ScheduleBeanICOMDB> listScheduleICOMDB(String scheduleId);

  public ScheduleBeanICOM findScheduleICOM(String scheduleId);

  public boolean setData(String scheduleId, BigDecimal mainVersion, BigDecimal createBy)
      throws Exception;

  /**
   * Import data of specified scheduleId from iCompensation database to iCom Staging database.
   * 
   * @param scheduleId the schedule to be imported.
   * @return version of the imported scheduleId. This number starts from 1.
   * @throws Exception when whatever error happened.
   */
  public int importData(String scheduleId) throws Exception;

  /**
   * Select current active schedule and version from IPG_SET_SCH_VER and IPG_SCHEDULE.
   */
  public CurrentScheduleBean getCurrenSchedule();

  public boolean synchronizeUsers() throws ClassNotFoundException, SQLException;

}
