/**
 * 
 */

package com.motif.playground.bo;

import com.motif.playground.beans.UserBean;

/**
 * @author 10024111
 *
 */
public interface UserBo {

  UserBean login(String userName, String password);

  boolean isInternalUser(String userName);

  String getRole(String userName);

}
