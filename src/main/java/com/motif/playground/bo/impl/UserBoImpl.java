/**
 * 
 */

package com.motif.playground.bo.impl;

import com.motif.playground.beans.UserBean;
import com.motif.playground.bo.UserBo;
import com.motif.playground.dao.UserDAO;

/**
 * @author 10024111
 *
 */
public class UserBoImpl implements UserBo {

  UserDAO userDAO;

  /*
   * (non-Javadoc)
   * 
   * @see com.motif.playground.bo.UserBo#login(java.lang.String, java.lang.String)
   */
  @Override
  public UserBean login(String userName, String password) {
    // TODO Auto-generated method stub
    return this.userDAO.login(userName, password);
  }

  @Override
  public boolean isInternalUser(String userName) {
    return this.userDAO.isInternalUser(userName);
  }

  @Override
  public String getRole(String userName) {
    return this.userDAO.getRole(userName);
  }

  public void setUserDAO(UserDAO userDAO) {
    this.userDAO = userDAO;
  }

}
