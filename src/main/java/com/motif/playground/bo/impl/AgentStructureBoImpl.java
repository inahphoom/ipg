package com.motif.playground.bo.impl;

import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.DownlineAgentsBean;
import com.motif.playground.beans.SummaryPerformanceBean;
import com.motif.playground.bo.AgentStructureBo;
import com.motif.playground.dao.AgentStructureDAO;

import java.util.List;

public class AgentStructureBoImpl implements AgentStructureBo {

  AgentStructureDAO agentStructureDAO;

  @Override
  public List<AgentsBean> searchAgentById(String agentCode) {
    return agentStructureDAO.searchAgentById(agentCode);
  }

  @Override
  public List<AgentsBean> searchAgentByName(String agentName) {
    return agentStructureDAO.searchAgentByName(agentName);
  }

  public void setAgentStructureDAO(AgentStructureDAO agentStructureDAO) {
    this.agentStructureDAO = agentStructureDAO;
  }

  @Override
  public AgentsBean getAgentInfo(String agentCode) {
    return agentStructureDAO.getAgentInfo(agentCode);
  }

  @Override
  public List<SummaryPerformanceBean> listPerformance(String agentCode, String scheduleId,
      int ver) {
    return agentStructureDAO.listPerformance(agentCode, scheduleId, ver);
  }

  @Override
  public List<DownlineAgentsBean> listDownlineAgents(String agentCode, String scheduleId) {
    return agentStructureDAO.listDownlineAgents(agentCode, scheduleId);
  }

  @Override
  public AgentsBean findUplineAgent(String agentCode) {
    return agentStructureDAO.findUplineAgent(agentCode);
  }

}
