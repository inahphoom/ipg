package com.motif.playground.bo.impl;

import com.motif.playground.beans.BenefitRuleBean;
import com.motif.playground.beans.ResultBenefitBean;
import com.motif.playground.bo.ResultDisplayBo;
import com.motif.playground.dao.ResultDisplayDAO;

import java.util.List;

public class ResultDisplayBoImpl implements ResultDisplayBo {

  ResultDisplayDAO resultDisplayDAO;

  @Override
  public List<BenefitRuleBean> getRule() {
    // TODO Auto-generated method stub
    return resultDisplayDAO.getRule();
  }

  public void setResultDisplayDAO(ResultDisplayDAO resultDisplayDAO) {
    this.resultDisplayDAO = resultDisplayDAO;
  }

  @Override
  public List<ResultBenefitBean> listBenefitByVersion(String rule,
      String scheduleId, int mainversion, int compareversion,
      String agentCode) {
    // TODO Auto-generated method stub
    return resultDisplayDAO.listBenefitByVersion(rule, scheduleId, mainversion, compareversion,
        agentCode);
  }
  
  @Override
  public List<ResultBenefitBean> listBenefitByVersionByName(String rule, String scheduleId, int mainversion, int compareversion, String agentName) {
	  return resultDisplayDAO.listBenefitByVersionByName(rule, scheduleId, mainversion, compareversion, agentName);
  }

  @Override
  public ResultBenefitBean getBenefitDetail(String rule, String scheduleId,
      int mainversion, int compareversion, String agentCode,
      String policyNo, String riderNo) {
    // TODO Auto-generated method stub
    return resultDisplayDAO.getBenefitDetail(rule, scheduleId, mainversion, compareversion,
        agentCode, policyNo, riderNo);
  }


}
