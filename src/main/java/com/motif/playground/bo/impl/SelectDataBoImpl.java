package com.motif.playground.bo.impl;

import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.bo.SelectDataBo;
import com.motif.playground.dao.SelectDataDAO;

import java.util.List;

public class SelectDataBoImpl implements SelectDataBo {

  SelectDataDAO selectDataDAO;

  @Override
  public List<VersionBean> listSchedule() {
    // TODO Auto-generated method stub
    return selectDataDAO.listScheduleData();
  }

  public void setSelectDataDAO(SelectDataDAO selectDataDAO) {
    this.selectDataDAO = selectDataDAO;
  }


}
