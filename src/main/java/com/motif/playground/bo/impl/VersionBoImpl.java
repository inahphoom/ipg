package com.motif.playground.bo.impl;

import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.bo.VersionBo;
import com.motif.playground.dao.VersionDAO;

import java.util.List;

public class VersionBoImpl implements VersionBo {

  VersionDAO versionDAO;

  @Override
  public List<VersionBean> listVersion(String scheduleId) {
    return versionDAO.listVersion(scheduleId);
  }

  @Override
  public List<VersionBeanICOM> listVersionICOM(String scheduleId) {
    return versionDAO.listVersionICOM(scheduleId);
  }

  @Override
  public void setVersionDAO(VersionDAO versionDAO) {
    this.versionDAO = versionDAO;
  }

}
