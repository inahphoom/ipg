package com.motif.playground.bo.impl;

import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.bo.ClawbackBo;
import com.motif.playground.dao.ClawbackDAO;

import java.util.List;

public class ClawbackBoImpl implements ClawbackBo {

  ClawbackDAO clawbackDAO;

  @Override
  public List<PolicyClawbackBean> listAgentClawback(String scheduleId,
      int version, String policyNo, String riderNo) {
    // TODO Auto-generated method stub
    return clawbackDAO.listAgentClawback(scheduleId, version, policyNo, riderNo);
  }

  public void setClawbackDAO(ClawbackDAO clawbackDAO) {
    this.clawbackDAO = clawbackDAO;
  }

}
