package com.motif.playground.bo.impl;

// import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.AgentsBeanICOM;
import com.motif.playground.beans.AgentsBeanICOMDB;
// import com.motif.playground.beans.BenefitBean;
import com.motif.playground.beans.BenefitBeanICOM;
import com.motif.playground.beans.BenefitBeanICOMDB;
import com.motif.playground.beans.BenefitCalNote;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.MasterBean;
import com.motif.playground.beans.MasterBeanICOM;
import com.motif.playground.beans.MasterBeanICOMDB;
// import com.motif.playground.beans.PerformanceBean;
import com.motif.playground.beans.PerformanceBeanICOM;
import com.motif.playground.beans.PerformanceBeanICOMDB;
import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.beans.PolicyClawbackBeanICOM;
import com.motif.playground.beans.PolicyClawbackBeanICOMDB;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.ScheduleBeanICOM;
import com.motif.playground.beans.ScheduleBeanICOMDB;
import com.motif.playground.beans.TierBean;
import com.motif.playground.beans.TierBeanICOM;
import com.motif.playground.beans.TierBeanICOMDB;
import com.motif.playground.beans.UserBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.bo.ScheduleBo;
import com.motif.playground.dao.CompensationAdapter;
import com.motif.playground.dao.ScheduleDAO;
import com.motiftech.common.config.ConfigManager;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;


public class ScheduleBoImpl implements ScheduleBo {

  ScheduleDAO scheduleDAO;
  CompensationAdapter compensationAdapter;
  Logger logger = Logger.getLogger(ScheduleBoImpl.class);

  @Override
  public List<TierBean> listTier(String scheduleId) {
    return scheduleDAO.listTier(scheduleId);
  }

  @Override
  public List<ScheduleBeanICOM> listScheduleICOM() {
    return scheduleDAO.listScheduleICOM();
  }

  @Override
  public List<ScheduleBeanICOMDB> listScheduleICOMDB(String scheduleId) {
    try {
      return scheduleDAO.listScheduleICOMDB(scheduleId);
    } catch (Exception e) {
      logger.error("An error occurred while listing iCompensation data. Please see previous stack "
          + "trace for detail.");
      return new ArrayList<ScheduleBeanICOMDB>();
    }
  }

  @Override
  public ScheduleBeanICOM findScheduleICOM(String scheduleId) {
    return scheduleDAO.findScheduleICOM(scheduleId);
  }

  @Override
  public boolean setData(String scheduleId, BigDecimal mainVersion, BigDecimal createBy)
      throws Exception {

    scheduleDAO.deleteBenefit();
    scheduleDAO.deletePolicyClawback();
    scheduleDAO.deleteAgents();
    scheduleDAO.deletePerformance();
    scheduleDAO.deleteTier();
    scheduleDAO.deleteSetSchedule();
    scheduleDAO.deleteVersion();
    scheduleDAO.deleteSchedule();
    scheduleDAO.deleteMaster();

    List<BenefitBeanICOM> benefitBeanICOMs = scheduleDAO.listBenefitICOM(scheduleId);
    List<PolicyClawbackBeanICOM> policyClawbackBeanICOMs =
        scheduleDAO.listPolicyClawbackICOM(scheduleId);
    List<AgentsBeanICOM> agentsBeanICOMs = scheduleDAO.listAgentsICOM(scheduleId);
    List<TierBeanICOM> tierBeanICOMs = scheduleDAO.listTierICOM(scheduleId);
    List<PerformanceBeanICOM> performanceBeanICOMs = scheduleDAO.listPerformanceICOM(scheduleId);
    List<ScheduleBeanICOM> scheduleBeanICOMs = scheduleDAO.listScheduleICOM(scheduleId);
    List<VersionBeanICOM> versionBeanICOMs = scheduleDAO.listVersionICOM(scheduleId);
    List<MasterBeanICOM> masterBeanICOMs = scheduleDAO.listMasterICOM();

    logger.info("Insert data from ICOM Staging Benefits");
    try {
      scheduleDAO.insertListBenefit(benefitBeanICOMs);
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data in IPG_Benefit_CAL. Stack trace is\n",
          exGen);
    }


    logger.info("Insert data from ICOM Staging ICOM_BENEFIT_CAL");
    for (PolicyClawbackBeanICOM policyClawbackBeanICOM : policyClawbackBeanICOMs) {

      PolicyClawbackBean policyClawbackBean = new PolicyClawbackBean();

      policyClawbackBean.setScheduleId(policyClawbackBeanICOM.getScheduleId());
      policyClawbackBean.setVer(policyClawbackBeanICOM.getVer());
      policyClawbackBean.setClawbackId(policyClawbackBeanICOM.getClawbackId());
      policyClawbackBean.setAgentCode(policyClawbackBeanICOM.getAgentCode());
      policyClawbackBean.setPolicyNo(policyClawbackBeanICOM.getPolicyNo());
      policyClawbackBean.setRiderNo(policyClawbackBeanICOM.getRiderNo());
      policyClawbackBean.setRecalScheduleId(policyClawbackBeanICOM.getRecalScheduleId());
      policyClawbackBean.setProdTypeId(policyClawbackBeanICOM.getProdTypeId());
      policyClawbackBean.setProdAmount(policyClawbackBeanICOM.getProdAmount());
      policyClawbackBean.setCreateDate(policyClawbackBeanICOM.getCreateDate());
      policyClawbackBean.setCreateBy(policyClawbackBeanICOM.getCreateBy());
      policyClawbackBean.setUpdateDate(policyClawbackBeanICOM.getUpdateDate());
      policyClawbackBean.setUpdateBy(policyClawbackBeanICOM.getUpdateBy());

      scheduleDAO.insertPolicyClawback(policyClawbackBean);
    }

    logger.info("Insert data from ICOM Staging ICOM_PSN_INF to IPG_PSN_INF");

    try {
      scheduleDAO.insertListAgents(agentsBeanICOMs);
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting IPG_PSN_INF.", exGen);
      System.out.println("An error occurred while inserting IPG_PSN_INF.");
      exGen.printStackTrace();
    }
    logger.info("Insert data into ICOM Staging Performance table");
    try {
      scheduleDAO.insertListPerformance(performanceBeanICOMs);
    } catch (Exception exGen) {
      logger.error("An error occurred while inserting data into IPG_PSN_PROD. Stack trace is\n",
          exGen);
      System.out.println("An error occurred while inserting data into IPG_PSN_PROD. "
          + "Stack trace is\n");
      exGen.printStackTrace();
    }


    logger.info("Insert data into ICOM Staging ICOM_PSN_TIER");
    for (TierBeanICOM tierBeanICOM : tierBeanICOMs) {
      TierBean tierBean = new TierBean();
      tierBean.setScheduleId(tierBeanICOM.getScheduleId());
      tierBean.setAgentCode(tierBeanICOM.getAgentCode());
      tierBean.setUplineL1Code(tierBeanICOM.getUplineL1Code());
      scheduleDAO.insertTier(tierBean);
    }

    logger.info("Insert data into IPG.IPG_SCHEDULE_ID");
    for (ScheduleBeanICOM scheduleBeanICOM : scheduleBeanICOMs) {

      ScheduleBean scheduleBean = new ScheduleBean();
      scheduleBean.setScheduleId(scheduleBeanICOM.getScheduleId());
      scheduleBean.setPeriodFrom(scheduleBeanICOM.getPeriodFrom());
      scheduleBean.setPeriodTo(scheduleBeanICOM.getPeriodTo());
      scheduleBean.setPaymentDate(scheduleBeanICOM.getPaymentDate());
      scheduleBean.setCycleName(scheduleBeanICOM.getCycleName());
      scheduleBean.setCreateBy(scheduleBeanICOM.getCreateBy());
      scheduleBean.setCreateDate(scheduleBeanICOM.getCreateDate());
      scheduleBean.setUpdateBy(scheduleBeanICOM.getUpdateBy());
      scheduleBean.setUpdateDate(scheduleBeanICOM.getUpdateDate());
      scheduleBean.setScheduleDesc(scheduleBeanICOM.getScheduleDesc());
      scheduleBean.setIsPaid(scheduleBeanICOM.getIsPaid());
      scheduleBean.setIsDeleted(scheduleBeanICOM.getIsDeleted());
      scheduleBean.setPeriodYear(scheduleBeanICOM.getPeriodYear());
      scheduleBean.setPeriodMonth(scheduleBeanICOM.getPeriodMonth());
      scheduleDAO.insertSchedule(scheduleBean);
    }

    logger.info("Insert data into IPG.IPG_SCHEDULE_VER.");
    for (VersionBeanICOM versionBeanICOM : versionBeanICOMs) {
      VersionBean versionBean = new VersionBean();
      versionBean.setScheduleId(versionBeanICOM.getScheduleId());
      versionBean.setVer(versionBeanICOM.getVer());
      versionBean.setCreateBy(versionBeanICOM.getCreateBy());
      versionBean.setCreateDate(versionBeanICOM.getCreateDate());
      versionBean.setIsDelete(versionBeanICOM.getIsDelete());
      scheduleDAO.insertVersion(versionBean);
    }
    
    logger.info("Insert data into IPG.IPG_MASTER_CODE.");
    for (MasterBeanICOM masterBeanICOM : masterBeanICOMs) {
    	MasterBean masterBean = new MasterBean();
    	masterBean.setCodeGroup(masterBeanICOM.getCodeGroup());
    	masterBean.setTypeId(masterBeanICOM.getTypeId());
    	masterBean.setCode(masterBeanICOM.getCode());
    	masterBean.setDescription(masterBeanICOM.getDescription());
    	scheduleDAO.insertMaster(masterBean);
    }
    
    logger.info("Insert data into IPG.IPG_SET_SCH_VER.");
    scheduleDAO.insertSetSchedule(scheduleId, mainVersion, createBy);
    return true;
  }

  /**
   * Import data of specified scheduleId from iCompensation database to iCom Staging database.
   * 
   * @param scheduleId the schedule to be imported.
   * @return version of the imported scheduleId. This number starts from 1.
   * @throws Exception when whatever error happened.
   */
  @Override
  public int importData(String scheduleId) throws Exception {

    BigDecimal newVer = new BigDecimal(0);
    try {
      scheduleDAO.deleteAgentsICOM(scheduleId);
      scheduleDAO.deleteTierICOM(scheduleId);
      scheduleDAO.deleteMasterICOM();
      // TODO: Don't delete, try update
      // scheduleDAO.deleteScheduleICOM(scheduleId);

      List<BenefitBeanICOMDB> benefitBeanICOMDBs;
      try {
        benefitBeanICOMDBs = scheduleDAO.listBenefitICOMDB(scheduleId);
      } catch (Exception e) {
        logger.warn("Cannot list data from iCompensation. Empty list will be used.");
        benefitBeanICOMDBs = new ArrayList<BenefitBeanICOMDB>();
      }

      List<PolicyClawbackBeanICOMDB> policyClawbackBeanICOMDBs;
      try {
        policyClawbackBeanICOMDBs = scheduleDAO.listPolicyClawbackICOMDB(scheduleId);
      } catch (Exception e1) {
        logger.warn("Cannot list data from iCompensation. Empty list will be used.");
        policyClawbackBeanICOMDBs = new ArrayList<PolicyClawbackBeanICOMDB>();
      }

      List<AgentsBeanICOMDB> agentsBeanICOMDBs;
      try {
        agentsBeanICOMDBs = scheduleDAO.listAgentsICOMDB(scheduleId);
      } catch (Exception e) {
        logger.warn("Cannot list data from iCompensation. Empty list will be used.");
        agentsBeanICOMDBs = new ArrayList<AgentsBeanICOMDB>();
      }

      List<PerformanceBeanICOMDB> performanceBeanICOMDBs =
          scheduleDAO.listPerformanceICOMDB(scheduleId);
      List<TierBeanICOMDB> tierBeanICOMDBs = scheduleDAO.listTierICOMDB(scheduleId);
      List<ScheduleBeanICOMDB> scheduleBeanICOMDBs;
      try {
        scheduleBeanICOMDBs = scheduleDAO.listScheduleICOMDB(scheduleId);
      } catch (Exception e) {
        logger.warn("Cannot list data from iCompensation. Empty list will be used.");
        scheduleBeanICOMDBs = new ArrayList<ScheduleBeanICOMDB>();
      }
      
      List<MasterBeanICOMDB> masterBeanICOMDBs;
      try {
    	  masterBeanICOMDBs = scheduleDAO.listMasterICOMDB();
      } catch (Exception e) {
    	  logger.warn("Cannot list data from iCompensation. Empty list will be used.");
    	  masterBeanICOMDBs = new ArrayList<MasterBeanICOMDB>();
      }

      // Set the version of import data
      newVer = scheduleDAO.getNewVer(scheduleId);
      if (ConfigManager.printLogViaSystemOut()) {
        System.out.println("New version will  be: " + newVer.toString());
      }
      VersionBeanICOM versionBeanICOM = new VersionBeanICOM();
      versionBeanICOM.setScheduleId(scheduleId);
      versionBeanICOM.setVer(newVer);
      versionBeanICOM.setCreateBy(new BigDecimal(0));
      versionBeanICOM.setCreateDate(new Date());
      versionBeanICOM.setIsDelete("N");

      // ===== Finished gathering data. Start the import =====
      boolean insertResult = false;
      long rowsInserted = 0;
      long rowsTotal = 0;

      logger.info("Begin Import.");

      List<ScheduleBeanICOM> scheduleList = scheduleDAO.listScheduleICOM(scheduleId);
      if (scheduleList.size() > 0) {
        // TODO: If the schedule exists, it should be updated.
      } else {
        insertResult = scheduleDAO.insertListScheduleICOM(scheduleBeanICOMDBs);
        rowsTotal += scheduleBeanICOMDBs.size();
        if (insertResult) {
          logger.info(scheduleBeanICOMDBs.size() + " of Schedule data have been inserted.");
          rowsInserted += scheduleBeanICOMDBs.size();
        } else {
          logger.error("Cannot insert schedule. Please see previous stacktrace for detail.");
        }
      }

      insertResult = scheduleDAO.insertVersionICOM(versionBeanICOM);
      rowsTotal += 1;
      if (insertResult) {
        logger.info(1 + " of Version data have been inserted.");
        rowsInserted += 1;
      } else {
        logger.error("Cannot insert schedule vesion. Please see previous stacktrace for detail.");
      }

      insertResult = scheduleDAO.insertListBenefitICOM(benefitBeanICOMDBs, newVer);
      rowsTotal += benefitBeanICOMDBs.size();
      if (insertResult) {
        logger.info(benefitBeanICOMDBs.size() + " of Benefit data have been inserted.");
        rowsInserted += benefitBeanICOMDBs.size();
      } else {
        logger.error("Cannot insert schedule vesion. Please see previous stacktrace for detail.");
      }

      insertResult = scheduleDAO.insertListPolicyClawbackICOM(policyClawbackBeanICOMDBs, newVer);
      rowsTotal += policyClawbackBeanICOMDBs.size();
      if (insertResult) {
        logger.info(policyClawbackBeanICOMDBs.size()
            + " of PolicyClawback data have been inserted.");
        rowsInserted += policyClawbackBeanICOMDBs.size();
      } else {
        logger.error("Cannot insert PolicyClawback. Please see previous stacktrace for detail.");
      }

      insertResult = scheduleDAO.insertListAgentsICOM(agentsBeanICOMDBs);
      rowsTotal += agentsBeanICOMDBs.size();
      if (insertResult) {
        logger.info(agentsBeanICOMDBs.size() + " of Agents data have been inserted.");
        rowsInserted += agentsBeanICOMDBs.size();
      } else {
        logger.error("Cannot insert Agents data. Please see previous stacktrace for detail.");
      }

      insertResult = scheduleDAO.insertListPerformanceICOM(performanceBeanICOMDBs, newVer);
      rowsTotal += performanceBeanICOMDBs.size();
      if (insertResult) {
        logger.info(performanceBeanICOMDBs.size() + " of Performance data have been inserted.");
        rowsInserted += performanceBeanICOMDBs.size();
      } else {
        logger.error("Cannot insert Performance data. Please see previous stacktrace for detail.");
      }

      insertResult = scheduleDAO.insertListTierICOM(tierBeanICOMDBs);
      rowsTotal += tierBeanICOMDBs.size();
      if (insertResult) {
        logger.info(tierBeanICOMDBs.size() + " of Tier data have been inserted.");
        rowsInserted += tierBeanICOMDBs.size();
      } else {
        logger.error("Cannot insert Tier data. Please see previous stacktrace for detail.");
      }

      
      // Calculation Notes
      final List<BenefitCalNote> calNoteList = compensationAdapter.listCalNoteIcomDb(scheduleId, 
          newVer);
      insertResult = scheduleDAO.insertBenefitCalNoteStaging(calNoteList);
      rowsTotal += calNoteList.size();
      if (insertResult) {
        logger.info(calNoteList.size() + " rows of calculation notes have been inserted.");
      } else {
        logger.error("Cannot insert calculation notes. Please see previous stacktrace of detail.");
        rowsInserted += calNoteList.size();
      }
      
      insertResult = scheduleDAO.insertListMasterICOM(masterBeanICOMDBs);
      rowsTotal += masterBeanICOMDBs.size();
      if (insertResult) {
        logger.info(masterBeanICOMDBs.size() + " of Master data have been inserted.");
      } else {
        logger.error("Cannot insert schedule vesion. Please see previous stacktrace for detail.");
        rowsInserted += masterBeanICOMDBs.size();
      }

      logger.info(rowsInserted + " of " + rowsTotal + " rows have been inserted.");

      logger.info("Starting Master Data update.");


      logger.info("Ending Master Data update.");

      logger.info("End Import for schedule " + scheduleId + " version " + newVer.toString());

    } catch (Exception e) {
      // scheduleDAO.rollbackTransaction();
      throw e;
    }
    return newVer.intValue();
  }

  @Override
  public boolean synchronizeUsers() throws ClassNotFoundException, SQLException {
    boolean result = true;
    scheduleDAO.deleteUser();
    List<UserBean> userBeans = scheduleDAO.listUser();
    for (UserBean userBean : userBeans) {
      logger.debug("Adding user: " + userBean.toString());
      result = result & scheduleDAO.insertUser(userBean);
    }
    return result;
  }

  @Override
  public CurrentScheduleBean getCurrenSchedule() {
    return scheduleDAO.getCurrenSchedule();
  }

  public void setScheduleDAO(ScheduleDAO scheduleDAO) {
    this.scheduleDAO = scheduleDAO;
  }

  public void setCompensationAdapter (CompensationAdapter compensationAdapter) {
    this.compensationAdapter = compensationAdapter;
  }
  
}
