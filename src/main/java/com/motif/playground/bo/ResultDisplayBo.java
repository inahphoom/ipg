package com.motif.playground.bo;

import com.motif.playground.beans.BenefitRuleBean;
import com.motif.playground.beans.ResultBenefitBean;

import java.util.List;

public interface ResultDisplayBo {
  List<BenefitRuleBean> getRule();

  List<ResultBenefitBean> listBenefitByVersion(String rule, String scheduleId, int mainversion,
      int compareversion, String agentCode);
  
  List<ResultBenefitBean> listBenefitByVersionByName(String rule, String scheduleId, int mainversion,
		  int compareversion, String agentName);

  ResultBenefitBean getBenefitDetail(String rule, String scheduleId, int mainversion,
      int compareversion, String agentCode, String policyNo, String riderNo);
}
