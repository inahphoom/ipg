package com.motif.playground.bo;

// import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;

import java.util.List;

public interface SelectDataBo {

  List<VersionBean> listSchedule();
}
