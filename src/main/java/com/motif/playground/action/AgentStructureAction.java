package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;
import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.ClawbackBean;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.DownlineAgentsBean;
import com.motif.playground.beans.ResultBenefitBean;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.SummaryPerformanceBean;
import com.motif.playground.beans.TierBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.bo.AgentStructureBo;
import com.motif.playground.bo.ResultDisplayBo;
import com.motif.playground.bo.ScheduleBo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AgentStructureAction extends BaseAction {


  private static final long serialVersionUID = -8208484073665664924L;
  public ScheduleBean currentSchedule;
  public VersionBean scheduleVersion;
  public Map<String, Object> resultMap;
  public List<Map<String, String>> listMapTier;
  public CurrentScheduleBean currentSche;
  public AgentStructureBo agentStructureBo;
  public ScheduleBo scheduleBo;
  public ResultDisplayBo resultDisplayBo;

  public String displayStructure() throws Exception {
    try {
      log.debug("Start - displayStructure()");
      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);
      log.debug("End - displayStructure()");
      return "displayStructure";
    } catch (Exception ex) {
      log.error("An error occurred. Stack trace is:", ex);
      throw ex;
    }
  }

  public String downline() throws Exception {

    try {
      log.debug("Start - downline()");

      String page = request.getParameter("page");
      final String agentCode = request.getParameter("agentCode");
      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);
      request.setAttribute("page", (page == null ? "" : page));
      request.setAttribute("agentCode", (agentCode == null ? "" : agentCode));

      log.debug("End - downline()");

      return "downline";
    } catch (Exception ex) {
      log.error("An error occurred. Stack trace is:", ex);
      throw ex;
    }

  }

  public String ownPerformance() throws Exception {

    try {
      log.debug("Start - ownPerformance()");

      String page = request.getParameter("page");
      final String agentCode = request.getParameter("agentCode");
      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);
      request.setAttribute("page", (page == null ? "" : page));
      request.setAttribute("agentCode", (agentCode == null ? "" : agentCode));

      log.debug("End - ownPerformance()");

      return "ownPerformance";
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String popupSearch() {

    try {
      log.debug("Start - popupSearch()");

      String searchBy = request.getParameter("searchBy");
      String searchInput = request.getParameter("searchInput");
      String searchFlag = request.getParameter("searchFlag");

      if (searchFlag == null) {

        searchFlag = "false";

      }

      log.debug("searchInput - " + searchInput);
      log.debug("searchBy - " + searchBy);

      request.setAttribute("searchInput", searchInput);
      request.setAttribute("searchBy", searchBy);
      request.setAttribute("searchFlag", searchFlag);

      log.debug("End - popupSearch()");

      return "popupSearch";
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String doSearchAgent() throws Exception {

    try {
      log.debug("Start - doSearchAgent()");

      String result;
      String page = request.getParameter("page");
      String searchBy = request.getParameter("searchBy");
      String searchInput = request.getParameter("searchInput");

      log.debug("searchInput - " + searchInput);
      log.debug("searchBy - " + searchBy);

      List<AgentsBean> listAgent = null;

      if ("id".equals(searchBy)) {

        listAgent = agentStructureBo.searchAgentById(searchInput);

      } else if ("name".equals(searchBy)) {

        listAgent = agentStructureBo.searchAgentByName(searchInput);
      }

      result = "doSearchAgent";
      request.setAttribute("agentList", listAgent);
      request.setAttribute("searchBy", searchBy);
      request.setAttribute("searchInput", searchInput);

      log.debug("End - doSearchAgent()");

      return result;
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  private void genTierTree(List<TierBean> tierBeans, String uplineL1Code) {

    try {
      for (TierBean tierBean : tierBeans) {

        if (tierBean.getUplineL1Code() != null && tierBean.getUplineL1Code().equals(uplineL1Code)
            && tierBean.getAgentCode() != null && !tierBean.getAgentCode().equals(uplineL1Code)) {

          Map<String, String> mapTier = new HashMap<String, String>();
          mapTier.put("agent_code", tierBean.getAgentCode());
          mapTier.put("upline_code", uplineL1Code);
          mapTier.put("display", tierBean.getDisplay());
          listMapTier.add(mapTier);

          genTierTree(tierBeans, tierBean.getAgentCode());

        }

      }
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  private void genUplineTierTree(List<TierBean> tierBeans, String uplineL1Code) {

    try {
      for (TierBean tierBean : tierBeans) {

        if (tierBean.getAgentCode() != null && tierBean.getAgentCode().equals(uplineL1Code)) {

          Map<String, String> mapTier = new HashMap<String, String>();
          mapTier.put("agent_code", tierBean.getAgentCode());
          mapTier.put("upline_code",
              (tierBean.getAgentCode() != null && tierBean.getUplineL1Code() != null
                  && !tierBean.getAgentCode().equals(tierBean.getUplineL1Code())
                      ? tierBean.getUplineL1Code()
                      : ""));
          mapTier.put("display", tierBean.getDisplay());
          listMapTier.add(mapTier);

          if (!mapTier.get("upline_code").toString().equals("")) {

            genUplineTierTree(tierBeans, mapTier.get("upline_code").toString());

          }

        }

      }
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String ajaxSearchAgentByKeyIn() throws Exception {

    try {
      log.debug("Start - ajaxSearchAgentByKeyIn()");

      String searchInput = request.getParameter("searchInput");
      String searchBy = request.getParameter("searchBy");
      String page = request.getParameter("page");

      log.debug("searchInput - " + searchInput);
      log.debug("searchBy - " + searchBy);
      log.debug("page - " + page);

      String result = null;

      if ("displayStructure".equals(page)) {

        result = "jsonResponse";

        String scheduleId = request.getParameter("scheduleId");

        List<TierBean> tierBeans = scheduleBo.listTier(scheduleId);

        listMapTier = new ArrayList<Map<String, String>>();
        for (TierBean tierBean : tierBeans) {

          if (tierBean.getAgentCode() != null && tierBean.getAgentCode().equals(searchInput)) {

            Map<String, String> mapTier = new HashMap<String, String>();
            mapTier.put("agent_code", searchInput);
            // mapTier.put("upline_code", "");
            mapTier.put("upline_code",
                (tierBean.getAgentCode() != null && tierBean.getUplineL1Code() != null
                    && !tierBean.getAgentCode().equals(tierBean.getUplineL1Code())
                        ? tierBean.getUplineL1Code()
                        : ""));
            mapTier.put("display", tierBean.getDisplay());

            listMapTier.add(mapTier);

            genTierTree(tierBeans, searchInput);

            if (!mapTier.get("upline_code").toString().equals("")) {

              genUplineTierTree(tierBeans, mapTier.get("upline_code").toString());

            }

            break;

          }

        }

      } else if ("downline".equals(page)) {

        result = "doAgentInfo";

        String scheduleId = request.getParameter("scheduleId");

        AgentsBean agent = agentStructureBo.getAgentInfo(searchInput);

        if (agent == null) {

          agent = new AgentsBean();

        }

        AgentsBean uplineAgent = agentStructureBo.findUplineAgent(searchInput);

        List<DownlineAgentsBean> downlineList =
            agentStructureBo.listDownlineAgents(searchInput, scheduleId);

        agent.setUplineAgent(uplineAgent);
        agent.setDownlineList(downlineList);

        request.setAttribute("agent", agent);

      } else if ("performance".equalsIgnoreCase(page)) {

        result = "doAgentPerformance";
        String scheduleId = request.getParameter("scheduleId");
        String agentId = request.getParameter("searchInput");
        String version = request.getParameter("ver");

        AgentsBean agent = agentStructureBo.getAgentInfo(searchInput);

        agent.setDownlineList(agentStructureBo.listDownlineAgents(searchInput, scheduleId));

        AgentsBean uplineAgent = agentStructureBo.findUplineAgent(searchInput);

        List<SummaryPerformanceBean> allPerform =
            agentStructureBo.listPerformance(agentId, scheduleId, Integer.parseInt(version));
        BigDecimal zeroDec = new BigDecimal(0.0000);
        List<SummaryPerformanceBean> performanceList = new ArrayList<SummaryPerformanceBean>();
        List<ClawbackBean> clawbackList = new ArrayList<ClawbackBean>();
        BigDecimal clawTotal = new BigDecimal(0);
        int iPerform = 0;
        int iClaw = 0;
        for (SummaryPerformanceBean pref : allPerform) {

          SummaryPerformanceBean pData = new SummaryPerformanceBean();
          ClawbackBean cData = new ClawbackBean();
          
          pData.setPolicyNumber(pref.getPolicyNumber());
          cData.setPolicyNumber(pref.getPolicyNumber());

          pData.setRiderNumber(pref.getRiderNumber());
          cData.setRiderNumber(pref.getRiderNumber());

          pData.setYear(pref.getYear());
          cData.setYear(pref.getYear());

          pData.setAcFlag(pref.getAcFlag() != null ? "Y" : "");
          cData.setAcFlag(pref.getAcFlag() != null ? "Y" : "");

          if (pref.getFyp() != null && pref.getFyp().compareTo(zeroDec) > 0) {

            pData.setFyp(pref.getFyp());

          } else if (pref.getFyp() != null && pref.getFyp().compareTo(zeroDec) < 0) {

            cData.setFyp(pref.getFyp());
            clawTotal = clawTotal.add(pref.getFyp());

          }

          if (pref.getAfyp() != null && pref.getAfyp().compareTo(zeroDec) > 0) {

            pData.setAfyp(pref.getAfyp());

          } else if (pref.getAfyp() != null && pref.getAfyp().compareTo(zeroDec) < 0) {

            cData.setAfyp(pref.getAfyp());
            clawTotal = clawTotal.add(pref.getAfyp());

          }

          if (pref.getFyc() != null && pref.getFyc().compareTo(zeroDec) > 0) {

            pData.setFyc(pref.getFyc());

          } else if (pref.getFyc() != null && pref.getFyc().compareTo(zeroDec) < 0) {

            cData.setFyc(pref.getFyc());
            clawTotal = clawTotal.add(pref.getFyc());

          }

          if (pref.getAfyc() != null && pref.getAfyc().compareTo(zeroDec) > 0) {

            pData.setAfyc(pref.getAfyc());

          } else if (pref.getAfyc() != null && pref.getAfyc().compareTo(zeroDec) < 0) {

            cData.setAfyc(pref.getAfyc());
            clawTotal = clawTotal.add(pref.getAfyc());

          }

          if (pref.getRyp() != null && pref.getRyp().compareTo(zeroDec) > 0) {

            pData.setRyp(pref.getRyp());

          } else if (pref.getRyp() != null && pref.getRyp().compareTo(zeroDec) < 0) {

            cData.setRyp(pref.getRyp());
            clawTotal = clawTotal.add(pref.getRyp());

          }

          if (pref.getAryp() != null && pref.getAryp().compareTo(zeroDec) > 0) {

            pData.setAryp(pref.getAryp());

          } else if (pref.getAryp() != null && pref.getAryp().compareTo(zeroDec) < 0) {

            cData.setAryp(pref.getAryp());
            clawTotal = clawTotal.add(pref.getAryp());

          }

          if (pref.getRyc() != null && pref.getRyc().compareTo(zeroDec) > 0) {

            pData.setRyc(pref.getRyc());

          } else if (pref.getRyc() != null && pref.getRyc().compareTo(zeroDec) < 0) {

            cData.setRyc(pref.getRyc());
            clawTotal = clawTotal.add(pref.getRyc());

          }

          if (pData.getFyp() != null
              || pData.getAfyp() != null
              || pData.getFyc() != null
              || pData.getAfyc() != null
              || pData.getRyp() != null
              || pData.getAryp() != null
              || pData.getRyc() != null) {
        	  
        	  pData.setNo(++iPerform);
            performanceList.add(pData);

          }

          if (cData.getFyp() != null
              || cData.getAfyp() != null
              || cData.getFyc() != null
              || cData.getAfyc() != null
              || cData.getRyp() != null
              || cData.getAryp() != null
              || cData.getRyc() != null) {
        	  
        	  cData.setNo(++iClaw);
            clawbackList.add(cData);

          }

        }

        request.setAttribute("clawBankTotal", clawTotal);

        agent.setUplineAgent(uplineAgent);

        request.setAttribute("agent", agent);
        request.setAttribute("performanceList", performanceList);
        request.setAttribute("clawbackList", clawbackList);

      } else if ("resultDisplay".equalsIgnoreCase(page)) {

        result = "doResultDisplay";
        String scheduleId = request.getParameter("scheduleId");
        String agentInput = request.getParameter("searchInput");
        String rule = request.getParameter("rule");
        String compareVersion = request.getParameter("compareVersion");
        String mainVersion = request.getParameter("mainVersion");

        log.debug("Rule - " + rule);
        log.debug("ScheduleId - " + scheduleId);
        log.debug("CompareVersion - " + compareVersion);
        log.debug("MainVersion - " + mainVersion);

        List<ResultBenefitBean> listBenefit = new ArrayList<>();
        
		if ("id".equals(searchBy)) {

			listBenefit = resultDisplayBo.listBenefitByVersion(rule, scheduleId, Integer.parseInt(mainVersion), Integer.parseInt(compareVersion), agentInput);

		} else if ("name".equals(searchBy)) {

			listBenefit = resultDisplayBo.listBenefitByVersionByName(rule, scheduleId, Integer.parseInt(mainVersion), Integer.parseInt(compareVersion), agentInput);

		}

        request.setAttribute("listBenefit", listBenefit);
        request.setAttribute("scheduleId", scheduleId);
        request.setAttribute("agentInput", agentInput);
        request.setAttribute("rule", rule);
        request.setAttribute("compareVersion", compareVersion);
        request.setAttribute("mainVersion", mainVersion);

      }

      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);

      log.debug("End - ajaxSearchAgentByKeyIn()");

      return result;
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public ScheduleBean getCurrentSchedule() {
    return currentSchedule;
  }

  public void setCurrentSchedule(ScheduleBean currentSchedule) {
    this.currentSchedule = currentSchedule;
  }

  public void setAgentStructureBo(AgentStructureBo agentStructureBo) {
    this.agentStructureBo = agentStructureBo;
  }

  public void setScheduleBo(ScheduleBo scheduleBo) {
    this.scheduleBo = scheduleBo;
  }

  public CurrentScheduleBean getCurrentSche() {
    return currentSche;
  }

  public void setCurrentSche(CurrentScheduleBean currentSche) {
    this.currentSche = currentSche;
  }

  public void setResultDisplayBo(ResultDisplayBo resultDisplayBo) {
    this.resultDisplayBo = resultDisplayBo;
  }

  public Map<String, Object> getResultMap() {
    return resultMap;
  }

  public void setResultMap(Map<String, Object> resultMap) {
    this.resultMap = resultMap;
  }

  public List<Map<String, String>> getListMapTier() {
    return listMapTier;
  }

  public void setListMapTier(List<Map<String, String>> listMapTier) {
    this.listMapTier = listMapTier;
  }

}
