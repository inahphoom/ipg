/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.action.common;

import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.util.ServletContextAware;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Adichart
 */
public class BaseAction extends ActionSupport
    implements ServletContextAware, SessionAware, ServletRequestAware, ServletResponseAware {

  private static final long serialVersionUID = 3110702722138832798L;

  protected Logger log = Logger.getLogger(BaseAction.class);

  protected ServletContext context;
  protected Map<String, Object> session = null;
  protected HttpServletRequest request = null;
  protected HttpServletResponse response = null;
  protected WebApplicationContext ctx;

  public <T> T getBean(Class<T> clazz) {
    this.log.info("Get Class bean :" + clazz.toString());
    try {
      if (null == this.ctx) {
        this.ctx = WebApplicationContextUtils.getWebApplicationContext(this.context);
      }
      return (T) this.ctx.getBean(clazz);
    } catch (Exception e) {
      this.log.error(ExceptionUtils.getStackTrace(e));
      return null;
    }
  }

  public Object getBean(String name) {
    this.log.info("Get String bean :" + name);
    try {
      if (null == this.ctx) {
        this.ctx = WebApplicationContextUtils.getWebApplicationContext(this.context);
      }
      return this.ctx.getBean(name);
    } catch (Exception e) {
      this.log.error(ExceptionUtils.getStackTrace(e));
      return null;
    }
  }

  public Map<String, Object> getSession() {
    return this.session;
  }

  public void setSession(Map<String, Object> session) {
    this.session = session;
  }

  public HttpServletRequest getRequest() {
    return this.request;
  }

  public HttpServletResponse getResponse() {
    return this.response;
  }

  public WebApplicationContext getCtx() {
    return this.ctx;
  }

  public void setCtx(WebApplicationContext ctx) {
    this.ctx = ctx;
  }

  public Logger getLog() {
    return this.log;
  }

  public void setLog(Logger log) {
    this.log = log;
  }

  protected void setAttributeSessionbeans(String key, Object value) {
    this.request.getSession().setAttribute(key, value);
  }

  protected Object getAttributeSessionbeans(String key) {
    return this.request.getSession().getAttribute(key);
  }

  protected void removeAttributeSessionbeans(String name) {
    this.request.getSession().removeAttribute(name);
  }

  public void setServletResponse(HttpServletResponse response) {
    this.response = response;
  }

  public void setServletRequest(HttpServletRequest request) {
    this.request = request;
  }

  public void setServletContext(ServletContext context) {
    this.context = context;
  }

  public ServletContext getServletContext() {
    return this.context;
  }
}
