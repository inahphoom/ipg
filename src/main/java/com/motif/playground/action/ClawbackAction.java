/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;
// import com.motif.playground.beans.AgentBean;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.PolicyClawbackBean;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.bo.AgentStructureBo;
import com.motif.playground.bo.ClawbackBo;
import com.motif.playground.bo.ScheduleBo;

// import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adichart
 */
public class ClawbackAction extends BaseAction {

  private static final long serialVersionUID = -6932687865724023959L;
  protected ScheduleBean currentSchedule;
  protected VersionBean scheduleVersion;
  CurrentScheduleBean currentSche;
  AgentStructureBo agentStructureBo;
  ScheduleBo scheduleBo;
  ClawbackBo clawbackBo;

  @Override
  public String execute() throws Exception {
    try {
      log.debug("Start - execute()");

      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);

      log.debug("End - execute()");
      return "execute";
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }
  }

  public String doSearchClawback() throws Exception {
    try {
      log.debug("Start - doSearchClawback()");

      String scheduleId = request.getParameter("scheduleId");
      String version = request.getParameter("version");
      String policyNo = request.getParameter("policyNo");
      String riderNo = request.getParameter("riderNo");

      List<PolicyClawbackBean> agentList =
          clawbackBo.listAgentClawback(scheduleId, Integer.parseInt(version), policyNo, riderNo);
      currentSche = scheduleBo.getCurrenSchedule();

      request.setAttribute("currentSchedule", currentSche);
      request.setAttribute("agentList", agentList);

      log.debug("End - doSearchClawback()");
      return "doSearchClawback";
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }
  }

  /**
   * @return the currentSchedule
   */
  public ScheduleBean getCurrentSchedule() {
    return currentSchedule;
  }

  /**
   * @param currentSchedule the currentSchedule to set
   */
  public void setCurrentSchedule(ScheduleBean currentSchedule) {
    this.currentSchedule = currentSchedule;
  }

  public VersionBean getScheduleVersion() {
    return scheduleVersion;
  }

  public void setScheduleVersion(VersionBean scheduleVersion) {
    this.scheduleVersion = scheduleVersion;
  }

  public CurrentScheduleBean getCurrentSche() {
    return currentSche;
  }

  public void setCurrentSche(CurrentScheduleBean currentSche) {
    this.currentSche = currentSche;
  }

  public AgentStructureBo getAgentStructureBo() {
    return agentStructureBo;
  }

  public void setAgentStructureBo(AgentStructureBo agentStructureBo) {
    this.agentStructureBo = agentStructureBo;
  }

  public ScheduleBo getScheduleBo() {
    return scheduleBo;
  }

  public void setScheduleBo(ScheduleBo scheduleBo) {
    this.scheduleBo = scheduleBo;
  }

  public ClawbackBo getClawbackBo() {
    return clawbackBo;
  }

  public void setClawbackBo(ClawbackBo clawbackBo) {
    this.clawbackBo = clawbackBo;
  }
}
