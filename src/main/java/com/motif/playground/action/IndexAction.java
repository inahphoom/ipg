/*
 * To change this license header, choose License Headers in Project Properties. To change this
 * template file, choose Tools | Templates and open the template in the editor.
 */

package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;

/**
 *
 * @author Adichart
 */
public class IndexAction extends BaseAction {

  private static final long serialVersionUID = -8978972373994146783L;

  public String index() throws Exception {
    log.debug("Start - index()");
    request.setAttribute("username", "abc123");
    log.debug("End - index()");
    return "index";
  }
}
