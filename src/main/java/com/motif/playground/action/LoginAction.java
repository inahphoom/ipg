package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;
import com.motif.playground.beans.UserBean;
import com.motif.playground.bo.UserBo;
import com.opensymphony.xwork2.ActionContext;

import util.AuthenUtil;

/**
 * @author Thanaphoom B.
 *
 */
public class LoginAction extends BaseAction {

  private static final long serialVersionUID = -9209931676636998233L;
  UserBo userBo;

  public String login() throws Exception {
    log.debug("Start - login()");

    String result;
    try {
      String username = request.getParameter("username");
      String password = request.getParameter("password");

      log.debug("UserName : " + username);
      // log.debug("Password : " + password);

      boolean isSuccess = authenticate(username, password);
      if (isSuccess) {
        result = "loginSuccess";
      } else {
        addActionError("Invalid Login");
        result = "loginFail";
      }

      log.debug("End - login()");
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

    return result;
  }

  private boolean authenticate(String username, String password) {
    boolean isSuccess = false;
    if (userBo.isInternalUser(username)) {
      UserBean user = userBo.login(username, password);
      if (user != null) {
        ActionContext.getContext().getSession().put("loginId", user.getLoginName());
        isSuccess = true;
      }
    } else {
      boolean flagLDAP = AuthenUtil.authenticate(username, password);
      log.debug("ldap login : " + flagLDAP);
      if (flagLDAP) {
        ActionContext.getContext().getSession().put("loginId", username);
        isSuccess = true;
      }
    }
    if (isSuccess) {
      ActionContext.getContext().getSession().put("loginRole", userBo.getRole(username));
    }
    return isSuccess;
  }

  public String logout() throws Exception {
    log.debug("Start - login()");

    ActionContext.getContext().getSession().remove("loginId");
    ActionContext.getContext().getSession().remove("loginRole");

    String result = "succcess";

    log.debug("End - login()");

    return result;
  }

  public UserBo getUserBo() {
    return userBo;
  }

  public void setUserBo(UserBo userBo) {
    this.userBo = userBo;
  }

}
