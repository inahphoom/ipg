package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;
import com.motif.playground.beans.BasicDropdownBean;
import com.motif.playground.beans.ScheduleBeanICOM;
import com.motif.playground.beans.ScheduleBeanICOMDB;
import com.motif.playground.beans.VersionBeanICOM;
import com.motif.playground.bo.ScheduleBo;
import com.motif.playground.bo.VersionBo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectDataAction extends BaseAction {

  private static final long serialVersionUID = -4119152517036283367L;

  public List<BasicDropdownBean> scheduleIdList;
  public Map<String, Object> resultMap;
  public SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
  public ScheduleBo scheduleBo;
  public VersionBo versionBo;

  public String selectData() throws Exception {

    log.debug("Start - selectData()");

    try {
      scheduleIdList = new ArrayList<BasicDropdownBean>();
      List<ScheduleBeanICOM> scheduleBeanICOMs = scheduleBo.listScheduleICOM();

      for (ScheduleBeanICOM scheduleBeanICOM : scheduleBeanICOMs) {

        scheduleIdList.add(new BasicDropdownBean(scheduleBeanICOM.getScheduleId(),
            scheduleBeanICOM.getScheduleId()));

      }

      request.setAttribute("scheduleIdList", scheduleIdList);

      log.debug("End - selectData()");

      return "selectData";
    } catch (Exception e) {
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String ajaxFetchScheduleData() {

    log.debug("Start - ajaxFetchScheduleData()");

    try {
      String scheduleId = request.getParameter("scheduleId");
      log.debug("scheduleId - " + scheduleId);

      resultMap = new HashMap<String, Object>();

      List<VersionBeanICOM> versionBeanICOMs = versionBo.listVersionICOM(scheduleId);
      ScheduleBeanICOM scheduleBeanICOM = scheduleBo.findScheduleICOM(scheduleId);

      resultMap.put("desc", scheduleBeanICOM.getScheduleDesc());
      resultMap.put("from", dateFormat.format(scheduleBeanICOM.getPeriodFrom()));
      resultMap.put("to", dateFormat.format(scheduleBeanICOM.getPeriodTo()));

      List<BasicDropdownBean> versionList = new ArrayList<BasicDropdownBean>();
      for (VersionBeanICOM versionBeanICOM : versionBeanICOMs) {

        versionList.add(new BasicDropdownBean(String.valueOf(versionBeanICOM.getVer()),
            String.valueOf(versionBeanICOM.getVer())));

      }

      resultMap.put("versionList", versionList);

      log.debug("End - ajaxFetchScheduleData()");

      return "jsonResponse";
    } catch (Exception e) {
      //e.printStackTrace();
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String ajaxSetData() {

    log.debug("Start - ajaxSetData()");
    try {
      String scheduleId = request.getParameter("scheduleId");
      String version = request.getParameter("version");

      log.debug("scheduleId - " + scheduleId);
      log.debug("version - " + version);

      boolean result = scheduleBo.setData(scheduleId, new BigDecimal(version), new BigDecimal("0"));

      resultMap = new HashMap<String, Object>();
      resultMap.put("result", result);

      log.debug("End - ajaxSetData()");

      return "jsonResponse";
    } catch (Exception e) {
      //e.printStackTrace();
      log.error(e);
      return "fail";
    }


  }

  public String pullData() throws Exception {

    log.debug("Start - pullData()");

    try {
      scheduleIdList = new ArrayList<BasicDropdownBean>();
      List<ScheduleBeanICOM> scheduleBeanICOMs = scheduleBo.listScheduleICOM();

      for (ScheduleBeanICOM scheduleBeanICOM : scheduleBeanICOMs) {

        scheduleIdList.add(new BasicDropdownBean(scheduleBeanICOM.getScheduleId(),
            scheduleBeanICOM.getScheduleId()));

      }

      request.setAttribute("scheduleIdList", scheduleIdList);

      log.debug("End - pullData()");

      return "pullData";
    } catch (Exception e) {
      //e.printStackTrace();
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String ajaxListScheduleICOMDB() {

    log.debug("Start - ajaxListScheduleICOMDB()");

    try {
      String scheduleId = request.getParameter("scheduleId");
      log.debug("scheduleId - " + scheduleId);

      scheduleIdList = new ArrayList<BasicDropdownBean>();
      resultMap = new HashMap<String, Object>();

      List<ScheduleBeanICOMDB> scheduleBeanICOMDBs = scheduleBo.listScheduleICOMDB(scheduleId);

      for (ScheduleBeanICOMDB scheduleBeanICOMDB : scheduleBeanICOMDBs) {

        scheduleIdList.add(new BasicDropdownBean(scheduleBeanICOMDB.getScheduleId(),
            scheduleBeanICOMDB.getScheduleId()));

      }

      resultMap.put("scheduleIdList", scheduleIdList);

      log.debug("End - ajaxListScheduleICOMDB()");

      return "jsonResponse";
    } catch (Exception e) {
      //e.printStackTrace();
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String ajaxImportData() {

    log.debug("Start - ajaxImportData()");

    try {
      String scheduleId = request.getParameter("scheduleId");

      log.debug("scheduleId - " + scheduleId);

      int newVer = 0;
      try {
        newVer = scheduleBo.importData(scheduleId);
      } catch (Exception ex) {
        //ex.printStackTrace();
        log.error("An error occurred. Stack trace is:", ex);
        resultMap = new HashMap<String, Object>();
        resultMap.put("result", false);
        resultMap.put("errMsg", ex.getMessage());
        return "jsonResponse";
      }

      resultMap = new HashMap<String, Object>();
      resultMap.put("result", true);
      resultMap.put("scheduleId", scheduleId);
      resultMap.put("ver", newVer);

      log.debug("End - ajaxImportData()");

      return "jsonResponse";
    } catch (Exception e) {
      //e.printStackTrace();
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }

  }

  public String synchronizeUsers() throws Exception {

    log.debug("Start - synchronizeUsers()");
    // boolean result = scheduleBo.synchronizeUsers();
    // request.setAttribute("result", result);
    // log.debug("End - synchronizeUsers()");
    return "synchronizeUsers";

  }

  public String ajaxSynchronizeUsers() throws Exception {

    log.debug("Start - ajaxSynchronizeUsers()");
    boolean result = scheduleBo.synchronizeUsers();
    request.setAttribute("result", result);     
    
    resultMap = new HashMap<String, Object>();
    resultMap.put("result", result);

    log.debug("End - ajaxSynchronizeUsers()");
    return "ajaxSynchronizeUsers";
  }

  public List<BasicDropdownBean> getScheduleIdList() {
    return scheduleIdList;
  }

  public void setScheduleIdList(List<BasicDropdownBean> scheduleIdList) {
    this.scheduleIdList = scheduleIdList;
  }

  public Map<String, Object> getResultMap() {
    return resultMap;
  }

  public void setResultMap(Map<String, Object> resultMap) {
    this.resultMap = resultMap;
  }

  public void setScheduleBo(ScheduleBo scheduleBo) {
    this.scheduleBo = scheduleBo;
  }

  public void setVersionBo(VersionBo versionBo) {
    this.versionBo = versionBo;
  }

}
