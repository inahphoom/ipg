package com.motif.playground.action;

import com.motif.playground.action.common.BaseAction;
import com.motif.playground.beans.AgentsBean;
import com.motif.playground.beans.BasicDropdownBean;
import com.motif.playground.beans.BenefitRuleBean;
import com.motif.playground.beans.CurrentScheduleBean;
import com.motif.playground.beans.DownlineAgentsBean;
import com.motif.playground.beans.ResultBenefitBean;
import com.motif.playground.beans.ScheduleBean;
import com.motif.playground.beans.VersionBean;
import com.motif.playground.bo.AgentStructureBo;
import com.motif.playground.bo.ResultDisplayBo;
import com.motif.playground.bo.ScheduleBo;
import com.motif.playground.bo.VersionBo;

//import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ResultDisplayAction extends BaseAction {

  private static final long serialVersionUID = -4904981431024108193L;
  
  protected ScheduleBean currentSchedule;
  protected VersionBean scheduleVersion;
  CurrentScheduleBean currentSche;
  ScheduleBo scheduleBo;
  VersionBo versionBo;
  ResultDisplayBo resultDisplayBo;
  AgentStructureBo agentStructureBo;

  public String byBenefit() throws Exception {
    
    //if (ConfigManager.printLogViaSystemOut() && log.isDebugEnabled()) {
    //  System.out.println("ResultDisplay - byBenefit starts.");
    //}
    log.debug("Start - byBenefit()");
    try {
      currentSche = scheduleBo.getCurrenSchedule();
      request.setAttribute("currentSchedule", currentSche);

      List<BenefitRuleBean> listRule = resultDisplayBo.getRule();
      List<VersionBean> versions = versionBo.listVersion(currentSche.getScheduleId());
      List<BasicDropdownBean> ruleList = new ArrayList<BasicDropdownBean>();
      List<BasicDropdownBean> versionList = new ArrayList<BasicDropdownBean>();
      for (BenefitRuleBean rule : listRule) {
        ruleList.add(new BasicDropdownBean(rule.getBenefitCode(), rule.getRuleDesc()));
      }

      for (VersionBean version : versions) {
        if (Integer.parseInt(currentSche.getVersion()) != Integer
            .parseInt(String.valueOf(version.getVer()))) {
          versionList.add(new BasicDropdownBean(String.valueOf(version.getVer()),
              String.valueOf(version.getVer())));
        }
      }

      request.setAttribute("ruleList", ruleList);
      request.setAttribute("versionList", versionList);

      log.debug("End - byBenefit()");
      return "byBenefit";
    } catch (Exception ex) {
      //if (ConfigManager.printLogViaSystemOut()) {
      // ex.printStackTrace();
      //}
      log.error("An error occurred. Stack trace is:", ex);
      throw ex;
    }
  }

  public String doCompare() throws Exception {
    log.debug("Start - doCompare()");
    String result = "";
    try {
      String page = request.getParameter("page");
      if ("resultDisplayByAgent".equalsIgnoreCase(page)) {
        result = "doResultDisplayByAgent";
        String scheduleId = request.getParameter("scheduleId");
        String agentId = request.getParameter("agentId");
        String rule = request.getParameter("rule");
        String compareVersion = request.getParameter("compareVersion");
        String mainVersion = request.getParameter("mainVersion");
        String policyNo = request.getParameter("policyNo");
        String riderNo = request.getParameter("riderNo");

        log.debug("Rule - " + rule);
        log.debug("ScheduleId - " + scheduleId);
        log.debug("CompareVersion - " + compareVersion);
        log.debug("MainVersion - " + mainVersion);

        currentSche = scheduleBo.getCurrenSchedule();
        AgentsBean agent = agentStructureBo.getAgentInfo(agentId);
        
        List<DownlineAgentsBean> downlineList =
                agentStructureBo.listDownlineAgents(agentId, scheduleId);
        agent.setDownlineList(downlineList);
        
        ResultBenefitBean benefitData = resultDisplayBo.getBenefitDetail(rule, scheduleId,
            Integer.parseInt(mainVersion), Integer.parseInt(compareVersion), agentId, policyNo,
            riderNo);

        request.setAttribute("agent", agent);
        request.setAttribute("benefit", benefitData);
        request.setAttribute("policyno", policyNo);
        request.setAttribute("riderno", riderNo);
        request.setAttribute("compareVersion", compareVersion);
        request.setAttribute("currentSchedule", currentSche);
      } else if ("doCompare".equalsIgnoreCase(page)) {
        result = "doCompare";
        String rule = request.getParameter("rule");
        String scheduleId = request.getParameter("scheduleId");
        String compareVersion = request.getParameter("version");
        String mainVersion = request.getParameter("mainVersion");

        log.debug("Rule - " + rule);
        log.debug("ScheduleId - " + scheduleId);
        log.debug("CompareVersion - " + compareVersion);


        request.setAttribute("rule", rule);
        request.setAttribute("scheduleId", scheduleId);
        request.setAttribute("compareVersion", compareVersion);
        request.setAttribute("mainVersion", mainVersion);
        request.setAttribute("searchInput", "");
      }
      log.debug("End - doCompare()");
    } catch (Exception e) {
      //e.printStackTrace();
      log.error("An error occurred. Stack trace is:", e);
      throw e;
    }
    return result;
  }

  /**
   * @return the currentSchedule
   */
  public ScheduleBean getCurrentSchedule() {
    return currentSchedule;
  }

  /**
   * @param currentSchedule the currentSchedule to set
   */
  public void setCurrentSchedule(ScheduleBean currentSchedule) {
    this.currentSchedule = currentSchedule;
  }

  public CurrentScheduleBean getCurrentSche() {
    return currentSche;
  }

  public void setCurrentSche(CurrentScheduleBean currentSche) {
    this.currentSche = currentSche;
  }

  public void setScheduleBo(ScheduleBo scheduleBo) {
    this.scheduleBo = scheduleBo;
  }

  public void setResultDisplayBo(ResultDisplayBo resultDisplayBo) {
    this.resultDisplayBo = resultDisplayBo;
  }

  public void setVersionBo(VersionBo versionBo) {
    this.versionBo = versionBo;
  }

  public void setAgentStructureBo(AgentStructureBo agentStructureBo) {
    this.agentStructureBo = agentStructureBo;
  }
}
