
package com.motiftech.common.authen;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

  private static final long serialVersionUID = -745544979223638912L;

  private int userId;
  private String username = "";
  private String password = "";
  // private SegUser user;
  private Collection<GrantedAuthority> authorities;


  public UserDetails() {}


  public UserDetails(String username, String password) {
    this.username = username;
    this.password = password;
  }


  /**
   * @return the userId
   */
  public int getUserId() {
    return userId;
  }


  //
  // public SegUser getUser() {
  // return user;
  // }
  //
  // public void setUser(SegUser user) {
  // this.user = user;
  // }

  @Override
  public Collection<GrantedAuthority> getAuthorities() {
    return this.authorities;
  }


  /************* implement method ***************/
  @Override
  public String getPassword() {
    return this.password;
  }


  @Override
  public String getUsername() {
    return this.username;
  }


  @Override
  public boolean isAccountNonExpired() {
    return true;
  }


  @Override
  public boolean isAccountNonLocked() {
    return true;
  }


  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }


  @Override
  public boolean isEnabled() {
    return true;
  }


  @Override
  public boolean equals(Object rhs) {
    if (!(rhs instanceof UserDetails) || (rhs == null)) {
      return false;
    }

    UserDetails user = (UserDetails) rhs;

    // We rely on constructor to guarantee any User has non-null
    // authorities
    if (!authorities.equals(user.authorities)) {
      return false;
    }

    // We rely on constructor to guarantee non-null username and password
    return (this.getPassword().equals(user.getPassword())
        && this.getUsername().equals(user.getUsername())
        && (this.isAccountNonExpired() == user.isAccountNonExpired())
        && (this.isAccountNonLocked() == user.isAccountNonLocked())
        && (this.isCredentialsNonExpired() == user.isCredentialsNonExpired())
        && (this.isEnabled() == user.isEnabled()));
  }

}
