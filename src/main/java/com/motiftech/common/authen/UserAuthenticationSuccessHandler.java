
package com.motiftech.common.authen;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;



public class UserAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

  private final Logger log = Logger.getLogger(this.getClass());


  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
      HttpServletResponse response, Authentication authentication)
      throws ServletException, IOException {
    this.log.info("--- onAuthenticationSuccess ---");

    super.onAuthenticationSuccess(request, response, authentication);
    this.log.info("--- onAuthenticationSuccess Success.............");

  }
}
