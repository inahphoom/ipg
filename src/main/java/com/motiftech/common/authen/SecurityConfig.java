
package com.motiftech.common.authen;

//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.web.authentication.AuthenticationFailureHandler;
//import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
//import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
//import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

//@Configuration
//@EnableWebSecurity
// @Profile("container")
public class SecurityConfig 
//extends WebSecurityConfigurerAdapter
{

//  @Autowired
//  private AuthenticationProvider authenticationProvider;
//
//  @Autowired
//  private AuthenticationProvider authenticationProviderDB;
//
//
//  @Override
//  @Order(1)
//
//  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//    auth.authenticationProvider(authenticationProvider);
//  }
//
//
//  @Override
//  public void configure(WebSecurity web) throws Exception {
//    web.ignoring().antMatchers("/scripts/**", "/styles/**", "/images/**", "/error/**");
//  }
//
//
//  @Order(2)
//  protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//    auth.authenticationProvider(authenticationProviderDB);
//  }
//
//
//  @Override
//  public void configure(HttpSecurity http) throws Exception {
//    http
//        .authorizeRequests()
//        .antMatchers("/rest/**").authenticated()
//        .antMatchers("/**").permitAll()
//        .anyRequest().authenticated()
//        .and()
//        .formLogin()
//        .successHandler(new AuthenticationSuccessHandler() {
//          @Override
//          public void onAuthenticationSuccess(
//              HttpServletRequest request, HttpServletResponse response,
//              Authentication a) throws IOException, ServletException {
//            // To change body of generated methods,
//            response.setStatus(HttpServletResponse.SC_OK);
//          }
//        })
//        .failureHandler(new AuthenticationFailureHandler())
//        // .loginProcessingUrl("/access/login")
//        .loginProcessingUrl("/login.do")
//        .and()
//        .logout()
//        .logoutUrl("/access/logout")
//        .logoutSuccessHandler(new LogoutSuccessHandler() {
//          @Override
//          public void onLogoutSuccess(
//              HttpServletRequest request,
//              HttpServletResponse response,
//              Authentication a) throws IOException, ServletException {
//            response.setStatus(HttpServletResponse.SC_NO_CONTENT);
//          }
//        })
//        .invalidateHttpSession(true)
//        .and().exceptionHandling().authenticationEntryPoint(new Http403ForbiddenEntryPoint())
//        .and().csrf().disable(); // Disabled CSRF protection
//  }
}
