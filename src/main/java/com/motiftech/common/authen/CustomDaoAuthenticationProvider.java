
package com.motiftech.common.authen;

//import org.springframework.dao.DataAccessException;
//import org.springframework.security.authentication.AuthenticationServiceException;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
//import org.springframework.security.authentication.dao.SaltSource;
//import org.springframework.security.authentication.encoding.PasswordEncoder;
//import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.util.Assert;

public class CustomDaoAuthenticationProvider 
//extends AbstractUserDetailsAuthenticationProvider 
{

//  private PasswordEncoder passwordEncoder = new PlaintextPasswordEncoder();
//  private SaltSource saltSource;
//  private UserDetailsService userDetailsService;
//  private boolean includeDetailsObject = true;
//
//
//  @Override
//  protected void additionalAuthenticationChecks(UserDetails userDetails,
//      UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
//    Object salt = null;
//
//    if (this.saltSource != null) {
//      salt = this.saltSource.getSalt(userDetails);
//    }
//
//    if (authentication.getCredentials() == null) {
//      throw new BadCredentialsException(messages.getMessage(
//          "AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
//          includeDetailsObject ? userDetails : null);
//    }
//
//    if (authentication.getPrincipal() == null) {
//      throw new BadCredentialsException(messages.getMessage(
//          "AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad Principal"),
//          includeDetailsObject ? userDetails : null);
//    }
//
//    String presentedPassword = authentication.getCredentials().toString();
//
//    if (!passwordEncoder.isPasswordValid(userDetails.getPassword(), presentedPassword, salt)) {
//      throw new BadCredentialsException(messages.getMessage(
//          "AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad Credentials password"),
//          includeDetailsObject ? userDetails : null);
//    }
//  }
//
//
//  protected void doAfterPropertiesSet() throws Exception {
//    Assert.notNull(this.userDetailsService, "A UserDetailsService must be set");
//  }
//
//
//  @Override
//  protected final UserDetails retrieveUser(String username,
//      UsernamePasswordAuthenticationToken authentication)
//      throws AuthenticationException {
//    UserDetails loadedUser = null;
//    System.out.println("UserDetails Test");
//    try {
//      loadedUser = this.getUserDetailsService().loadUserByUsername(username,
//          authentication.getCredentials());
//    } catch (DataAccessException repositoryProblem) {
//      throw new AuthenticationServiceException(repositoryProblem.getMessage(), repositoryProblem);
//    }
//
//    if (loadedUser == null) {
//      throw new AuthenticationServiceException(
//          "UserDetailsService returned null, which is an interface contract violation");
//    }
//
//    return loadedUser;
//  }
//
//
//  /**
//   * Sets the PasswordEncoder instance to be used to encode and validate passwords. If not set,
//   * {@link PlaintextPasswordEncoder} will be used by default.
//   *
//   * @param passwordEncoder The passwordEncoder to use
//   */
//  public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
//    this.passwordEncoder = passwordEncoder;
//  }
//
//
//  protected PasswordEncoder getPasswordEncoder() {
//    return passwordEncoder;
//  }
//
//
//  /**
//   * The source of salts to use when decoding passwords. <code>null</code> is a valid value, meaning
//   * the <code>DaoAuthenticationProvider</code> will present <code>null</code> to the relevant
//   * <code>PasswordEncoder</code>.
//   *
//   * @param saltSource to use when attempting to decode passwords via the
//   *        <code>PasswordEncoder</code>
//   */
//  public void setSaltSource(SaltSource saltSource) {
//    this.saltSource = saltSource;
//  }
//
//
//  protected SaltSource getSaltSource() {
//    return saltSource;
//  }
//
//
//  public UserDetailsService getUserDetailsService() {
//    return userDetailsService;
//  }
//
//
//  public void setUserDetailsService(UserDetailsService userDetailsService) {
//    this.userDetailsService = userDetailsService;
//  }
//
//
//  protected boolean isIncludeDetailsObject() {
//    return includeDetailsObject;
//  }
//
//
//  /**
//   * Determines whether the UserDetails will be included in the <tt>extraInformation</tt> field of a
//   * thrown BadCredentialsException. Defaults to true, but can be set to false if the exception will
//   * be used with a remoting protocol, for example.
//   *
//   * @deprecated use
//   *             {@link org.springframework.security.authentication.ProviderManager#setClearExtraInformation(boolean)}
//   */
//  @Deprecated
//  public void setIncludeDetailsObject(boolean includeDetailsObject) {
//    this.includeDetailsObject = includeDetailsObject;
//  }
}
