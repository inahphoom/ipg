
package com.motiftech.common.config;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


public class FileUtil {

  private static final Logger logger = Logger.getLogger(FileUtil.class);


  /**
   * Returns a file handle of a file. The search will be performed in following order:
   * <ol>
   * <li>JVM Parameter (provided by -D[key]=[value])</li>
   * <li>Environment variable</li>
   * <li>Fully qualified file name given by the caller</li>
   * <li>File with specific name in CLASSPATH</li>
   * </ol>
   *
   * @param envKeyName JVM parameter or System environment variable name to look for. This parameter
   *        is case-sensitive. This method will check the JVM parameter first.
   * @param defaultFilePath File with full path name to open if there is no system environment
   *        variable. This is used only when there is no valid file defined in JVM parameter nor
   *        environment variable. Empty String (not null String) should be sent if method should
   *        skip this search.
   * @param defaultFileName The name of the file to be searched in classpath. This is used only when
   *        there is no valid file defined in JVM parameter, environment variable, nor defaultPath.
   *        Empty String (not null String) should be sent if method should skip this search.
   * @param checkFileRead Check if the file can be read before returning. If the value is set to
   *        <strong>true</strong>, it will check if the file can be read. if not, it will look for
   *        other file variable in search order. If the value is set to <strong>false</strong> it
   *        will pass back the first defined value in search order.
   * @return Configuration file.
   * @throws IOException When the file cannot not be read from anywhere in search order.
   */
  public static File getFile(String envKeyName, String defaultFilePath, String defaultFileName,
      boolean checkFileRead) throws IOException {

    List<String> configSearchList = new ArrayList<String>();
    boolean doneSearch = false;

    if (System.getProperty(envKeyName) != null) {
      logger.info("JVM parameter \"" + envKeyName + "\" found.");
      configSearchList.add(System.getProperty(envKeyName));
      if (!checkFileRead) {
        doneSearch = true;
      }
    }
    if (!doneSearch) {
      Map<String, String> envVar = System.getenv();
      for (String envName : envVar.keySet()) {
        if (envKeyName.equals(envName)) {
          logger.info("System environment variable \"" + envKeyName + "\" found.");
          configSearchList.add(envVar.get(envName));
          if (!checkFileRead) {
            doneSearch = true;
          }
          break;
        }
      }
    }
    if (!doneSearch && (defaultFilePath.length() > 0)) {
      logger.info("Default file path \"" + defaultFilePath + "\" was specified.");
      configSearchList.add(defaultFilePath);
      doneSearch = true;
    }
    if (!doneSearch && (defaultFileName.length() > 0)) {
      logger.info("Default file name \"" + defaultFileName + "\" was specified.");
      configSearchList.add(defaultFileName);
      doneSearch = true;
    }

    File configFile;
    if (!checkFileRead) {
      // This case will send back the file specified by first defined variable.
      logger.info("File \"" + configSearchList.get(0) + "\" was chosen from search locations.");
      configFile = new File(configSearchList.get(0));
      return configFile;
    } else {
      // This case will keep reading file specified in list until one is readable, or die trying.
      for (int configIndex = 0; configIndex < configSearchList.size(); configIndex++) {
        configFile = new File(configSearchList.get(configIndex));
        if (configFile.exists() && configFile.canRead()) {
          logger.info("File \"" + configSearchList.get(configIndex)
              + "\" was chosen from search locations.");
          return configFile;
        }
      }
      throw new IOException("Cannot find any readable file in search locations.");
    }

  }


  /**
   * Find the location of a file. The search will be performed in following order:
   * <ol>
   * <li>JVM Parameter (provided by -D[key]=[value])</li>
   * <li>Environment variable</li>
   * <li>Fully qualified file name given by the caller</li>
   * <li>File with specific name in CLASSPATH</li>
   * </ol>
   *
   * @param envKeyName JVM parameter or System environment variable name to look for. This parameter
   *        is case-sensitive. This method will check the JVM parameter first.
   * @param defaultFilePath File with full path name to open if there is no system environment
   *        variable. This is used only when there is no valid file defined in JVM parameter nor
   *        environment variable. Empty String (not null String) should be sent if method should
   *        skip this search.
   * @param defaultFileName The name of the file to be searched in classpath. This is used only when
   *        there is no valid file defined in JVM parameter, environment variable, nor defaultPath.
   *        Empty String (not null String) should be sent if method should skip this search.
   * @param checkFileRead Check if the file can be read before returning. If the value is set to
   *        <strong>true</strong>, it will check if the file can be read. if not, it will look for
   *        other file variable in search order. If the value is set to <strong>false</strong> it
   *        will pass back the first defined value in search order.
   * @return Configuration file.
   * @throws IOException When the file cannot not be read from anywhere in search order.
   */
  public static String getFilePath(String envKeyName, String defaultFilePath,
      String defaultFileName, boolean checkFileRead) throws IOException {

    List<String> configSearchList = new ArrayList<String>();
    boolean doneSearch = false;

    if (System.getProperty(envKeyName) != null) {
      logger.info("JVM parameter \"" + envKeyName + "\" found.");
      configSearchList.add(System.getProperty(envKeyName));
      if (!checkFileRead) {
        doneSearch = true;
      }
    }
    if (!doneSearch) {
      Map<String, String> envVar = System.getenv();
      for (String envName : envVar.keySet()) {
        if (envKeyName.equals(envName)) {
          logger.info("System environment variable \"" + envKeyName + "\" found.");
          configSearchList.add(envVar.get(envName));
          if (!checkFileRead) {
            doneSearch = true;
          }
          break;
        }
      }
    }
    if (!doneSearch && (defaultFilePath.length() > 0)) {
      logger.info("Default file path \"" + defaultFilePath + "\" was specified.");
      configSearchList.add(defaultFilePath);
      doneSearch = true;
    }
    if (!doneSearch && (defaultFileName.length() > 0)) {
      logger.info("Default file name \"" + defaultFileName + "\" was specified.");
      configSearchList.add(defaultFileName);
      doneSearch = true;
    }

    File configFile;
    if (!checkFileRead) {
      // This case will send back the file specified by first defined variable.
      logger.info("File \"" + configSearchList.get(0) + "\" was chosen from search locations.");
      configFile = new File(configSearchList.get(0));
      return configFile.getAbsolutePath();
    } else {
      // This case will keep reading file specified in list until one is readable, or die trying.
      for (int configIndex = 0; configIndex < configSearchList.size(); configIndex++) {
        configFile = new File(configSearchList.get(configIndex));
        if (configFile.exists() && configFile.canRead()) {
          logger.info("File \"" + configSearchList.get(configIndex)
              + "\" was chosen from search locations.");
          return configFile.getAbsolutePath();
        }
      }
      throw new IOException("Cannot find any readable file in search locations.");
    }

  }


  /**
   * Find the location of a file. The search will be performed in following order:
   * <ol>
   * <li>JVM Parameter (provided by -D[key]=[value])</li>
   * <li>Environment variable</li>
   * <li>Fully qualified file name given by the caller</li>
   * <li>File with specific name in CLASSPATH</li>
   * </ol>
   *
   * @param envKeyName JVM parameter or System environment variable name to look for. This parameter
   *        is case-sensitive. This method will check the JVM parameter first.
   * @param defaultFilePaths List of full path names to open if there is no system environment
   *        variable. This is used only when there is no valid file defined in JVM parameter nor
   *        environment variable. Empty String (not null String) should be sent if method should
   *        skip this search.
   * @param defaultFileName The name of the file to be searched in classpath. This is used only when
   *        there is no valid file defined in JVM parameter, environment variable, nor defaultPath.
   *        Empty String (not null String) should be sent if method should skip this search.
   * @param checkFileRead Check if the file can be read before returning. If the value is set to
   *        <strong>true</strong>, it will check if the file can be read. if not, it will look for
   *        other file variable in search order. If the value is set to <strong>false</strong> it
   *        will pass back the first defined value in search order.
   * @return Configuration file.
   * @throws IOException When the file cannot not be read from anywhere in search order.
   */
  public static String getFilePath(String envKeyName, List<String> defaultFilePaths,
      String defaultFileName, boolean checkFileRead) throws IOException {

    List<String> configSearchList = new ArrayList<String>();
    boolean doneSearch = false;

    // 1 Building search list
    // 1.1 Check JVM Parameters
    if ((!"".equals(envKeyName)) && (System.getProperty(envKeyName) != null)) {
      logger.info("JVM parameter \"" + envKeyName + "\" found.");
      configSearchList.add(System.getProperty(envKeyName));
      if (!checkFileRead) {
        doneSearch = true;
      }
    }

    // 1.2 Check OS Environment variables
    if (!doneSearch) {
      Map<String, String> envVar = System.getenv();
      for (String envName : envVar.keySet()) {
        if (envKeyName.equals(envName)) {
          logger.info("System environment variable \"" + envKeyName + "\" found.");
          configSearchList.add(envVar.get(envName));
          if (!checkFileRead) {
            doneSearch = true;
          }
          break;
        }
      }
    }

    // 1.3 Check default file paths
    for (String strSearchList : defaultFilePaths) {
      if (!doneSearch && (strSearchList.length() > 0)) {
        logger.info("Default file path \"" + strSearchList + "\" was specified.");
        configSearchList.add(strSearchList);
        doneSearch = true;
      }
    }

    // 1.4 Check file name in CLASSPATH
    if (!doneSearch && (defaultFileName.length() > 0)) {
      logger.info("Default file name \"" + defaultFileName + "\" was specified.");
      configSearchList.add(defaultFileName);
      doneSearch = true;
    }

    File configFile;
    if (!checkFileRead) {
      // This case will send back the file specified by first defined variable.
      logger.info("File \"" + configSearchList.get(0) + "\" was chosen from search locations.");
      configFile = new File(configSearchList.get(0));
      return configFile.getAbsolutePath();
    } else {
      // This case will keep reading file specified in list until one is readable, or die trying.
      for (int configIndex = 0; configIndex < configSearchList.size(); configIndex++) {
        configFile = new File(configSearchList.get(configIndex));
        if (configFile.exists() && configFile.canRead()) {
          logger.info("File \"" + configSearchList.get(configIndex)
              + "\" was chosen from search locations.");
          return configFile.getAbsolutePath();
        }
      }
      throw new IOException("Cannot find any readable file in search locations.");
    }

  }
}
