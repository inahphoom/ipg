package com.motiftech.common.exceptions;


/**
 * 
 * @author Wittaya W.
 *
 */
public class InputOutOfRangeException extends Exception {

  private static final long serialVersionUID = -5389241332166831730L;
  private String rangeDescription;

  public InputOutOfRangeException() {

  }

  public InputOutOfRangeException(String rangeDescription) {
    this.rangeDescription = rangeDescription;
  }

  public String getRangeDescription() {
    return rangeDescription;
  }

  public void setRangeDescription(String rangeDescription) {
    this.rangeDescription = rangeDescription;
  }

}
